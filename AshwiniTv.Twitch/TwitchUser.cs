﻿using System;
using Newtonsoft.Json;

namespace AshwiniTv.Twitch
{
    public class TwitchUser
    {
        public string Type { get; set; }
        public string Name { get; set; }

        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public DateTime UpdatedAt { get; set; }

        public string Logo { get; set; }

        [JsonProperty("_id")]
        public int Id { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        public string Email { get; set; }
        public bool Partnered { get; set; }
        public string Bio { get; set; }
    }
}