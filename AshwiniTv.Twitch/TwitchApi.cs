﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AshwiniTv.Twitch
{
    public static class TwitchApi
    {
        private static readonly CultureInfo enUS = new CultureInfo("en-US");

        public static async Task<string> AuthUser(string code, string redirectUri, string clientId, string secret)
        {
            const string url = "https://api.twitch.tv/kraken/oauth2/token";

            KeyValuePair<string, string>[] values =
            {
                new KeyValuePair<string, string>("client_id", clientId),
                new KeyValuePair<string, string>("client_secret", secret),
                new KeyValuePair<string, string>("grant_type", "authorization_code"),
                new KeyValuePair<string, string>("redirect_uri", redirectUri),
                new KeyValuePair<string, string>("code", code)
            };

            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                var content = new FormUrlEncodedContent(values);
                response = await client.PostAsync(url, content).ConfigureAwait(false);
            }

            JToken accessToken;
            var retObj = JObject.Parse(await response.Content.ReadAsStringAsync());
            retObj.TryGetValue("access_token", out accessToken);

            return accessToken.Value<string>();
        }

        public static async Task<TwitchUser> GetAuthedUser(string clientId, string token)
        {
            const string url = "https://api.twitch.tv/kraken/user";

            HttpResponseMessage response;

            using (var client = new HttpClient())
            {
                client.SetTwitchApiAuthHeaders(token, clientId);
                response = await client.GetAsync(url).ConfigureAwait(false);
            }

            return response.StatusCode == HttpStatusCode.Unauthorized
                ? null
                : JsonConvert.DeserializeObject<TwitchUser>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IList<string>> GetChannelSubscribers(string channel, string clientId, string token,
            int limit,
            int offset)
        {
            const string url = "https://api.twitch.tv/kraken/channels/{0}/subscriptions?limit={1}&offset={2}";

            JObject subObject;

            using (var client = new HttpClient())
            {
                client.SetTwitchApiAuthHeaders(token, clientId);
                var response = await client.GetAsync(string.Format(url, channel, limit, offset));
                var json = await response.Content.ReadAsStringAsync();
                subObject = JObject.Parse(json);
            }

            var totalCount = subObject["_total"];

            if (totalCount == null || totalCount.Value<int>() == 0) return null;

            var jSubs = subObject["subscriptions"] as JArray;

            return (from JObject obj in jSubs
                select obj["user"] as JObject
                into user
                select user["name"]
                into username
                select username.Value<string>()).ToList();
        }

        public static async Task<bool?> IsChannelLive(string channel, string clientId)
        {
            const string url = "https://api.twitch.tv/kraken/streams/{0}";

            try
            {
                JObject streamObject;
                using (var client = new HttpClient())
                {
                    client.SetTwitchApiHeaders(clientId);
                    var response = await client.GetAsync(string.Format(url, channel));
                    var json = await response.Content.ReadAsStringAsync();
                    streamObject = JObject.Parse(json);
                }

                return streamObject["stream"].Type != JTokenType.Null;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<bool> UpdateTitle(string channel, string clientId, string newTitle, string token)
        {
            return await UpdateChannel(channel, clientId, "status", newTitle, token);
        }

        public static async Task<bool> UpdateGame(string channel, string clientId, string newGame, string token)
        {
            return await UpdateChannel(channel, clientId, "game", newGame, token);
        }

        public static async Task<DateTime?> GetFollowTime(string channel, string clientId, string username)
        {
            const string url = "https://api.twitch.tv/kraken/users/{0}/follows/channels/{1}";

            try
            {
                JObject followObject;
                using (var client = new HttpClient())
                {
                    client.SetTwitchApiHeaders(clientId);
                    var response = await client.GetAsync(string.Format(url, username, channel));
                    var json = await response.Content.ReadAsStringAsync();
                    followObject = JObject.Parse(json);
                }

                return followObject["error"] != null ? DateTime.MaxValue : DateTime.Parse(followObject["created_at"].Value<string>(), enUS);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static async Task<string> GetLastGamePlayed(string channel, string clientId)
        {
            const string url = "https://api.twitch.tv/kraken/channels/{0}";

            try
            {
                JObject channelObject;
                using (var client = new HttpClient())
                {
                    client.SetTwitchApiHeaders(clientId);
                    var response = await client.GetAsync(string.Format(url, channel));
                    var json = await response.Content.ReadAsStringAsync();
                    channelObject = JObject.Parse(json);
                }

                return channelObject["game"].Value<string>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public static async Task<IList<string>> GetChatters(string channel, string clientId)
        {
            const string url = "https://tmi.twitch.tv/group/user/{0}/chatters";

            try
            {
                var result = new List<string>();
                using (var client = new HttpClient())
                {
                    client.SetTwitchApiHeaders(clientId);
                    var response = await client.GetAsync(string.Format(url, channel));
                    var json = await response.Content.ReadAsStringAsync();
                    var viewersObject = JObject.Parse(json);

                    foreach (var category in viewersObject["chatters"])
                    {
                        result.AddRange(((JProperty)category).Value.Values<string>());
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        private static async Task<bool> UpdateChannel(string channel, string clientId, string changedValue,
            string newValue, string token)
        {
            const string url = "https://api.twitch.tv/kraken/channels/{0}";

            KeyValuePair<string, string>[] values =
            {
                new KeyValuePair<string, string>($"channel[{changedValue}]", newValue)
            };

            JObject resObject;

            using (var client = new HttpClient())
            {
                client.SetTwitchApiAuthHeaders(token, clientId);
                var response = await client.PutAsync(string.Format(url, channel), new FormUrlEncodedContent(values));
                var resData = await response.Content.ReadAsStringAsync();
                resObject = JObject.Parse(resData);
            }

            JToken returnedStatus;
            return resObject.TryGetValue(changedValue, out returnedStatus) && returnedStatus.Value<string>() == newValue;
        }
    }
}