﻿using System.Net.Http;
using System.Net.Http.Headers;

namespace AshwiniTv.Twitch
{
    internal static class Extensions
    {
        public static void SetTwitchApiAuthHeaders(this HttpClient client, string token, string clientId)
        {
            SetTwitchApiHeaders(client, clientId);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("OAuth", token);
        }

        public static void SetTwitchApiHeaders(this HttpClient client, string clientId)
        {
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.twitchtv.v3+json"));
            client.DefaultRequestHeaders.Add("Client-ID", clientId);
        }
    }
}