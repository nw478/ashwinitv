﻿using System.ServiceModel;

namespace AshwiniTv.AshBot.SnapchatNotify
{
    [ServiceContract]
    public interface INotifyService
    {
        [OperationContract]
        void Reset();

        [OperationContract]
        bool SendSnapchatTo(string username);
    }

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class NotifyService : INotifyService
    {
        private readonly MainWindow _window;

        public NotifyService(MainWindow window)
        {
            _window = window;
        }

        public void Reset()
        {
            _window.NavigateToCompose();
        }

        public bool SendSnapchatTo(string username)
        {
            return _window.SendSnapchatTo(username);
        }
    }
}
