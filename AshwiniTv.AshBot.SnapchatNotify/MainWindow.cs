﻿using System;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using AshwiniTv.Shared;

namespace AshwiniTv.AshBot.SnapchatNotify
{
    public partial class MainWindow : Form
    {
        private const string MessageSubject = "Ashwini's snapchat";

        private static readonly string MessageBody =
            string.Format(
                @"Thank you for subscribing to Ashwini - her snapchat is {0}. Please do not give it to anyone else.",
                TokenManager.SnapchatName);

        private readonly ServiceHost _host;

        public AutoResetEvent ReadyEvent { get; private set; }

        public MainWindow()
        {
            InitializeComponent();

            ReadyEvent = new AutoResetEvent(false);

            _host = new ServiceHost(new NotifyService(this));
            _host.Open();
        }

        public bool SendSnapchatTo(string user)
        {
            try
            {
                var document = wbMain.Document;

                var toLoginField = document.GetElementById("to_login");
                var subjectField = document.GetElementById("message_subject");
                var messageField = document.GetElementById("message_body");
                var sendButton = document.GetElementById("send");

                toLoginField.SetAttribute("value", user);
                subjectField.SetAttribute("value", MessageSubject);
                messageField.SetAttribute("value", MessageBody);

                sendButton.InvokeMember("click");

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void NavigateToCompose()
        {
            wbMain.Navigate("http://www.twitch.tv/message/compose");
        }

        private void wbMain_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() == "http://www.twitch.tv/message/compose") ReadyEvent.Set();
        }
    }
}