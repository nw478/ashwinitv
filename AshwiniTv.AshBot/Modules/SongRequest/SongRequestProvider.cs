﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Modules.SongRequest
{
    public sealed class SongRequestProvider : Module
    {
        private const string BaseUrl = "http://192.168.1.115:8085/SongRequest/";
        private const string UserAgent = "WiniBot v2.x";
        private static long _logId;
        private readonly BlockingCollection<SongRequestItem> _requests;
        private readonly SongRequestStatus _status;
        private readonly Thread _worker;

        public SongRequestProvider(ChannelContext context)
            : base(context)
        {
            _requests = new BlockingCollection<SongRequestItem>();
            _status = new SongRequestStatus();
            var config = GetConfigValues();

            base.Enabled = bool.Parse(config["enabled"]);
            QueueCapacity = int.Parse(config["queue_capacity"]);
            SongsPerPleb = int.Parse(config["songs_per_pleb"]);
            SongsPerSub = int.Parse(config["songs_per_sub"]);
            SongPoints = bool.Parse(config["song_points"]);
            MaxSongLength = TimeSpan.FromSeconds(int.Parse(config["max_len"]));
            SubOnly = bool.Parse(config["sub_only"]);
            SkipVoting = bool.Parse(config["skip_voting"]);
            SkipVoteCount = int.Parse(config["skip_vote_count"]);

            Logger.QueueMessage(
                "SongRequestProvider starting - enabled: {0}, capacity: {1}, songs per user: {2}, song points: {3}, max song length: {4}, sub only: {5}",
                Enabled, QueueCapacity, SongsPerPleb, SongPoints, MaxSongLength, SubOnly);

            _worker = new Thread(RequestWorker) {Name = "SongRequest Worker"};
            _worker.Start();
        }

        public bool BanUser(string username, bool ban)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var user = tits.Users.FirstOrDefault(u => u.username == username);
                if (user == null) return false;
                user.srBan = ban;
                tits.SaveChanges();
                Logger.QueueMessage("SRP::BanUser - User {0} {1} from song requests", username,
                    ban ? "banned" : "unbanned");
                return true;
            }
        }

        internal void SongStateChange(long songId, bool isPlaying)
        {
            if (songId == -1) return;

            lock (SyncLock)
            {
                _status.IsPlaying = isPlaying;
                _status.RequestId = songId;
                _status.Voters.Clear();
                _status.SkipVotes = 0;
                UpdateCurrentStatus();
            }
        }

        public void SongSkipVote(string user)
        {
            lock (SyncLock)
            {
                if (!SkipVoting) return;

                if (!_status.Voters.Add(user)) return;

                ++_status.SkipVotes;
                if (_status.SkipVotes == 1)
                    Context.SendMessage(ModuleResources.SongRequest_SkipVoteRegistered1, _status.Name);
                else
                    Context.SendMessage(ModuleResources.SongRequest_SkipVoteRegistered2More, _status.SkipVotes,
                        _status.Name);

                if (_status.SkipVotes < SkipVoteCount) return;

                SkipSong(-1);
                _status.Voters.Clear();
                _status.SkipVotes = 0;

                var sb = new StringBuilder();
                foreach (var name in _status.Voters) sb.AppendFormat("{0}, ", name);
                sb.Length -= 2;

                Context.SendMessage(ModuleResources.SongRequest_SongSkippedByVotes, sb.ToString());
            }
        }

        private bool RequestSongCheck(SongRequestItem item)
        {
            if (!Enabled) return false;

            var user = item.RequestedBy;

            if (SubOnly && !SubManager.IsSub(user)) return false;

            using (var tits = new AshwiniTvEntities())
            {
                if (IsUserBanned(user, tits))
                {
                    Logger.QueueMessage("SRP::RequestSongCheck::[{0}] - user {1} is banned from requesting songs",
                        item.LogRequestId, user);
                    Context.SendMessage(ModuleResources.SongRequest_UserBanned, user);
                    return false;
                }

                if (SongPoints && !Context.PointManager.SongRequested(user))
                {
                    Context.SendMessage(ModuleResources.SongRequest_NotEnoughPoints, user);
                    return false;
                }

                if (IsQueueFull(tits))
                {
                    Logger.QueueMessage("SRP::RequestSongCheck::[{0}] - song queue is full", item.LogRequestId);
                    Context.SendMessage(ModuleResources.SongRequest_QueueFull, user);
                    return false;
                }

                int songLimit;

                if (UserCanAddMoreSongs(user, out songLimit, tits)) return true;

                Logger.QueueMessage("SRP::RequestSongCheck::[{0}] - user {1} has too many songs in the queue",
                    item.LogRequestId, user);
                Context.SendMessage(ModuleResources.SongRequest_TooManySongs, user, songLimit);
                return false;
            }
        }

        private bool IsQueueFull(AshwiniTvEntities tits)
        {
            var songCount = tits.SongRequests.Count(s => !s.hidden && !s.played && !s.skipped);
            return songCount >= QueueCapacity;
        }

        private bool UserCanAddMoreSongs(string user, out int count, AshwiniTvEntities tits)
        {
            count = SubManager.IsRegular(user, true) ? SongsPerSub : SongsPerPleb;
            var dbUser = DbUtils.EnsureUser(user, tits);
            var songCount =
                tits.SongRequests.Count(s => s.requestedBy == dbUser.id && !s.played && !s.hidden && !s.skipped);
            return songCount < count;
        }

        private void UpdateCurrentStatus()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbRequest = tits.SongRequests.Find(_status.RequestId);

                // ReSharper disable once PossibleNullReferenceException
                _status.Name = dbRequest.Song.name;
                _status.RequestedBy = dbRequest.User.username;
                _status.Link = dbRequest.Song.source == (byte) ApiSong.SongSource.SoundCloud
                    ? GetLink(dbRequest.Song.source, dbRequest.Song.link)
                    : GetLink(dbRequest.Song.source, dbRequest.Song.trackID);
            }
        }

        private static bool IsUserBanned(string user, AshwiniTvEntities tits)
        {
            var dbUser = tits.Users.FirstOrDefault(u => u.username == user);
            return dbUser != null && dbUser.srBan;
        }

        private static Dictionary<string, string> GetConfigValues()
        {
            using (var tits = new AshwiniTvEntities())
            {
                return tits.SRDatas.ToDictionary(c => c.name, c => c.value);
            }
        }

        private static void SetConfigValue<T>(string name, T value)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var config = tits.SRDatas.First(c => c.name == name);
                config.value = value.ToString();
                tits.SaveChanges();
            }
        }

        private static ApiSong GetSong(string link, long reqId)
        {
            var sw = new Stopwatch();
            sw.Start();
            var apiSong = new ApiSong(link);
            
            Logger.QueueMessage("SRP::GetSong::[{0}] - parsed song link - source {1}, track id {2}", reqId,
                apiSong.Source, apiSong.TrackId);

            using (var tits = new AshwiniTvEntities())
            {
                Song dbSong;

                switch (apiSong.Source)
                {
                    case ApiSong.SongSource.Vimeo:
                    case ApiSong.SongSource.YouTube:
                        dbSong =
                            tits.Songs.FirstOrDefault(
                                s => s.trackID == apiSong.TrackId && s.source == (byte) apiSong.Source);
                        break;
                    case ApiSong.SongSource.SoundCloud:
                        var scUrl = SoundCloudApi.StripSoundCloudLink(apiSong.Link);
                        dbSong = tits.Songs.FirstOrDefault(s => s.link == scUrl);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (dbSong != null)
                {
                    apiSong.Error = ApiSong.SongError.None;
                    apiSong.Id = dbSong.id;
                    apiSong.Length = dbSong.length;
                    apiSong.Link = dbSong.link;
                    apiSong.Name = dbSong.name;
                    apiSong.Suspicious = dbSong.warning;
                    apiSong.SuspiciousReason = (SongWarningReason) dbSong.warningReason;
                    apiSong.Banned = dbSong.banned;
                    apiSong.BanOnRequest = dbSong.banUserOnRequest;
                    sw.Stop();
                    Logger.QueueMessage("SRP::GetSong::[{0}] - song already exists in DB - id {1}; time taken {2} ms", reqId, dbSong.id, sw.ElapsedMilliseconds);
                    return apiSong;
                }
            }

            Logger.QueueMessage("SRP::GetSong::[{0}] - retrieving data", reqId);
            switch (apiSong.Source)
            {
                case ApiSong.SongSource.YouTube:
                    YouTubeApi.RetrieveVideoData(apiSong, "CZ");
                    break;
                case ApiSong.SongSource.SoundCloud:
                    SoundCloudApi.RetrieveVideoData(apiSong);
                    break;
                case ApiSong.SongSource.Vimeo:
                    VimeoApi.RetrieveVideoData(apiSong);
                    break;
            }
            Logger.QueueMessage("SRP::GetSong::[{0}] - retrieved data, song title is {1}", reqId, apiSong.Name);

            if (string.IsNullOrEmpty(apiSong.Name))
            {
                apiSong.Error = ApiSong.SongError.DoesNotExist;
                sw.Stop();
                Logger.QueueMessage("SRP::GetSong::[{0}] - song does not exist, time taken {1} ms", reqId, sw.ElapsedMilliseconds);
                return apiSong;
            }

            apiSong.ExamineSong();

            Logger.QueueMessage("SRP::GetSong::[{0}] - inserting song into DB", reqId);
            using (var tits = new AshwiniTvEntities())
            using (new TableLock(tits, "Songs"))
            {
                var lastId = tits.Songs.OrderByDescending(s => s.id).Select(s => s.id).FirstOrDefault();
                apiSong.Id = lastId + 1;

                var song = new Song
                {
                    id = apiSong.Id,
                    length = apiSong.Length,
                    source = (byte) apiSong.Source,
                    trackID = apiSong.TrackId,
                    link = apiSong.Link,
                    name = apiSong.Name,
                    warning = apiSong.Suspicious,
                    warningReason = (byte) apiSong.SuspiciousReason,
                    banned = apiSong.Banned,
                    banUserOnRequest = apiSong.BanOnRequest
                };

                tits.Songs.Add(song);
                tits.SaveChanges();
            }

            sw.Stop();
            Logger.QueueMessage("SRP::GetSong::[{0}] - song inserted into DB, id {1}, time taken {2} ms", reqId, apiSong.Id, sw.ElapsedMilliseconds);
            return apiSong;
        }

        private static string GetLink(byte source, string trackId)
        {
            switch ((ApiSong.SongSource) source)
            {
                case ApiSong.SongSource.YouTube:
                    return "https://www.youtube.com/watch?v=" + trackId;
                case ApiSong.SongSource.Vimeo:
                    return "https://vimeo.com/" + trackId;
                case ApiSong.SongSource.SoundCloud:
                    return "https://www.soundcloud.com/" + trackId;
                default:
                    return null;
            }
        }

        private static void NotifySongListChange(bool skipCurrentSong = false)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.BaseAddress = BaseUrl;
                    client.Headers.Add(HttpRequestHeader.UserAgent, UserAgent);
                    client.DownloadData(skipCurrentSong ? "SkipCurrentSong" : "SongListChange");
                }
            }
            catch (WebException ex)
            {
                Console.WriteLine(@"WebException: {0}", ex.Message);
            }
        }

        private void ReportSongError(ApiSong.SongError error, string requestedBy)
        {
            switch (error)
            {
                case ApiSong.SongError.None:
                    return;
                case ApiSong.SongError.HostError:
                    Context.SendMessage(ModuleResources.SongRequest_HostError, requestedBy);
                    break;
                case ApiSong.SongError.DoesNotExist:
                    Context.SendMessage(ModuleResources.SongRequest_NotFound, requestedBy);
                    break;
                case ApiSong.SongError.RegionRestricted:
                    Context.SendMessage(ModuleResources.SongRequest_RegionRestricted, requestedBy);
                    break;
                case ApiSong.SongError.NotEmbeddable:
                    Context.SendMessage(ModuleResources.SongRequest_NoEmbed, requestedBy);
                    break;
                case ApiSong.SongError.NotATrack:
                    Context.SendMessage(ModuleResources.SongRequest_NotATrack, requestedBy);
                    break;
            }
        }

        internal static long GetLogId()
        {
            return Interlocked.Increment(ref _logId);
        }

        private enum SongRequestAction
        {
            NewSong,
            CurrentSong,
            SkipSong,
            ResetSongs,
            AddCurrentToList,
            AddNewToList,
            RequestSongFromList,
            PromoteSong,
            BanSong,
            PrioritizeSong,
            BanPlaylist
        }

        private class SongRequestItem
        {
            public string Link { get; set; }
            public string RequestedBy { get; set; }
            public long Id { get; set; }
            public SongRequestAction Action { get; set; }
            public object[] ExtraData { get; set; }
            public long LogRequestId { get; } = GetLogId();
        }

        private class SongRequestStatus
        {
            public SongRequestStatus()
            {
                Voters = new HashSet<string>();
            }

            public long RequestId { get; set; }
            public int Position { get; set; }
            public string Name { get; set; }
            public string Link { get; set; }
            public string RequestedBy { get; set; }
            public bool IsPlaying { get; set; }
            public int SkipVotes { get; set; }
            public HashSet<string> Voters { get; }
        }

        #region PROPERTIES

        public override bool Enabled
        {
            get { return base.Enabled; }
            set { throw new InvalidOperationException(); }
        }

        public bool SubOnly { get; private set; }
        public int QueueCapacity { get; private set; }
        public int SongsPerPleb { get; private set; }
        public int SongsPerSub { get; private set; }
        public bool SongPoints { get; private set; }
        public bool SkipVoting { get; private set; }
        public int SkipVoteCount { get; private set; }
        public TimeSpan MaxSongLength { get; private set; }

        #endregion

        #region REQUESTS

        public void RequestSong(string user, string link, int prioPoints)
        {
            var req = new SongRequestItem
            {
                Link = link,
                RequestedBy = user,
                Action = SongRequestAction.NewSong,
                ExtraData = new object[] {prioPoints}
            };

            Logger.QueueMessage("SRP::RequestSong::[{2}] - Song {0} requested by {1}", link, user, req.LogRequestId);
            _requests.Add(req);
        }

        public void CurrentSong(string user)
        {
            var req = new SongRequestItem {Action = SongRequestAction.CurrentSong, RequestedBy = user};
            Logger.QueueMessage("SRP::CurrentSong::[{1}] - {0} requested current song info", user, req.LogRequestId);
            _requests.Add(req);
        }

        public void SkipSong(int id)
        {
            var req = new SongRequestItem {Action = SongRequestAction.SkipSong, Id = id};
            Logger.QueueMessage("SRP::SkipSong::[{1}] - song id {0} requested to be skipped", id, req.LogRequestId);
            _requests.Add(req);
        }

        public void ResetSongs()
        {
            var req = new SongRequestItem {Action = SongRequestAction.ResetSongs};
            Logger.QueueMessage("SRP::ResetSongs::[{0}] - song list requested to be reset", req.LogRequestId);
            _requests.Add(req);
        }

        public void AddCurrentSongToPersonalList(string user, string alias)
        {
            _requests.Add(new SongRequestItem
            {
                Action = SongRequestAction.AddCurrentToList,
                RequestedBy = user,
                ExtraData = new object[] {alias == string.Empty ? null : alias}
            });
        }

        public void AddNewSongToPersonalList(string user, string link, string alias)
        {
            _requests.Add(new SongRequestItem
            {
                Action = SongRequestAction.AddNewToList,
                RequestedBy = user,
                Link = link,
                ExtraData = new object[] {alias == string.Empty ? null : alias}
            });
        }

        public void RequestSongFromPersonalList(string user, string alias)
        {
            _requests.Add(new SongRequestItem
            {
                Action = SongRequestAction.RequestSongFromList,
                RequestedBy = user,
                Link = alias
            });
        }

        public void PromoteSong(long id)
        {
            _requests.Add(new SongRequestItem {Action = SongRequestAction.PromoteSong, Id = id});
        }

        public void BanSong(string link)
        {
            var req = new SongRequestItem {Action = SongRequestAction.BanSong, Link = link};
            Logger.QueueMessage("SRP::BanSong::[{1}] - song {0} requested to be banned", link, req.LogRequestId);
            _requests.Add(req);
        }

        public void PrioritizeSong(string user, long id, int pointCost, bool forced)
        {
            var req = new SongRequestItem
            {
                Action = SongRequestAction.PrioritizeSong,
                RequestedBy = user,
                Id = id,
                ExtraData = new object[] {pointCost, forced}
            };
            Logger.QueueMessage("SRP::PrioritizeSong::[{3}] - {0} requested prio of song {1} for {2} points", user, id,
                pointCost, req.LogRequestId);
            _requests.Add(req);
        }

        public void BanPlaylist(string playlistId)
        {
            var req = new SongRequestItem {Action = SongRequestAction.BanPlaylist, Link = playlistId};
            Logger.QueueMessage("SRP::BanPlaylist::[{1}] - playlist {0} was requested to be banned", playlistId,
                req.LogRequestId);
            _requests.Add(req);
        }

        #endregion

        #region CONFIG SETTINGS

        public void SetVolume(int volume)
        {
            Task.Run(() =>
            {
                using (var wc = new WebClient())
                {
                    wc.BaseAddress = BaseUrl;
                    wc.Headers.Add(HttpRequestHeader.UserAgent, UserAgent);

                    wc.DownloadData("SetVolume?volume=" + volume);
                }
            });
        }

        public void SetEnabled(bool enabled)
        {
            base.Enabled = enabled;
            SetConfigValue("enabled", enabled);
        }

        public void SetSubOnly(bool subOnly)
        {
            SubOnly = subOnly;
            SetConfigValue("sub_only", subOnly);
        }

        public void SetRequestPoints(bool points)
        {
            SongPoints = points;
            SetConfigValue("song_points", points);
        }

        public void SetSongsPerPleb(int spu)
        {
            SongsPerPleb = spu;
            SetConfigValue("songs_per_pleb", spu);
        }

        public void SetSongsPerSub(int spu)
        {
            SongsPerSub = spu;
            SetConfigValue("songs_per_sub", spu);
        }

        public void SetQueueCapacity(int capacity)
        {
            QueueCapacity = capacity;
            SetConfigValue("queue_capacity", capacity);
        }

        public void SetMaxSongLength(TimeSpan maxLen)
        {
            MaxSongLength = maxLen;
            SetConfigValue("max_len", (int) maxLen.TotalSeconds);
        }

        public void SetSkipVote(bool voting)
        {
            SkipVoting = voting;
            SetConfigValue("skip_voting", voting);
        }

        public void SetSkipVoteCount(int count)
        {
            SkipVoteCount = count;
            SetConfigValue("skip_vote_count", count);
        }

        #endregion

        #region REQUEST WORKERS

        private void RequestWorker()
        {
            while (true)
            {
                try
                {
                    var item = _requests.Take();
                    Logger.QueueMessage("SRP::RequestWorker::[{0}] - retrieved sr item", item.LogRequestId);

                    switch (item.Action)
                    {
                        case SongRequestAction.NewSong:
                            Task.Run(() => RequestSong(item));
                            break;
                        case SongRequestAction.CurrentSong:
                            Task.Run(() => ReportCurrentSong(item.RequestedBy));
                            break;
                        case SongRequestAction.SkipSong:
                            Task.Run(() => SkipSong(item));
                            break;
                        case SongRequestAction.ResetSongs:
                            Task.Run(() => InternalResetSongs());
                            break;
                        case SongRequestAction.AddCurrentToList:
                            Task.Run(() => AddCurrentSongToPersonalList(item));
                            break;
                        case SongRequestAction.AddNewToList:
                            Task.Run(() => AddNewSongToPersonalList(item));
                            break;
                        case SongRequestAction.RequestSongFromList:
                            Task.Run(() => RequestSongFromPersonalList(item));
                            break;
                        case SongRequestAction.PromoteSong:
                            Task.Run(() => InternalPromoteSong(item.Id));
                            break;
                        case SongRequestAction.BanSong:
                            Task.Run(() => InternalBanSong(item.Link, item.LogRequestId));
                            break;
                        case SongRequestAction.PrioritizeSong:
                            Task.Run(() => PrioritizeSong(item));
                            break;
                        case SongRequestAction.BanPlaylist:
                            Task.Run(() => InternalBanPlaylist(item));
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + @" " + ex.StackTrace);
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private void RequestSong(SongRequestItem item)
        {
            Logger.QueueMessage("SRP::__RequestSong::[{0}] - Processing request", item.LogRequestId);
            if (!RequestSongCheck(item)) return;

            var apiSong = GetSong(item.Link, item.LogRequestId);
            Logger.QueueMessage("SRP::__RequestSong::[{0}] - retrieved details of song '{1}'", item.LogRequestId,
                apiSong.Name);

            if (apiSong.Error != ApiSong.SongError.None)
            {
                ReportSongError(apiSong.Error, item.RequestedBy);
                return;
            }

            if (apiSong.Length > MaxSongLength)
            {
                Logger.QueueMessage("SRP::__RequestSong::[{0}] - song rejected, too long ({1})", item.LogRequestId,
                    apiSong.Length);
                Context.SendMessage(ModuleResources.SongRequest_TooLong, item.RequestedBy, MaxSongLength.Minutes);
                return;
            }

            if (apiSong.Banned)
            {
                Logger.QueueMessage("SRP::__RequestSong::[{0}] - song rejected, banned", item.LogRequestId,
                    apiSong.Length);
                Context.SendMessage(ModuleResources.SongRequest_BannedSong, item.RequestedBy);
                if (apiSong.BanOnRequest)
                {
                    Logger.QueueMessage("SRP::__RequestSong::[{0}] - banning user {1}", item.LogRequestId,
                        item.RequestedBy);
                    Context.TimeoutUser(item.RequestedBy, (int) TimeSpan.FromHours(12).TotalSeconds);
                }
                return;
            }

            long requestId;

            using (var tits = new AshwiniTvEntities())
            {
                var existingRequest =
                    tits.SongRequests.FirstOrDefault(
                        sr => sr.songID == apiSong.Id && !sr.played && !sr.hidden && !sr.skipped);

                if (existingRequest != null)
                {
                    Logger.QueueMessage(
                        "SRP::__RequestSong::[{0}] - song already in queue (RID {1}), discarding request",
                        item.LogRequestId, existingRequest.id);
                    Context.SendMessage(ModuleResources.SongRequest_SongAlreadyExists, item.RequestedBy);
                    return;
                }

                Logger.QueueMessage("SRP::__RequestSong::[{0}] - inserting request into DB", item.LogRequestId);
                using (new TableLock(tits, "SongRequests"))
                {
                    requestId = tits.SongRequests.OrderByDescending(s => s.id).Select(s => s.id).FirstOrDefault();
                    ++requestId;

                    var position =
                        tits.SongRequests.Where(sr => !sr.played && !sr.hidden && !sr.skipped)
                            .Select(sr => sr.position)
                            .Max() ?? 0;
                    ++position;

                    var dbUser = DbUtils.EnsureUser(item.RequestedBy, tits);

                    var request = new Shared.DB.SongRequest
                    {
                        id = requestId,
                        requestedBy = dbUser.id,
                        songID = apiSong.Id,
                        position = position
                    };

                    tits.SongRequests.Add(request);
                    tits.SaveChanges();
                    Logger.QueueMessage("SRP::__RequestSong::[{0}] - request inserted into DB, RID {1}",
                        item.LogRequestId, requestId);
                }
            }

            var prioPoints = (int) item.ExtraData[0];
            if (prioPoints > 0) PrioritizeSong(item.RequestedBy, requestId, prioPoints, true);

            NotifySongListChange();
            Context.SendMessage(ModuleResources.SongRequest_SongAdded, item.RequestedBy, apiSong.Name, requestId);
        }

        private void ReportCurrentSong(string user)
        {
            if (!Enabled) return;

            if (!_status.IsPlaying) Context.SendMessage(ModuleResources.SongRequest_IsNowPaused);
            else
                Context.SendMessage(ModuleResources.SongRequest_CurrentSong, user, _status.Name, _status.RequestedBy,
                    _status.Link, _status.RequestId);
        }

        private void SkipSong(SongRequestItem item)
        {
            if (Shared.SongRequest.SkipSong(item.Id, _status.RequestId))
            {
                NotifySongListChange(item.Id == -1);
                Context.SendMessage(ModuleResources.SongRequest_Skipped, item.Id);
            }
            else Context.SendMessage(ModuleResources.SongRequest_NoSuchRequest, item.Id);
        }

        private void InternalResetSongs()
        {
            using (var tits = new AshwiniTvEntities()) tits.ResetSongs();
            NotifySongListChange();
            Context.SendMessage(ModuleResources.SongRequest_SongsReset);
        }

        private void AddCurrentSongToPersonalList(SongRequestItem item)
        {
            string songName;

            using (var tits = new AshwiniTvEntities())
            {
                var listEntry = new SongList();

                lock (SyncLock)
                {
                    listEntry.songID = tits.SongRequests.Find(_status.RequestId).songID;
                    songName = _status.Name;
                }

                listEntry.userID = DbUtils.EnsureUser(item.RequestedBy, tits).id;
                listEntry.alias = item.ExtraData[0].ToString();
                tits.SongLists.Add(listEntry);
                tits.SaveChanges();
            }

            Context.SendMessage(ModuleResources.SongRequests_PersonalList_Added, item.RequestedBy, songName);
        }

        private void AddNewSongToPersonalList(SongRequestItem item)
        {
            var apiSong = GetSong(item.Link, item.LogRequestId);

            if (apiSong.Error != ApiSong.SongError.None)
            {
                ReportSongError(apiSong.Error, item.RequestedBy);
                return;
            }

            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = DbUtils.EnsureUser(item.RequestedBy, tits);

                var listEntry = new SongList
                {
                    userID = dbUser.id,
                    songID = apiSong.Id,
                    alias = item.ExtraData[0].ToString()
                };

                tits.SongLists.Add(listEntry);
                tits.SaveChanges();
            }

            Context.SendMessage(ModuleResources.SongRequests_PersonalList_Added, item.RequestedBy, apiSong.Name);
        }

        private void RequestSongFromPersonalList(SongRequestItem item)
        {
            if (!RequestSongCheck(item)) return;

            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = DbUtils.EnsureUser(item.RequestedBy, tits);
                IEnumerable<Song> eligibleSongs;
                if (string.IsNullOrEmpty(item.Link))
                    eligibleSongs =
                        tits.SongLists.Where(sl => sl.userID == dbUser.id)
                            .OrderBy(sl => Guid.NewGuid())
                            .Select(sl => sl.Song);
                else
                {
                    eligibleSongs =
                        tits.SongLists.Where(sl => sl.userID == dbUser.id && sl.alias.Contains(item.Link))
                            .OrderBy(sl => Guid.NewGuid())
                            .Select(sl => sl.Song);
                    if (!eligibleSongs.Any())
                        eligibleSongs =
                            tits.SongLists.Where(
                                sl => sl.userID == dbUser.id && sl.Song.name.Contains(item.Link))
                                .OrderBy(sl => Guid.NewGuid())
                                .Select(sl => sl.Song);
                }

                var songs = eligibleSongs.Where(song => song.length <= MaxSongLength || !song.banned).ToList();

                if (songs.Any())
                {
                    foreach (var song in from song in songs
                        // ReSharper disable once AccessToDisposedClosure
                        let existingRequest = tits.SongRequests.FirstOrDefault(
                            sr => sr.songID == song.id && !sr.played && !sr.hidden && !sr.skipped)
                        where existingRequest == null
                        select song)
                    {
                        long requestId;

                        using (new TableLock(tits, "SongRequests"))
                        {
                            requestId =
                                tits.SongRequests.OrderByDescending(s => s.id).Select(s => s.id).FirstOrDefault();
                            ++requestId;

                            var request = new Shared.DB.SongRequest
                            {
                                id = requestId,
                                requestedBy = dbUser.id,
                                songID = song.id
                            };

                            tits.SongRequests.Add(request);
                            tits.SaveChanges();
                        }

                        NotifySongListChange();

                        Context.SendMessage(ModuleResources.SongRequest_SongAdded, item.RequestedBy, song.name,
                            requestId);
                        return;
                    }
                }

                Context.SendMessage(ModuleResources.SongRequests_PersonalList_NoSuchSongs, item.RequestedBy);
            }
        }

        private void InternalPromoteSong(long id)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var promoted =
                    tits.SongRequests.FirstOrDefault(sr => sr.promoted && !sr.played && !sr.skipped && !sr.hidden);
                if (promoted != null) Context.SendMessage(ModuleResources.SongRequest_AnotherAlreadyPromoted);
                else
                {
                    var song = tits.SongRequests.FirstOrDefault(sr => sr.id == id);
                    if (song == null) Context.SendMessage(ModuleResources.SongRequest_NoSuchRequest, id);
                    else
                    {
                        song.promoted = true;
                        tits.SaveChanges();
                        NotifySongListChange();
                        Context.SendMessage(ModuleResources.SongRequest_SongPromoted, song.Song.name);
                    }
                }
            }
        }

        private bool InternalBanSong(string song, long logId, bool message = true)
        {
            var apiSong = GetSong(song, logId);
            if (apiSong.Error != ApiSong.SongError.None) return false;

            using (var tits = new AshwiniTvEntities())
            {
                Song dbSong;

                switch (apiSong.Source)
                {
                    case ApiSong.SongSource.Vimeo:
                    case ApiSong.SongSource.YouTube:
                        dbSong =
                            tits.Songs.FirstOrDefault(
                                s => s.trackID == apiSong.TrackId && s.source == (byte) apiSong.Source);
                        break;
                    case ApiSong.SongSource.SoundCloud:
                        var scUrl = SoundCloudApi.StripSoundCloudLink(apiSong.Link);
                        dbSong = tits.Songs.FirstOrDefault(s => s.link == scUrl);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (dbSong == null) return false;
                var alreadyBanned = dbSong.banned;
                dbSong.banned = true;
                dbSong.banUserOnRequest = true;
                tits.SaveChanges();

                if (message) Context.SendMessage(ModuleResources.SongRequest_SongBan_VideoBanned, apiSong.Name);
                return !alreadyBanned;
            }
        }

        private void InternalBanPlaylist(SongRequestItem item)
        {
            ISet<string> videos = null;
            string title = null;

            if (!YouTubeApi.RetrievePlaylist(item.Link, ref title, ref videos))
            {
                Context.SendMessage(ModuleResources.SongRequest_SongBan_ListProcessingFailed);
                return;
            }

            Context.SendMessage(ModuleResources.SongRequest_SongBan_ProcessingPlaylist, title);

            var totalProcessed = videos.Count;
            var totalBanned = videos.Sum(videoId => Convert.ToInt32(InternalBanSong(videoId, item.LogRequestId, false)));

            Context.SendMessage(ModuleResources.SongRequest_SongBan_PlaylistBanned, totalBanned,
                totalProcessed - totalBanned);
        }

        private void PrioritizeSong(SongRequestItem item)
        {
            var positions = Math.Abs(((int) item.ExtraData[0])/Context.PointManager.SongPrioPoints);
            var force = (bool) item.ExtraData[1];

            using (var tits = new AshwiniTvEntities())
            {
                var req = tits.SongRequests.Find(item.Id);

                if (req.played || req.hidden || req.skipped)
                {
                    Context.SendMessage(ModuleResources.SongRequest_NoSuchRequest, item.Id);
                    return;
                }

                if (req.position < 3)
                {
                    Context.SendMessage(ModuleResources.SongRequest_SongPrioSongAlreadyTop, item.RequestedBy);
                    return;
                }

                if (req.User.username.Trim() != item.RequestedBy && !force)
                {
                    Context.SendMessage(ModuleResources.SongRequest_SongPrioNotYourSong, item.RequestedBy, item.Id,
                        positions*Context.PointManager.SongPrioPoints);
                    return;
                }

                // ReSharper disable once PossibleInvalidOperationException
                if (req.position.Value - positions < 2) positions = req.position.Value - 2;

                if (!Context.PointManager.SongPrioritized(item.RequestedBy, positions))
                {
                    Context.SendMessage(ModuleResources.SongRequest_SongPrioNotEnoughPoints, item.RequestedBy);
                    return;
                }

                using (new TableLock(tits, "SongRequests"))
                {
                    var victimReq =
                        tits.SongRequests.SingleOrDefault(
                            sr =>
                                !sr.played && !sr.hidden && !sr.skipped &&
                                sr.position.Value == req.position.Value - positions);

                    if (victimReq == null) throw new InvalidOperationException("wtf");

                    var newPos = victimReq.position;
                    victimReq.position = req.position;
                    req.position = newPos;
                    tits.SaveChanges();
                }

                NotifySongListChange();
                Context.SendMessage(ModuleResources.SongRequest_SongPrioritized, item.RequestedBy, positions,
                    positions*Context.PointManager.SongPrioPoints);
            }
        }

        #endregion
    }
}