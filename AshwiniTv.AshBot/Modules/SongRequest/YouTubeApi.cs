﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using AshwiniTv.Shared;
using Newtonsoft.Json.Linq;

// ReSharper disable PossibleNullReferenceException

namespace AshwiniTv.AshBot.Modules.SongRequest
{
    public static class YouTubeApi
    {
        private static readonly Regex RxH = new Regex("\\d+H", RegexOptions.Compiled);
        private static readonly Regex RxM = new Regex("\\d+M", RegexOptions.Compiled);
        private static readonly Regex RxS = new Regex("\\d+S", RegexOptions.Compiled);

        public static bool RetrieveVideoData(ApiSong song, string streamerCountry)
        {
            var url =
                $"https://www.googleapis.com/youtube/v3/videos?id={song.TrackId}&key={TokenManager.YouTubeDataApi}" +
                "&part=snippet,contentDetails,status" +
                "&fields=pageInfo/totalResults,items/snippet(title,publishedAt),items/contentDetails(duration,regionRestriction),items/status/embeddable";

            var jsonData = Encoding.UTF8.GetString(DownloadData(url));

            if (!string.IsNullOrEmpty(jsonData)) return ParseJson(jsonData, song, streamerCountry);
            song.Error = ApiSong.SongError.HostError;
            return false;
        }

        public static bool RetrievePlaylist(string playlistId, ref string playlistName, ref ISet<string> videoIds)
        {
            var url =
                $"https://www.googleapis.com/youtube/v3/playlists?part=snippet&id={playlistId}&key={TokenManager.YouTubeDataApi}";

            var jsonData = Encoding.UTF8.GetString(DownloadData(url));
            if (string.IsNullOrEmpty(jsonData)) return false;
            var responseObject = JObject.Parse(jsonData);

            playlistName = GetPlaylistTitle(responseObject);

            url =
                $"https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlistId}&key={TokenManager.YouTubeDataApi}" +
                "&part=contentDetails&maxResults=50";

            jsonData = Encoding.UTF8.GetString(DownloadData(url));

            if (string.IsNullOrEmpty(jsonData)) return false;
            responseObject = JObject.Parse(jsonData);
            var resultCount = GetResultCount(responseObject);

            if (resultCount == 0) return false;

            var videoList = new HashSet<string>();

            do
            {
                var ids = responseObject.SelectTokens("$.items[*].contentDetails.videoId").Values<string>();
                foreach (var id in ids) videoList.Add(id);

                var nextPageToken = GetNextPageToken(responseObject);
                if (string.IsNullOrEmpty(nextPageToken)) break;

                url =
                    $"https://www.googleapis.com/youtube/v3/playlistItems?playlistId={playlistId}&key={TokenManager.YouTubeDataApi}" +
                    $"&part=contentDetails&maxResults=50&pageToken={nextPageToken}";
                jsonData = Encoding.UTF8.GetString(DownloadData(url));
                if (string.IsNullOrEmpty(jsonData)) break;
                responseObject = JObject.Parse(jsonData);
                resultCount = GetResultCount(responseObject);
            } while (resultCount > 0);

            videoIds = videoList;
            return true;
        }

        public static bool VideoSearch(ApiSong song, string streamerCountry)
        {
            var url =
                $"https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q={song.Name}&type=video&key={TokenManager.YouTubeDataApi}";
            var jsonData = Encoding.UTF8.GetString(DownloadData(url));
            if (string.IsNullOrEmpty(jsonData))
            {
                song.Error = ApiSong.SongError.HostError;
                return false;
            }

            var ytResponse = JObject.Parse(jsonData);
            var vidId = ytResponse.SelectToken("$.items[0].id.videoId");

            if (vidId == null) return false;

            song.Name = string.Empty;
            song.TrackId = vidId.Value<string>();
            song.Source = ApiSong.SongSource.YouTube;
            return RetrieveVideoData(song, streamerCountry);
        }

        private static bool ParseJson(string json, ApiSong song, string streamerCountry)
        {
            var ytResponse = JObject.Parse(json);
            var totalResults = ytResponse.SelectToken("$.pageInfo.totalResults");

            if (totalResults == null || totalResults.Value<int>() == 0)
            {
                song.Error = ApiSong.SongError.DoesNotExist;
                return false;
            }

            var item = ytResponse.SelectToken("$.items[0]") as JObject;
            var snippet = item["snippet"] as JObject;

            var publishedAt = snippet["publishedAt"];
            song.CreatedTime = song.ModifiedTime = publishedAt.Value<DateTime>();

            var title = snippet["title"];
            song.Name = title.Value<string>();

            var contentDetails = item["contentDetails"] as JObject;
            song.Length = ParseDuration(contentDetails["duration"].Value<string>());

            var regionRestriction = contentDetails["regionRestriction"] as JObject;

            if (regionRestriction != null)
            {
                var allowed = regionRestriction["allowed"] as JArray;

                if (allowed != null &&
                    !allowed.Values<string>().Contains(streamerCountry, StringComparer.InvariantCultureIgnoreCase))
                {
                    song.Error = ApiSong.SongError.RegionRestricted;
                    return false;
                }

                var blocked = regionRestriction["blocked"] as JArray;

                if (blocked != null &&
                    blocked.Values<string>().Contains(streamerCountry, StringComparer.InvariantCultureIgnoreCase))
                {
                    song.Error = ApiSong.SongError.RegionRestricted;
                    return false;
                }
            }

            if (!item.SelectToken("$.status.embeddable").Value<bool>())
            {
                song.Error = ApiSong.SongError.NotEmbeddable;
                return false;
            }

            song.Link = null;
            song.Error = ApiSong.SongError.None;
            return true;
        }

        private static TimeSpan ParseDuration(string duration)
        {
            try
            {
                var mHours = RxH.Match(duration);
                var mMinutes = RxM.Match(duration);
                var mSeconds = RxS.Match(duration);

                var hours = 0;
                var minutes = 0;
                var seconds = 0;

                if (mHours.Success) hours = int.Parse(mHours.Value.Substring(0, mHours.Length - 1));
                if (mMinutes.Success) minutes = int.Parse(mMinutes.Value.Substring(0, mMinutes.Length - 1));
                if (mSeconds.Success) seconds = int.Parse(mSeconds.Value.Substring(0, mSeconds.Length - 1));

                return new TimeSpan(hours, minutes, seconds);
            }
            catch (Exception)
            {
                return TimeSpan.Zero;
            }
        }

        private static byte[] DownloadData(string url)
        {
            using (var wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.UserAgent, "Ashwini.tv/WiniBot v2 (gzip)");
                wc.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                return UnpackData(wc.DownloadData(url));
            }
        }

        private static byte[] UnpackData(byte[] gzippedData)
        {
            using (var result = new MemoryStream())
            {
                var input = new MemoryStream(gzippedData);
                using (var gz = new GZipStream(input, CompressionMode.Decompress)) gz.CopyTo(result);
                return result.ToArray();
            }
        }

        private static int GetResultCount(JToken responseObject)
        {
            var resultCount = responseObject.SelectToken("$.pageInfo.resultsPerPage");
            return resultCount?.Value<int>() ?? 0;
        }

        private static string GetNextPageToken(JObject responseObject)
        {
            var token = responseObject["nextPageToken"];
            return token?.Value<string>();
        }

        private static string GetPlaylistTitle(JToken responseObject)
        {
            var title = responseObject.SelectToken("$.items[0].snippet.title");
            return title?.Value<string>();
        }
    }
}