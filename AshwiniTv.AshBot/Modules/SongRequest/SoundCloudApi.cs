﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Text.RegularExpressions;
using AshwiniTv.Shared;
using Newtonsoft.Json;

namespace AshwiniTv.AshBot.Modules.SongRequest
{
    public static class SoundCloudApi
    {
        private const string ResolveUrl = "https://api.soundcloud.com/resolve.json?url={0}&client_id={1}";
        private static readonly Regex RxStripDomain = new Regex(@"https?://soundcloud\.com/", RegexOptions.Compiled);

        public static bool RetrieveVideoData(ApiSong song)
        {
            string jsonUrl;
            string jsonData;

            var resolveRequest =
                WebRequest.CreateHttp(string.Format(ResolveUrl, Uri.EscapeDataString(song.Link),
                    TokenManager.SoundCloudClientId));
            resolveRequest.Referer = "https://www.ashwini.tv";
            resolveRequest.AllowAutoRedirect = false;

            try
            {
                using (var resolveResponse = (HttpWebResponse) resolveRequest.GetResponse())
                {
                    if (resolveResponse.StatusCode != HttpStatusCode.Found)
                    {
                        song.Error = ApiSong.SongError.HostError;
                        return false;
                    }

                    jsonUrl = resolveResponse.Headers[HttpResponseHeader.Location];

                    if (!jsonUrl.Contains("/tracks/"))
                    {
                        song.Error = ApiSong.SongError.NotATrack;
                        return false;
                    }
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError &&
                    ((HttpWebResponse) ex.Response).StatusCode == HttpStatusCode.NotFound)
                {
                    song.Error = ApiSong.SongError.DoesNotExist;
                }
                else song.Error = ApiSong.SongError.HostError;

                return false;
            }

            using (var wc = new WebClient()) jsonData = wc.DownloadString(jsonUrl);

            if (!string.IsNullOrEmpty(jsonData)) return ParseJson(jsonData, song);
            song.Error = ApiSong.SongError.HostError;
            return false;
        }

        public static string StripSoundCloudLink(string link)
        {
            return RxStripDomain.Replace(link, string.Empty);
        }

        private static bool ParseJson(string json, ApiSong song)
        {
            try
            {
                var track = JsonConvert.DeserializeObject<SoundCloudTrack>(json);

                if (track.EmbeddableBy != "all")
                {
                    song.Error = ApiSong.SongError.NotEmbeddable;
                    return false;
                }

                song.Name = track.Title;
                song.TrackId = track.Id.ToString();
                song.Link = StripSoundCloudLink(track.Link);
                song.Length = TimeSpan.FromMilliseconds(track.Duration);

                song.Error = ApiSong.SongError.None;
                return true;
            }
            catch
            {
                song.Error = ApiSong.SongError.HostError;
                return false;
            }
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
        private class SoundCloudTrack
        {
            public long Id { get; set; }
            public long Duration { get; set; }
            public string Title { get; set; }

            [JsonProperty("permalink_url")]
            public string Link { get; set; }

            [JsonProperty("embeddable_by")]
            public string EmbeddableBy { get; set; }
        }
    }
}