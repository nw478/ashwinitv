﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using AshwiniTv.Shared;
using Newtonsoft.Json;

namespace AshwiniTv.AshBot.Modules.SongRequest
{
    public static class VimeoApi
    {
        private const string VimeoReferrer = "Ashwini.tv/WiniBot v2";

        public static bool RetrieveVideoData(ApiSong song)
        {
            string jsonData;

            using (var wc = new WebClient())
            {
                wc.Headers.Add(HttpRequestHeader.Referer, VimeoReferrer);
                wc.Headers.Add(HttpRequestHeader.Authorization, "bearer " + TokenManager.VimeoApi);

                jsonData = wc.DownloadString($"https://api.vimeo.com/videos/{song.TrackId}");
            }

            if (!string.IsNullOrEmpty(jsonData)) return ParseJson(jsonData, song);
            song.Error = ApiSong.SongError.HostError;
            return false;
        }

        private static bool ParseJson(string json, ApiSong song)
        {
            try
            {
                var video = JsonConvert.DeserializeObject<VimeoVideo>(json);
                song.Name = video.Name;
                song.Length = TimeSpan.FromSeconds(video.Duration);
                song.CreatedTime = video.CreatedTime;
                song.ModifiedTime = video.ModifiedTime;
                song.Link = null;
                song.Error = ApiSong.SongError.None;
                return true;
            }
            catch (Exception)
            {
                song.Error = ApiSong.SongError.HostError;
                return false;
            }
        }

        [SuppressMessage("ReSharper", "ClassNeverInstantiated.Local")]
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
        private class VimeoVideo
        {
            public string Name { get; set; }
            public int Duration { get; set; }

            [JsonProperty("created_time")]
            public DateTime CreatedTime { get; set; }

            [JsonProperty("modified_time")]
            public DateTime ModifiedTime { get; set; }
        }
    }
}