﻿using System;
using System.Text.RegularExpressions;
using AshwiniTv.Shared;

namespace AshwiniTv.AshBot.Modules.SongRequest
{
    public class ApiSong
    {
        public enum SongError
        {
            None,
            HostError,
            DoesNotExist,
            RegionRestricted,
            NotEmbeddable,
            NotATrack
        }

        public enum SongSource
        {
            YouTube = 0,
            SoundCloud = 1,
            Vimeo = 2
        }

        private static readonly Regex RxYtIdV = new Regex("v={1}[a-zA-Z0-9\\-_]+", RegexOptions.Compiled);

        private static readonly Regex RxYtId =
            new Regex(
                @"^(?:https?://)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$",
                RegexOptions.Compiled);

        private static readonly Regex RxVmId = new Regex(@"\d+$", RegexOptions.Compiled);
        private static readonly TimeSpan SuspiciousLength = TimeSpan.FromMinutes(2);

        public ApiSong(string link)
        {
            Link = link;
            ParseLink();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string TrackId { get; set; }
        public SongSource Source { get; set; }
        public TimeSpan Length { get; set; }
        public bool Suspicious { get; set; }
        public SongWarningReason SuspiciousReason { get; set; }
        public bool Banned { get; set; }
        public bool BanOnRequest { get; set; }
        public SongError Error { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime ModifiedTime { get; set; }

        public void ParseLink()
        {
            if (TryProcessYouTubeLink() || TryProcessSoundCloudLink() || TryProcessVimeoLink()) return;

            ProcessYouTubeId();
        }

        public void ExamineSong()
        {
            if (Length < SuspiciousLength)
            {
                Suspicious = true;
                SuspiciousReason |= SongWarningReason.TooShort;
            }

            if (!(Math.Abs((DateTime.Now - CreatedTime).TotalDays) < 1.0)) return;
            Suspicious = true;
            SuspiciousReason |= SongWarningReason.TooRecent;
        }

        private bool TryProcessYouTubeLink()
        {
            var mId = RxYtId.Match(Link);
            if (mId.Success && mId.Groups.Count > 1) TrackId = mId.Groups[1].Value;
            else
            {
                mId = RxYtIdV.Match(Link);
                if (!mId.Success) return false;
                TrackId = mId.Value.Substring(2);
            }

            Source = SongSource.YouTube;
            return true;
        }

        private void ProcessYouTubeId()
        {
            TrackId = Link;
            Source = SongSource.YouTube;
        }

        private bool TryProcessSoundCloudLink()
        {
            if (!Link.Contains("soundcloud")) return false;
            Source = SongSource.SoundCloud;
            return true;
        }

        private bool TryProcessVimeoLink()
        {
            if (!Link.Contains("vimeo")) return false;

            var mId = RxVmId.Match(Link);
            if (string.IsNullOrEmpty(mId.Value)) return false;
            TrackId = mId.Value;

            Source = SongSource.Vimeo;
            return true;
        }
    }
}