﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public class ActivityWatcher : TimedModule
    {
        private readonly Dictionary<string, DateTime> _activityMap;
        private readonly Dictionary<string, DateTime> _permanentActivityMap;
        private readonly CultureInfo _enUs;

        public ActivityWatcher(ChannelContext context)
            : base(context, 60000)
        {
            _activityMap = new Dictionary<string, DateTime>();
            _permanentActivityMap = new Dictionary<string, DateTime>();
            _enUs = new CultureInfo("en-US");
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            lock (SyncLock)
            {
                _activityMap[message.Nick] = DateTime.Now;
                _permanentActivityMap[message.Nick] = DateTime.Now;
            }
            return true;
        }

        public void LastSeen(string name)
        {
            const string dateFormatString = "d MMM yyyy";
            const string timeFormatString = "H:mm";

            lock (SyncLock)
            {
                var user = DbUtils.EnsureUser(name);
                var here = Context.GetAllUsersInChannel().Contains(name);

                if (here)
                {
                    if (user.lastActive == null)
                        Context.SendMessage(ModuleResources.ActivityWatcher_HereNotTalking, name);
                    else
                        Context.SendMessage(ModuleResources.ActivityWatcher_LastSeenNow, name,
                            user.lastActive.Value.ToString(dateFormatString, _enUs),
                            user.lastActive.Value.ToString(timeFormatString, _enUs));
                    return;
                }

                if (user.lastSeen == null) Context.SendMessage(ModuleResources.ActivityWatcher_LastSeenNever, name);
                else
                {
                    if (user.lastActive == null)
                        Context.SendMessage(ModuleResources.ActivityWatcher_LastSeenNoActivity, name,
                            user.lastSeen.Value.ToString(dateFormatString, _enUs),
                            user.lastSeen.Value.ToString(timeFormatString, _enUs));
                    else
                        Context.SendMessage(ModuleResources.ActivityWatcher_LastSeen, name,
                            user.lastSeen.Value.ToString(dateFormatString, _enUs),
                            user.lastSeen.Value.ToString(timeFormatString, _enUs),
                            user.lastActive.Value.ToString(dateFormatString, _enUs),
                            user.lastActive.Value.ToString(timeFormatString, _enUs));
                }
            }
        }

        public IList<long> GetUsersActiveInDays(int days)
        {
            var today = DateTime.Today;
            using (var tits = new AshwiniTvEntities())
            {
                return
                    tits.Users.Where(u => DbFunctions.DiffDays(today, u.lastActive) <= days)
                        .Select(u => u.id).ToList();
            }
        }

        public IEnumerable<string> GetUsernamesActiveIn(TimeSpan time)
        {
            var tp = DateTime.Now - time;
            lock (SyncLock) return _permanentActivityMap.Where(pair => pair.Value >= tp).Select(pair => pair.Key);
        }

        public void RemoveUserActivity(string user)
        {
            lock (SyncLock) _permanentActivityMap.Remove(user);
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs args)
        {
            var users = Context.GetAllUsersInChannel();
            var nicknames = users as IList<string> ?? users.ToList();
            var now = DateTime.Now;

            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                // ReSharper disable once AccessToDisposedClosure
                foreach (var user in nicknames.Select(nick => DbUtils.EnsureUser(nick, tits)))
                {
                    DateTime lastActivity;
                    var username = user.username.Trim();
                    if (_activityMap.TryGetValue(username, out lastActivity))
                    {
                        user.lastActive = now;
                        _activityMap.Remove(username);
                    }

                    user.lastSeen = now;
                }

                foreach (var value in _activityMap)
                {
                    var user = DbUtils.EnsureUser(value.Key);
                    user.lastActive = value.Value;
                }

                _activityMap.Clear();

                tits.SaveChanges();
            }
        }
    }
}
