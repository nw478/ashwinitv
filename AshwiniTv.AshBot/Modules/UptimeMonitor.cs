﻿using System;
using System.Linq;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;
using AshwiniTv.Twitch;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class UptimeMonitor : TimedModule
    {
        private readonly Timer _offlineCheckTimer;
        private DateTime _detectedOffline;
        private bool _detectingOffline;

        public UptimeMonitor(ChannelContext context)
            : base(context, 30000)
        {
            var live = TwitchApi.IsChannelLive("ashwinitv", TokenManager.TwitchClientId).Result;
            StreamLive = live ?? false;

            _offlineCheckTimer = new Timer(300000) {AutoReset = false};
            _offlineCheckTimer.Elapsed += _offlineCheckTimer_Elapsed;
            _detectingOffline = false;
        }

        public bool StreamLive { get; private set; }

        public void ReportUptime()
        {
            if (!StreamLive) Context.SendMessage(ModuleResources.UptimeMonitor_StreamNotLive);
            else
            {
                var uptime = GetCurrentUptime();
                ReportFormattedUptime(uptime);
            }
        }

        public void ReportPastUptime(DateTime date)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var uptimes = tits.Uptimes.Where(u => u.day == date.Date);

                if (!uptimes.Any())
                    Context.SendMessage(ModuleResources.UptimeMonitor_WasNotLive, date.ToShortDateString());
                else
                {
                    foreach (var uptime in uptimes)
                        ReportPastFormattedUptime(date, TimeSpan.FromTicks(uptime.uptimeTicks));
                }
            }
        }

        private void ReportFormattedUptime(TimeSpan uptime)
        {
            if (uptime.Hours > 0)
                Context.SendMessage(ModuleResources.UptimeMonitor_LiveForHours, uptime.Hours, uptime.Minutes,
                    uptime.Seconds);
            else if (uptime.Minutes > 0)
                Context.SendMessage(ModuleResources.UptimeMonitor_LiveForMinutes, uptime.Minutes, uptime.Seconds);
            else Context.SendMessage(ModuleResources.UptimeMonitor_LiveForSeconds, uptime.Seconds);
        }

        private void ReportPastFormattedUptime(DateTime day, TimeSpan uptime)
        {
            if (uptime.Hours > 0)
                Context.SendMessage(ModuleResources.UptimeMonitor_WasLiveForHours, day.ToShortDateString(), uptime.Hours,
                    uptime.Minutes, uptime.Seconds);
            else if (uptime.Minutes > 0)
                Context.SendMessage(ModuleResources.UptimeMonitor_WasLiveForMinutes, day.ToShortDateString(),
                    uptime.Minutes, uptime.Seconds);
            else
                Context.SendMessage(ModuleResources.UptimeMonitor_WasLiveForSeconds, day.ToShortDateString(),
                    uptime.Seconds);
        }

        public TimeSpan GetCurrentUptime(AshwiniTvEntities context = null)
        {
            var tits = context ?? new AshwiniTvEntities();

            try
            {
                var dbStart = tits.UptimeProgresses.FirstOrDefault();
                if (dbStart == null) return TimeSpan.Zero;
                return DateTime.Now - dbStart.start;
            }
            finally
            {
                if (context == null) tits.Dispose();
            }
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            if (_detectingOffline) return;

            var live = TwitchApi.IsChannelLive("ashwinitv", TokenManager.TwitchClientId).Result;
            if (!live.HasValue) return;

            if (live.Value == StreamLive) return;
            if (live.Value)
            {
                using (var tits = new AshwiniTvEntities())
                {
                    tits.UptimeProgresses.Add(new UptimeProgress {date = DateTime.Today, start = DateTime.Now});
                    tits.SaveChanges();
                }

                Context.SendMessage(ModuleResources.UptimeMonitor_StreamLiveAnnounce);
                StreamLive = true;
                //Announcer.FixAnnounceTimes();
                //Announcer.SetAnnounceTimeLive();
            }
            else
            {
                _detectingOffline = true;
                _detectedOffline = DateTime.Now;
                _offlineCheckTimer.Start();
            }
        }

        private void _offlineCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _offlineCheckTimer.Stop();
            var live = TwitchApi.IsChannelLive("ashwinitv", TokenManager.TwitchClientId).Result;

            if (live.HasValue && !live.Value)
            {
                DateTime actualStart;
                using (var tits = new AshwiniTvEntities())
                {
                    var dbStart = tits.UptimeProgresses.FirstOrDefault();
                    if (dbStart == null)
                    {
                        Console.WriteLine(@"Uptime progress not found in DB.");
                        return;
                    }

                    Context.SendMessage(ModuleResources.UptimeMonitor_StreamOfflineAnnounce);
                    var streamLength = _detectedOffline - dbStart.start;

                    var thisUptime = new Uptime
                    {
                        uptimeTicks = streamLength.Ticks,
                        day = dbStart.start.Date
                    };

                    tits.Uptimes.Add(thisUptime);
                    tits.ClearUptimeProgress();
                    tits.SaveChanges();
                    actualStart = dbStart.start;
                }

                ReportPastUptime(actualStart.Date);
                Context.SongRequest.SetEnabled(false);
                StreamLive = false;
                //Announcer.SetAnnounceTimeOffline();
            }

            _detectingOffline = false;
        }
    }
}