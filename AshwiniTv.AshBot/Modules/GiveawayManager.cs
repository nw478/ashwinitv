﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class GiveawayManager : TimedModule
    {
        private const int TicketPrice = 1000;
        private const int MaxTickets = 10;

        private readonly HashSet<string> _secretWords;
        private readonly RNGCryptoServiceProvider _rng;

        public GiveawayManager(ChannelContext context)
            : base(context, 600000, true, false)
        {
            _secretWords = new HashSet<string>();
            _rng = new RNGCryptoServiceProvider();
            Reload();
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            lock (SyncLock)
            {
                if (!Context.UptimeMon.StreamLive || _secretWords.Count == 0 || ChannelContext.DevNames.Any(n => message.Nick.ToLower() == n)) return true;

                var comp = new CaseInsensitiveEqualityComparer();
                var word = string.Empty;
                var msgArray =
                    message.MessageArray.Select(m => new string(m.Where(c => !char.IsPunctuation(c)).ToArray()));

                if (!msgArray.Any(w =>
                {
                    if (!_secretWords.Contains(w, comp)) return false;
                    word = w;
                    return true;
                })) return true;

                word = word.ToLower();

                using (var tits = new AshwiniTvEntities())
                {
                    if (!GiveTicket(message.Nick, tits)) return true;
                    var sw = tits.SecretWords.FirstOrDefault(w => w.word == word);
                    if (sw != null) tits.SecretWords.Remove(sw);
                    tits.SaveChanges();
                    _secretWords.Remove(word);
                    Context.SendAction(ModuleResources.Giveaway_TicketWon, message.Nick, word);
                }

                return true;
            }
        }

        public void ShowInfo()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var text = tits.Announces.Find(10);
                Context.SendMessage(text.text);
            }
        }

        public void ShowSubs()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var incompleteGiveaways = tits.Giveaways.Where(g => g.winner == null);

                if (incompleteGiveaways.Any(g => g.subsToGo == 0)) Context.SendMessage(ModuleResources.Giveaway_SubsDone);
                else Context.SendMessage(ModuleResources.Giveaway_Subs, incompleteGiveaways.First().subsToGo);
            }
        }

        public void Treat(string name)
        {
            var chance = SubManager.IsSub(name) ? 10 : 5;

            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                var user = DbUtils.EnsureUser(name, tits);
                var treated = tits.Treats.Find(user.id);

                if (!Context.UptimeMon.StreamLive || treated != null)
                {
                    Context.SendMessage("( ° ͜ʖ͡°)╭∩╮");
                    return;
                }

                var roll = _rng.GetNextTrueRandomUInt32(100U);
                if (roll < chance)
                {
                    GiveTicket(name, tits);
                    Context.SendMessage(ModuleResources.Giveaway_TreatSuccess, name);
                }
                else Context.SendMessage("( ° ͜ʖ͡°)╭∩╮");

                tits.Treats.Add(new Treat {userId = user.id});
                tits.SaveChanges();
            }
        }

        public void Start()
        {
            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                if (tits.Giveaways.Count(g => g.day != null && g.winner == null) == 1) return;
                var giveaway = tits.Giveaways.FirstOrDefault(g => g.day == null);
                if (giveaway == null) return;
                giveaway.day = DateTime.Today;
                tits.SaveChanges();
                Context.SendMessage(ModuleResources.Giveaway_EntryStart);
                ModuleTimer.Start();
            }
        }

        public void Ticket(string username, int tickets = 1)
        {
            lock (SyncLock)
            {
                var currentPoints = Context.PointManager.GetUserPoints(username);
                if (currentPoints < TicketPrice)
                {
                    Context.SendMessage(ModuleResources.Giveaway_NoPoints, username, currentPoints);
                    return;
                }

                using (var tits = new AshwiniTvEntities())
                {
                    var giveaway = GetCurrentGiveaway(tits);
                    if (giveaway == null) return;

                    var user = DbUtils.EnsureUser(username, tits);
                    var ticketCount = tits.GiveawayEntries.Count(e => e.giveawayId == giveaway.id && e.userId == user.id);

                    if (ticketCount >= MaxTickets)
                    {
                        Context.SendMessage(ModuleResources.GiveAway_TicketLimitReached, username);
                        return;
                    }

                    var overflow = tickets > (MaxTickets - ticketCount);
                    tickets = Math.Min(tickets, MaxTickets - ticketCount);

                    for (var i = 0; i < tickets; ++i)
                    {
                        var entry = new GiveawayEntry()
                        {
                            id = Guid.NewGuid(),
                            giveawayId = giveaway.id,
                            userId = user.id,
                            ticket = true
                        };

                        tits.GiveawayEntries.Add(entry);
                        ++ticketCount;
                    }

                    Context.PointManager.InternalModifyUserPoints(username, -TicketPrice * tickets);
                    tits.SaveChanges();
                    Context.SendMessage(ModuleResources.Giveaway_TicketBought, username, tickets, ticketCount);
                    if (overflow) Context.SendMessage(ModuleResources.GiveAway_TicketLimitReached, username);
                }
            }
        }

        public bool GiveTicket(string username, AshwiniTvEntities tits = null)
        {
            lock (SyncLock)
            {
                var db = tits ?? new AshwiniTvEntities();

                try
                {

                    var giveaway = GetCurrentGiveaway(db);
                    if (giveaway == null) return false;

                    var user = DbUtils.EnsureUser(username, db);
                    var ticketCount = db.GiveawayEntries.Count(e => e.giveawayId == giveaway.id && e.userId == user.id);

                    if (ticketCount >= MaxTickets) return false;

                    var entry = new GiveawayEntry()
                    {
                        id = Guid.NewGuid(),
                        giveawayId = giveaway.id,
                        userId = user.id,
                        ticket = true
                    };

                    db.GiveawayEntries.Add(entry);
                    db.SaveChanges();

                    return true;
                }
                finally
                {
                    if (tits == null) db.Dispose();
                }
            }
        }

        public void GiveTicketToCops()
        {
            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                var count = tits.CopTicketCounts.Find(1);
                ++count.ticketCount;
                tits.SaveChanges();
            }
        }

        public void TicketCount(string username)
        {
            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                var giveaway = GetCurrentGiveaway(tits);
                if (giveaway == null) return;

                var user = DbUtils.EnsureUser(username, tits);
                var ticketCount = tits.GiveawayEntries.Count(e => e.giveawayId == giveaway.id && e.userId == user.id);

                var emote = string.Empty;
                switch (ticketCount)
                {
                        case 0:
                        emote = "MingLee"; break;
                        case 10:
                        emote = "PogChamp"; break;
                }

                Context.SendMessage(ModuleResources.Giveaway_CurrentTickets, username, ticketCount, emote);
            }
        }

        public void Winner()
        {
            ModuleTimer.Stop();

            using (var tits = new AshwiniTvEntities())
            {
                var giveaway = GetCurrentGiveaway(tits);
                if (giveaway == null) return;
                var entries =
                    tits.GiveawayEntries.Where(e => e.giveawayId == giveaway.id).Select(e => e.userId).ToList();

                entries.AddRange(SubManager.GetAllSubUserIDs());
                var activeUsers = Context.Activity.GetUsersActiveInDays(3);

                var eligibleEntries = entries.Where(t => activeUsers.Contains(t)).ToList();

                var rng = new RNGCryptoServiceProvider();
                var iWinner = rng.GetNextTrueRandomUInt32((uint)eligibleEntries.Count);

                var iiWinner = checked((int)iWinner);

                giveaway.winner = eligibleEntries[iiWinner];
                giveaway.day = DateTime.Today;
                tits.SaveChanges();

                var uWinner = tits.Users.Find(eligibleEntries[iiWinner]);

                Context.SendMessage(ModuleResources.Giveaway_WinnerIs1);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.Giveaway_WinnerIsDots);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.GiveAway_WinnerIsEmote);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.Giveaway_RollingFrom, eligibleEntries.Count);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.GiveAway_WinnerIsEmote);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.Giveaway_WinnerIsDots);
                Thread.Sleep(3000);
                Context.SendMessage(ModuleResources.Giveaway_WinnerIs3, uWinner.username.ToUpper());
            }
        }

        public void Reload()
        {
            lock (SyncLock)
            {
                _secretWords.Clear();
                using (var tits = new AshwiniTvEntities())
                {
                    foreach (var sw in tits.SecretWords) _secretWords.Add(sw.word);
                }
            }
        }

        public void ShowHint()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var hint = tits.SecretWordHints.FirstOrDefault();
                if (string.IsNullOrEmpty(hint?.text)) return;
                Context.SendMessage(hint.text);
            }
        }

        public void UpdateHint(string text)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var hint = tits.SecretWordHints.FirstOrDefault();
                if (hint != null) hint.text = text;
                else
                {
                    hint = new SecretWordHint
                    {
                        id = 1,
                        text = text
                    };

                    tits.SecretWordHints.Add(hint);
                }

                tits.SaveChanges();
            }
        }

        private static Giveaway GetCurrentGiveaway(AshwiniTvEntities tits)
        {
            return tits.Giveaways.FirstOrDefault(g => g.day != null && g.winner == null);
        }

        private class CaseInsensitiveEqualityComparer : EqualityComparer<string>
        {
            public override bool Equals(string x, string y)
            {
                return x.Equals(y, StringComparison.InvariantCultureIgnoreCase);
            }

            public override int GetHashCode(string obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}
