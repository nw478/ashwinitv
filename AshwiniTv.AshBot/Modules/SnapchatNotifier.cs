﻿using System.Collections.Concurrent;
using System.Threading.Tasks;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.AshBot.SnapchatNotify;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class SnapchatNotifier : Module
    {
        private readonly BlockingCollection<string> _users;

        public SnapchatNotifier(ChannelContext context)
            : base(context)
        {
            _users = new BlockingCollection<string>();
            Task.Factory.StartNew(Worker);
        }

        public void AddUser(string user)
        {
            _users.Add(user);
        }

        private void Worker()
        {
            while (true)
            {
                var user = _users.Take();
                using (var client = new NotifyServiceClient()) client.SendSnapchatTo(user);
                Task.Delay(10000).Wait();
                using (var client = new NotifyServiceClient()) client.Reset();
                Context.SendMessage(ModuleResources.SnapchatNotify_Sent, user);
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}