﻿using System;
using System.Linq;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Modules
{
    public class QuoteManager : Module
    {
        public QuoteManager(ChannelContext context)
            : base(context)
        {
        }

        public void AddQuote(string author, string text, string addedBy)
        {
            lock (SyncLock)
                using (var tits = new AshwiniTvEntities())
                using (new TableLock(tits, "Quotes"))
                {
                    var dbAddedBy = DbUtils.EnsureUser(addedBy, tits);

                    var allIds = tits.Quotes.Select(qu => qu.id);
                    var freeIds = from n in allIds where !allIds.Select(nu => nu).Contains(n + 1) orderby n select n + 1;
                    var qId = freeIds.First();

                    var q = new Quote
                    {
                        id = qId,
                        quoteFrom = author,
                        text = text,
                        addedBy = dbAddedBy.id,
                        timestamp = DateTime.Now
                    };

                    tits.Quotes.Add(q);
                    tits.SaveChanges();

                    Context.SendMessage(ModuleResources.QuoteManager_Added, qId);
                }
        }

        public void RemoveQuote(int id, string removedBy)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbQ = tits.Quotes.Find(id);

                if (dbQ == null)
                {
                    Context.SendMessage(ModuleResources.QuoteManager_IDNotFound, id);
                    return;
                }

                if (!Context.IsUserMod(removedBy) && dbQ.User.username != removedBy)
                {
                    Context.SendMessage(ModuleResources.QuoteManager_SelfRemoveOnly, removedBy);
                    return;
                }

                tits.Quotes.Remove(dbQ);
                tits.SaveChanges();

                Context.SendMessage(ModuleResources.QuoteManager_Removed, id);
            }
        }

        public void GetQuote(int id)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var q = tits.Quotes.Find(id);
                if (q == null)
                {
                    Context.SendMessage(ModuleResources.QuoteManager_IDNotFound, id);
                    return;
                }

                SendQuote(q);
            }
        }

        public void RandomQuote()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var q = tits.Quotes.OrderBy(r => Guid.NewGuid()).FirstOrDefault();

                if (q == null) Context.SendMessage(ModuleResources.QuoteManager_NoneExist);
                else SendQuote(q);
            }
        }

        private void SendQuote(Quote q)
        {
            Context.SendMessage("\"{0}\" - {1} - {2}", q.text, q.quoteFrom, q.timestamp.ToString("d"));
        }
    }
}