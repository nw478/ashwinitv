﻿using System;
using System.Timers;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public interface IModule : IDisposable
    {
        bool Enabled { get; set; }
        bool ProcessMessage(IrcMessageData message);
    }

    public abstract class Module : IModule
    {
        protected Module(ChannelContext context)
        {
            Context = context;
            SyncLock = new object();
        }

        protected ChannelContext Context { get; private set; }
        protected object SyncLock { get; private set; }
        public virtual bool Enabled { get; set; }

        public virtual bool ProcessMessage(IrcMessageData message)
        {
            return true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }
    }

    public abstract class TimedModule : Module
    {
        protected TimedModule(ChannelContext context, int timerIntervalMs, bool autoReset = true, bool startTimer = true)
            : base(context)
        {
            ModuleTimer = new Timer(timerIntervalMs)
            {
                AutoReset = autoReset
            };

            ModuleTimer.Elapsed += TimerElapsed;
            if (startTimer) ModuleTimer.Start();
        }

        protected Timer ModuleTimer { get; private set; }

        protected virtual void TimerElapsed(object sender, ElapsedEventArgs args)
        {
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposing) return;
            if (ModuleTimer == null) return;

            ModuleTimer.Stop();
            ModuleTimer.Close();
            ModuleTimer = null;
        }
    }
}