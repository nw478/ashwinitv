﻿using System;
using System.Linq;
using System.Timers;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class Announcer : TimedModule
    {
        public Announcer(ChannelContext context)
            : base(context, 60000)
        {
            Announce();
        }

        //public static void SetAnnounceTimeLive()
        //{
        //    SetAnnounceTime(1);
        //}

        //public static void SetAnnounceTimeOffline()
        //{
        //    SetAnnounceTime(6);
        //}

        //public static void FixAnnounceTimes()
        //{
        //    var now = DateTime.Now;
        //    var follow = now.Subtract(TimeSpan.FromMinutes(61));
        //    var vote = now.Subtract(TimeSpan.FromMinutes(31));
        //    var ask = now.Subtract(TimeSpan.FromMinutes(16));

        //    using (var tits = new AshwiniTvEntities())
        //    {
        //        foreach (var announce in tits.Announces.Where(a => a.id < 5)) announce.lastTime = follow;
        //        foreach (var announce in tits.Announces.Where(a => a.id > 4 && a.id < 9)) announce.lastTime = vote;
        //        tits.Announces.Single(a => a.id == 9).lastTime = ask;
        //        tits.SaveChanges();
        //    }
        //}

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Announce();
        }

        private void Announce()
        {
            var now = DateTime.Now;

            using (var tits = new AshwiniTvEntities())
            {
                foreach (var announce in tits.Announces.Where(a => a.enabled))
                {
                    if ((announce.lastTime + TimeSpan.FromTicks(announce.period)) >= now) continue;

                    if (announce.action) Context.SendAction(announce.text);
                    else Context.SendMessage(announce.text);
                    announce.lastTime = now;
                }

                tits.SaveChanges();
            }
        }

        //private static void SetAnnounceTime(int hours)
        //{
        //    using (var tits = new AshwiniTvEntities()) tits.SetAnnounceTime(hours);
        //}
    }
}