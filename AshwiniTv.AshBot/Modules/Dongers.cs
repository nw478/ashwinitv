﻿using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public class Dongers : Module
    {
        public Dongers(ChannelContext context)
            : base(context)
        {
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            React(message.Message, message.Nick);
            return true;
        }

        private void React(string message, string user)
        {
            if (message.ToLower().Contains("peaceful protest"))
            {
                Context.SendMessage(
                    @"(◕‿◕✿) Just a friendly reminder that protesting peacefully is against the law and violating this law is punishable by a stick up the ass. Spread the word.");
                return;
            }

            if (message.Contains(" HeyGuys") || message.Contains("HeyGuys ") || message.Contains("HeyGuys"))
            {
                Context.SendMessage(1000, "HeyGuys");
                return;
            }

            if (message.Contains(" wow") || message.Contains("wow ") || message.Contains("wow"))
            {
                Context.SendMessage(1000, "wow indeed ConcernDoge");
                return;
            }

            var access = Context.GetUserAccessLevel(user);

            if (message.Contains("(ง ͠° ͟ʖ ͡°)ง This is our chat, plebs (ง ͠° ͟ʖ ͡°)ง") &&
                (int) access >= (int) UserAccessLevel.Broadcaster)
            {
                Context.SendMessage(@"(ง •̀_•́)ง Yeah, beat it! (ง •̀_•́)ง");
            }
        }
    }
}