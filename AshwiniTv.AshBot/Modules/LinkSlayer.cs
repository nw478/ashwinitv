﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;
using AshwiniTv.AshBot.Commands;
using AshwiniTv.AshBot.Localization;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    [SuppressMessage("ReSharper", "EmptyEmbeddedStatement")]
    public sealed class LinkSlayer : TimedModule
    {
        private static readonly string[] CurrentMods =
        {
            "ashwinitv", "n1ghtsh0ck", "nyantouro", "figghter",
            "ashwini_pokemonbot", "winibutt", "hornpub"
        };

        public static readonly Regex UrlRegex =
            new Regex(
                @"(?:(?:https?|ftp):\/\/)*(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?",
                RegexOptions.Compiled);

        /*public static readonly Regex UrlRegex = new Regex(@"(https?|ftp)://(-\.)?([^\s/?\.#-]+\.?)+(/[^\s]*)?",
            RegexOptions.Compiled);*/

        private static readonly CommandInfo SrCommandInfo = CommandInfos.Get(typeof (SongRequestCommand));
        private readonly Dictionary<string, DateTime> _permittedUsers;

        public LinkSlayer(ChannelContext context)
            : base(context, 600000)
        {
            _permittedUsers = new Dictionary<string, DateTime>();
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            if (!Enabled) return true;

            var msg = message.Message.ToLower();
            if ((msg.Contains("youtube") || msg.Contains("youtu.be") || msg.Contains("soundcloud") ||
                 msg.Contains("vimeo")) && Context.SongRequest.Enabled &&
                SrCommandInfo.Keywords.Any(kw => kw == message.MessageArray[0].Substring(1).ToLower()))
                return true;

            if (!UrlRegex.IsMatch(message.Message)) return true;

            var user = message.Nick;

            if (CurrentMods.Any(u => u == user) || SubManager.IsRegular(user, true)) return true;

            lock (SyncLock)
            if (_permittedUsers.ContainsKey(user))
            {
                while (_permittedUsers.Remove(user)) ;
                return true;
            }

            Context.PurgeUser(user);
            Context.SendMessage(ModuleResources.LinkSlayer_UserPurged);
            return false;
        }

        public void PermitUser(string user)
        {
            user = user.ToLower();

            lock (SyncLock)
            {
                if (_permittedUsers.ContainsKey(user)) _permittedUsers[user] = DateTime.Now;
                else _permittedUsers.Add(user, DateTime.Now);
            }

            Context.SendMessage(ModuleResources.LinkSlayer_UserPermitted, user);
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            var now = DateTime.Now;

            lock (SyncLock)
            {
                var users = _permittedUsers.Where(u => u.Value.AddMinutes(10) < now);
                foreach (var user in users)
                {
                    while (_permittedUsers.Remove(user.Key)) ;
                }
            }
        }
    }
}