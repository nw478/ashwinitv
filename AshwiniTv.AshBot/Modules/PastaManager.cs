﻿using System;
using System.Linq;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class PastaManager : TimedModule
    {
        public PastaManager(ChannelContext context)
            : base(context, 3600000)
        {
            TimerElapsed(this, null);
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs args)
        {
            SendRandomPasta();
        }

        public void SendRandomPasta()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var p = tits.Pastas.Where(FilterPastas).OrderBy(r => Guid.NewGuid()).FirstOrDefault();
                if (p == null) return;

                if (p.action) Context.SendAction(p.text);
                else Context.SendMessage(p.text);
            }
        }

        public void AddPasta(string text)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var p = new Pasta
                {
                    id = Guid.NewGuid(),
                    text = text,
                    enabled = true,
                    action = false,
                    disabledOnStream = false
                };

                tits.Pastas.Add(p);
                tits.SaveChanges();
                Context.SendMessage(ModuleResources.PastaManager_PastaAdded);
            }
        }

        private bool FilterPastas(Pasta p)
        {
            var ok = p.enabled;
            if (Context.UptimeMon.StreamLive) ok = ok && !p.disabledOnStream;
            return ok;
        }
    }
}