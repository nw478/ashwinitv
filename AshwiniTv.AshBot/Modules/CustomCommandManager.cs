﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public class CustomCommandManager : Module
    {
        private readonly ConcurrentDictionary<string, CustomCommand> _commands;

        public CustomCommandManager(ChannelContext context)
            : base(context)
        {
            _commands = new ConcurrentDictionary<string, CustomCommand>();
            LoadCommands();
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            return !ParseMessage(message); // return false on success to stop other modules from processing
        }

        public void AddCommand(string keyword, string text, string user, UserAccessLevel level)
        {
            if (_commands.ContainsKey(keyword))
            {
                Context.SendMessage(ModuleResources.CustomCommand_AlreadyExists);
                return;
            }

            text = text.Trim();
            if (text.ToLower().StartsWith(".disconnect"))
            {
                Context.SendMessage(ModuleResources.CustomCommand_NotAllowed);
                return;
            }

            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = DbUtils.EnsureUser(user, tits);
                var dbCommand = new CustomCommand
                {
                    id = Guid.NewGuid(),
                    keyword = keyword,
                    text = text,
                    userID = dbUser.id,
                    useCount = 0,
                    access = (byte) level
                };

                tits.CustomCommands.Add(dbCommand);
                tits.SaveChanges();

                _commands.AddOrUpdate(keyword, dbCommand, (k, v) => dbCommand);
            }

            Context.SendMessage(ModuleResources.CustomCommand_Added);
        }

        public void RemoveCommand(string keyword, string removedBy)
        {
            CustomCommand localCommand;

            if (!_commands.TryRemove(keyword, out localCommand))
            {
                Context.SendMessage(ModuleResources.CustomCommand_DoesNotExist);
                return;
            }

            var editorAccess = Context.GetUserAccessLevel(removedBy);
            if (localCommand.@protected && (int) editorAccess < (int) UserAccessLevel.Broadcaster)
            {
                Context.SendMessage(ModuleResources.CustomCommand_Protected, removedBy, keyword);
                return;
            }

            using (var tits = new AshwiniTvEntities())
            {
                var dbCommand = tits.CustomCommands.Find(localCommand.id);
                if (dbCommand == null) return;

                tits.CustomCommands.Remove(dbCommand);
                tits.SaveChanges();
                Context.SendMessage(ModuleResources.CustomCommand_Removed);
            }
        }

        public void EditCommand(string keyword, UserAccessLevel newAccessLevel, string newText, string editor)
        {
            newText = newText.Trim();
            if (newText.ToLower().StartsWith(".disconnect"))
            {
                Context.SendMessage(ModuleResources.CustomCommand_NotAllowed);
                return;
            }

            CustomCommand localCommand;

            if (!_commands.TryRemove(keyword, out localCommand))
            {
                Context.SendMessage(ModuleResources.CustomCommand_DoesNotExist);
                return;
            }

            var editorAccess = Context.GetUserAccessLevel(editor);
            if (localCommand.@protected && (int) editorAccess < (int) UserAccessLevel.Broadcaster)
            {
                Context.SendMessage(ModuleResources.CustomCommand_Protected, editor, keyword);
                return;
            }

            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = DbUtils.EnsureUser(editor, tits);
                var dbCommand = tits.CustomCommands.Find(localCommand.id);

                dbCommand.access = (byte) newAccessLevel;
                dbCommand.text = newText;
                dbCommand.userID = dbUser.id;

                tits.SaveChanges();
                _commands.AddOrUpdate(keyword, dbCommand, (k, v) => dbCommand);
                Context.SendMessage(ModuleResources.CustomCommand_Edited);
            }
        }

        public void ToggleCommandProtection(string keyword, bool protect)
        {
            CustomCommand localCommand;

            if (!_commands.TryRemove(keyword, out localCommand))
            {
                Context.SendMessage(ModuleResources.CustomCommand_DoesNotExist);
                return;
            }

            using (var tits = new AshwiniTvEntities())
            {
                var dbCommand = tits.CustomCommands.Find(localCommand.id);
                dbCommand.@protected = protect;
                tits.SaveChanges();
                _commands.AddOrUpdate(keyword, dbCommand, (k, v) => dbCommand);
            }

            Context.SendMessage(
                protect ? ModuleResources.CustomCommand_NowProtected : ModuleResources.CustomCommand_NowUnprotected,
                keyword);
        }

        public void Creator(string keyword)
        {
            CustomCommand localCommand;

            if (!_commands.TryGetValue(keyword, out localCommand))
            {
                Context.SendMessage(ModuleResources.CustomCommand_DoesNotExist);
                return;
            }

            using (var tits = new AshwiniTvEntities())
                Context.SendMessage(ModuleResources.CustomCommand_Who, keyword,
                    tits.Users.Find(localCommand.userID).username);
        }

        private bool ParseMessage(IrcMessageData message)
        {
            if (message.Message[0] != Context.CommandPrefix) return false;

            var keyword = message.MessageArray[0].Substring(1).ToLower();
            CustomCommand command;

            if (!_commands.TryGetValue(keyword, out command)) return false;
            if (!VerifyAccess(message.Nick, command.access)) return false;
            if (Context.UptimeMon.StreamLive && command.disabledOnStream) return false;

            ++command.useCount;
            var text = command.text;

            text = text.Replace("@touser@", message.MessageArray.Length > 1 ? message.MessageArray[1] : string.Empty);
            text = text.Replace("@count@", command.useCount.ToString()).Replace("@cnt@", command.useCount.ToString());
            text = text.Replace("@user@", message.Nick);

            Context.SendMessage(text);
            Task.Run(() => UpdateUseCount(command));
            return true;
        }

        private bool VerifyAccess(string user, byte requiredAccess)
        {
            if (Context.IsUserIgnored(user)) return false;
            var access = Context.GetUserAccessLevel(user);
            return (byte) access >= requiredAccess;
        }

        private void LoadCommands()
        {
            _commands.Clear();

            using (var tits = new AshwiniTvEntities())
            {
                foreach (var dbCmd in tits.CustomCommands)
                {
                    _commands.AddOrUpdate(dbCmd.keyword.ToLower(), dbCmd, (k, v) => dbCmd);
                }
            }
        }

        private static void UpdateUseCount(CustomCommand command)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbCmd = tits.CustomCommands.Find(command.id);
                ++dbCmd.useCount;
                tits.SaveChanges();
            }
        }
    }
}