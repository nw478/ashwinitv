﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Meebey.SmartIrc4net;
using Microsoft.AspNet.SignalR.Client;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class EmoteDisplay : Module
    {
        private readonly BlockingCollection<IrcMessageData> _messages;
        private readonly HubConnection _hubConnection;
        private readonly IHubProxy _hubProxy;

        private static readonly HashSet<string> EmotesToDisplay = new HashSet<string>
        {
            "Kappa"
        };

        public EmoteDisplay(ChannelContext context)
            : base(context)
        {
            _messages = new BlockingCollection<IrcMessageData>();
            _hubConnection = new HubConnection("https://www.ashwini.tv/signalr/hubs");
            _hubProxy = _hubConnection.CreateHubProxy("EmoteHub");

            Task.Factory.StartNew(Worker, TaskCreationOptions.LongRunning);

            _hubConnection.Start();
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            _messages.Add(message);
            return true;
        }

        private void DisplayEmote(string emote)
        {
            _hubProxy.Invoke("EmoteAppeared", emote).Wait();
        }

        private void Worker()
        {
            while (true)
            {
                var message = _messages.Take();

                lock (SyncLock)
                foreach (var word in message.MessageArray)
                {
                    if (EmotesToDisplay.Contains(word)) DisplayEmote(word);
                }
            }
        }
    }
}
