﻿using System;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using AshwiniTv.AshBot.Localization;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class ScreamerShield : Module
    {
        private static readonly string[] ScreamerDomains =
        {
            "strawpoii",
            "strawpoil",
            "strawpoli"
        };

        private static readonly string[] ScreamerLinks =
        {
            "https://akk.li/pics/anne.jpg",
            "http://akk.li/pics/anne.jpg",
            "http://fbcdn-sphotos-e-a.hubatka.cz/hphotos-ak-prn1/44530_491807354174208_665546187_n.jpg",
            "https://fbcdn-sphotos-e-a.hubatka.cz/hphotos-ak-prn1/44530_491807354174208_665546187_n.jpg"
        };

        public ScreamerShield(ChannelContext context)
            : base(context)
        {
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            var sMessage = message.Message.Trim();
            if (ScreamerDomains.Any(d => sMessage.Contains(d)) || ScreamerLinks.Any(l => sMessage.Contains(l)))
            {
                Context.SendMessage(ModuleResources.ScreamerShield_Timeout);
                Context.TimeoutUser(message.Nick, 600);
                return false;
            }

            var matches = LinkSlayer.UrlRegex.Matches(sMessage);
            if (matches.Count == 0) return true;

            if (
                !(from Match linkMatch in matches select UnrollRedirects(linkMatch.Value)).Any(
                    url => ScreamerDomains.Contains(url) || ScreamerLinks.Contains(url))) return true;
            Context.SendMessage(ModuleResources.ScreamerShield_Timeout);
            Context.TimeoutUser(message.Nick, 600);
            return false;
        }

        private static string UnrollRedirects(string url)
        {
            try
            {
                var redirect = true;

                while (redirect)
                {
                    var req = (HttpWebRequest) WebRequest.Create(url);
                    req.AllowAutoRedirect = false;
                    var res = (HttpWebResponse) req.GetResponse();

                    var code = (int) res.StatusCode;

                    redirect = code >= 300 && code < 400;
                    if (redirect) url = res.Headers[HttpResponseHeader.Location];
                }

                return url;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}