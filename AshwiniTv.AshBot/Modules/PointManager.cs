﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Modules
{
    public class PointManager : TimedModule
    {
        private static readonly string[] ExcludedUsers = {"datashbot", "winibot", "winibutt", "kappa_o_clock"};
        private Dictionary<string, int> _pointCfg;

        public PointManager(ChannelContext context)
            : base(context, 60000)
        {
            LoadPointConfig();
        }

        public int SongPrioPoints => _pointCfg["song_prio_points"];

        public void LoadPointConfig()
        {
            lock (SyncLock)
                using (var tits = new AshwiniTvEntities())
                    _pointCfg = tits.PointConfigs.ToDictionary(pc => pc.name, pc => pc.value);
        }

        public long GetUserPoints(string user)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = tits.Points.FirstOrDefault(p => p.User.username == user);
                return dbUser?.points ?? 0;
            }
        }

        public string[] GetTopUsernames()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var t = from p in tits.Points where p.points == tits.Points.Max(pp => pp.points) select p.User.username.Trim();
                return t.ToArray();
            }
        }

        public void ReportPointsForUser(string user, bool self)
        {
            var points = GetUserPoints(user);

            Context.SendMessage(
                self ? ModuleResources.PointManager_PointReport : ModuleResources.PointManager_3rdPartyPointReport, user,
                points);
        }

        public bool SongRequested(string user)
        {
            return InternalModifyUserPoints(user, -_pointCfg["song_points"]);
        }

        public bool SongPrioritized(string user, int positions)
        {
            var points = positions*_pointCfg["song_prio_points"];
            return InternalModifyUserPoints(user, -points);
        }

        public void RefundSongRequest(string user)
        {
            InternalModifyUserPoints(user, _pointCfg["song_points"]);
        }

        public void ModifyUserPoints(string user, int diff)
        {
            if (!InternalModifyUserPoints(user, diff)) return;
            switch (diff)
            {
                case 1:
                    Context.SendMessage(ModuleResources.PointManager_PointAdded, user);
                    break;
                case -1:
                    Context.SendMessage(ModuleResources.PointManager_PointRemoved, user);
                    break;
                default:
                    if (diff > 0) Context.SendMessage(ModuleResources.PointManager_PointsAdded, diff, user);
                    else Context.SendMessage(ModuleResources.PointManager_PointsRemoved, Math.Abs(diff), user);
                    break;
            }
        }

        public void BulkModifyUserPoints(IEnumerable<Tuple<string, long>> pointChanges, bool add)
        {
            using (var tits = new AshwiniTvEntities())
            {
                foreach (var change in pointChanges)
                {
                    var user = tits.Points.FirstOrDefault(u => u.User.username == change.Item1);
                    if (user == null) continue;
                    user.points += add ? change.Item2 : -change.Item2;
                }

                tits.SaveChanges();
            }
        }

        public void GiftPoints(string from, string to, int amount)
        {
            if (amount < 1)
            {
                Context.SendMessage(ModuleResources.PointManager_GiftAmountIsNegative);
                return;
            }

            if (from == to.ToLower()) return;

            lock (SyncLock)
                using (var tits = new AshwiniTvEntities())
                {
                    var recipient = tits.Points.FirstOrDefault(p => p.User.username == to.ToLower());
                    if (recipient == null) return;
                    var source = tits.Points.FirstOrDefault(p => p.User.username == from);
                    if (source == null /* wut */) return;

                    if (source.points < amount)
                    {
                        Context.SendMessage(ModuleResources.PointManager_NotEnoughPoints, from);
                        return;
                    }

                    source.points -= amount;
                    recipient.points += amount;
                    tits.SaveChanges();
                    Context.SendMessage(ModuleResources.PointManager_GiftDone, from, to, amount);
                }
        }

        public void ReportTopUsers(int number)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var topUsers =
                    (from p in tits.Points
                        where !ExcludedUsers.Contains(p.User.username)
                        orderby p.points descending
                        select p).Take(number).ToArray();
                Context.SendMessage(ModuleResources.PointManager_TopPointHeader, number);
                var sb = new StringBuilder();

                for (var i = 0; i < topUsers.Length; ++i)
                    sb.AppendFormat("{0}. {1} - {2} | ", i + 1, topUsers[i].User.username.Trim(), topUsers[i].points);
                sb.Length -= 3;

                Context.SendMessage(sb.ToString());
            }
        }

        public void UpdatePointsForNewSub(string newSub)
        {
            lock (SyncLock)
                using (var tits = new AshwiniTvEntities())
                {
                    var dbUser = tits.Users.FirstOrDefault(u => u.username == newSub && u.subscribed);
                    if (dbUser != null) return;
                    var ptData = tits.Points.FirstOrDefault(p => p.userID == dbUser.id);
                    if (ptData == null)
                    {
                        dbUser = DbUtils.EnsureUser(newSub, tits);

                        ptData = new Points
                        {
                            userID = dbUser.id,
                            points = _pointCfg["subbing_points"],
                            lastUpdate = DateTime.Now
                        };

                        tits.Points.Add(ptData);
                    }
                    else ptData.points += _pointCfg["subbing_points"];

                    tits.SaveChanges();
                }
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (Context.UptimeMon.StreamLive) UpdatePointsForUsersInChannel();
                //Console.WriteLine(@"Updated points");
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"Error updating points:" + ex.Message + @" : " + ex.InnerException?.Message);
            }
        }

        internal bool InternalModifyUserPoints(string user, long amount)
        {
            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = tits.Points.FirstOrDefault(p => p.User.username == user);
                if (dbUser == null) return false;
                if ((dbUser.points + amount) < 0) return false;
                dbUser.points += amount;
                tits.SaveChanges();
                return true;
            }
        }

        private void UpdatePointsForUsersInChannel()
        {
            var users = Context.GetAllUsersInChannel();
            var nicknames = users as string[] ?? users.ToArray();

            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                var updateTime = DateTime.Now;
                var pointPeriod = TimeSpan.FromSeconds(_pointCfg["point_period"]);
                var subPointPeriod = TimeSpan.FromSeconds(_pointCfg["sub_point_period"]);

                foreach (var nickname in nicknames)
                {
                    var isSubbed = SubManager.IsSub(nickname);
                    var dbPoints = tits.Points.FirstOrDefault(p => p.User.username == nickname);
                    if (dbPoints == null)
                    {
                        var dbUser = DbUtils.EnsureUser(nickname, tits);

                        var ptData = new Points
                        {
                            userID = dbUser.id,
                            points =
                                isSubbed
                                    ? _pointCfg["subbing_points"] + _pointCfg["base_sub_points"]
                                    : _pointCfg["base_points"],
                            lastUpdate = updateTime
                        };

                        tits.Points.Add(ptData);
                    }
                    else if ((dbPoints.lastUpdate + (isSubbed ? subPointPeriod : pointPeriod)) < updateTime)
                    {
                        dbPoints.points += isSubbed ? _pointCfg["base_sub_points"] : _pointCfg["base_points"];
                        dbPoints.lastUpdate = updateTime;
                    }
                }

                tits.SaveChanges();
            }
        }
    }
}