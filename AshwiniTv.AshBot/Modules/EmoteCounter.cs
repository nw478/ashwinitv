﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class EmoteCounter : TimedModule
    {
        private readonly List<EmoteData> _emoteData;
        private readonly Dictionary<string, int> _emoteIDs;
        private readonly BlockingCollection<IrcMessageData> _messageQueue;

        public EmoteCounter(ChannelContext context)
            : base(context, 60000)
        {
            _emoteIDs = new Dictionary<string, int>();
            _messageQueue = new BlockingCollection<IrcMessageData>();
            _emoteData = new List<EmoteData>();

            LoadEmoteIDs();

            Task.Factory.StartNew(Worker);
        }

        public override bool ProcessMessage(IrcMessageData message)
        {
            _messageQueue.Add(message);
            return true;
        }

        public void EmoteTotalForUser(string emote, string user, string requestedBy)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbEmoteObject = tits.Emotes.FirstOrDefault(e => e.emoteText == emote);
                if (dbEmoteObject == null)
                {
                    Context.SendMessage(ModuleResources.EmoteCounter_EmoteNotTracked, requestedBy, emote);
                    return;
                }

                var ecl =
                    tits.EmoteCountsLifetimes.FirstOrDefault(
                        ec => ec.emoteID == dbEmoteObject.id && ec.User.username == user);
                if (ecl == null)
                {
                    Context.SendMessage(ModuleResources.EmoteCounter_UserEmoteTotalNoData, requestedBy, emote);
                    return;
                }

                Context.SendMessage(ModuleResources.EmoteCounter_UserEmoteTotal, user, emote, ecl.count);
            }
        }

        public void EmoteTotal(string emote, string requestedBy)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbEmoteObject = tits.Emotes.FirstOrDefault(e => e.emoteText == emote);
                if (dbEmoteObject == null)
                {
                    Context.SendMessage(ModuleResources.EmoteCounter_EmoteNotTracked, requestedBy, emote);
                    return;
                }

                var totalCount =
                    tits.EmoteCountsLifetimes.Where(ecl => ecl.emoteID == dbEmoteObject.id).Sum(ecl => ecl.count);
                Context.SendMessage(ModuleResources.EmoteCounter_EmoteTotal, emote, totalCount);
            }
        }

        public void EmoteTotalLastDay()
        {
            using (var tits = new AshwiniTvEntities())
            {
                var limit = DateTime.Now.Subtract(TimeSpan.FromDays(1));
                var kappaCount =
                    tits.EmoteCounts.Where(ecl => ecl.emoteID == 1 && ecl.timestamp > limit).Sum(ecl => ecl.count);
                var dansGameCount =
                    tits.EmoteCounts.Where(ecl => ecl.emoteID == 3 && ecl.timestamp > limit).Sum(ecl => ecl.count);
                var pogChamp =
                    tits.EmoteCounts.Where(ecl => ecl.emoteID == 5 && ecl.timestamp > limit).Sum(ecl => ecl.count);
                var bibleThump =
                    tits.EmoteCounts.Where(ecl => ecl.emoteID == 6 && ecl.timestamp > limit).Sum(ecl => ecl.count);
                var sourPls =
                    tits.EmoteCounts.Where(ecl => ecl.emoteID == 8 && ecl.timestamp > limit).Sum(ecl => ecl.count);

                Context.SendMessage(ModuleResources.EmoteCounter_24hStats);
                Context.SendMessage("Kappa: {0}x DansGame: {1}x PogChamp: {2}x BibleThump: {3}x SourPls : {4}x",
                    kappaCount, dansGameCount, pogChamp, bibleThump, sourPls);
            }
        }

        public void EmoteTotalLastDayUser(string user)
        {
            using (var ech = new EmoteCountHelper(DateTime.Now.Subtract(TimeSpan.FromDays(1)), user))
            {
                var counts = new Dictionary<string, long>
                {
                    {"Kappa", ech.CountEmote(1)},
                    {"DansGame", ech.CountEmote(3)},
                    {"PogChamp", ech.CountEmote(5)},
                    {"BibleThump", ech.CountEmote(6)},
                    {"SourPls", ech.CountEmote(8)}
                };


                var stats = new StringBuilder();

                foreach (var pair in counts)
                {
                    stats.AppendFormat("{0} : {1}x ", pair.Key, pair.Value);
                }

                Context.SendMessage(ModuleResources.EmoteCounter_24hStatsUser, user);
                Context.SendMessage(stats.ToString());
            }
        }

        public void EmoteUser(string emote, string user, string requestedBy, string time)
        {
            var timeUnit = time.Last();
            int timeValue;
            if (!int.TryParse(time.Substring(0, time.Length - 1), out timeValue)) return;

            TimeSpan tsTime;

            switch (timeUnit)
            {
                case 'm':
                    tsTime = TimeSpan.FromMinutes(timeValue);
                    break;
                case 'h':
                    tsTime = TimeSpan.FromHours(timeValue);
                    break;
                case 'd':
                    tsTime = TimeSpan.FromDays(timeValue);
                    break;
                case 'M':
                    tsTime = TimeSpan.FromDays(timeValue*30);
                    break;
                default:
                    return;
            }

            List<long> counts;

            using (var tits = new AshwiniTvEntities())
            {
                var dbEmoteObject = tits.Emotes.FirstOrDefault(e => e.emoteText == emote);
                if (dbEmoteObject == null)
                {
                    Context.SendMessage(ModuleResources.EmoteCounter_EmoteNotTracked, requestedBy, emote);
                    return;
                }

                counts =
                    tits.EmoteCounts.Where(ec => ec.User.username == user && ec.emoteID == dbEmoteObject.id)
                        .OrderByDescending(ec => ec.timestamp)
                        .Select(ec => ec.count)
                        .ToList();
            }

            long count = 0;
            var minutes = Math.Min((int) tsTime.TotalMinutes, counts.Count);

            for (var i = 0; i < minutes; ++i) count += counts[i];

            Context.SendMessage(ModuleResources.EmoteCounter_UserEmote, user, emote, count);
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            var updateTime = DateTime.Now;

            lock (SyncLock)
                using (var tits = new AshwiniTvEntities())
                {
                    foreach (var ed in _emoteData)
                    {
                        var dbUser = DbUtils.EnsureUser(ed.User);

                        var lifetimeEd = tits.EmoteCountsLifetimes.Find(ed.Emote, dbUser.id);
                        if (lifetimeEd == null)
                        {
                            var ecl = new EmoteCountsLifetime
                            {
                                emoteID = ed.Emote,
                                count = ed.Count,
                                userID = tits.Users.Single(u => u.username == ed.User).id
                            };
                            tits.EmoteCountsLifetimes.Add(ecl);
                        }
                        else lifetimeEd.count += ed.Count;

                        var ec = new EmoteCount
                        {
                            emoteID = ed.Emote,
                            userID = dbUser.id,
                            count = ed.Count,
                            timestamp = updateTime
                        };

                        tits.EmoteCounts.Add(ec);
                    }

                    tits.SaveChanges();
                    _emoteData.Clear();
                }
        }

        private void LoadEmoteIDs()
        {
            _emoteIDs.Clear();

            using (var tits = new AshwiniTvEntities())
            {
                foreach (var dbEmote in tits.Emotes)
                {
                    _emoteIDs.Add(dbEmote.emoteText, dbEmote.id);
                }
            }
        }

        private void Worker()
        {
            while (true)
            {
                var msg = _messageQueue.Take();

                lock (SyncLock)
                    foreach (var word in msg.MessageArray)
                    {
                        int eId;
                        if (!_emoteIDs.TryGetValue(word, out eId)) continue;

                        var data = _emoteData.Find(ed => ed.User == msg.Nick && ed.Emote == eId);

                        if (data != null) ++data.Count;
                        else
                        {
                            data = new EmoteData
                            {
                                User = msg.Nick,
                                Emote = eId,
                                Count = 1
                            };
                            _emoteData.Add(data);
                        }
                    }
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private class EmoteData
        {
            public string User { get; set; }
            public int Emote { get; set; }
            public int Count { get; set; }
        }

        private sealed class EmoteCountHelper : IDisposable
        {
            private readonly DateTime _limit;
            private readonly AshwiniTvEntities _tits;
            private readonly long _userId;

            public EmoteCountHelper(DateTime limit, string user)
            {
                _limit = limit;
                _tits = new AshwiniTvEntities();
                _userId = DbUtils.EnsureUser(user).id;
            }

            public void Dispose()
            {
                _tits?.Dispose();
            }

            public long CountEmote(int emoteId)
            {
                try
                {
                    return
                        _tits.EmoteCounts.Where(
                            ecl => ecl.emoteID == emoteId && ecl.timestamp > _limit && ecl.userID == _userId)
                            .Sum(ecl => ecl.count);
                }
                catch
                {
                    return 0;
                }
            }
        }
    }
}