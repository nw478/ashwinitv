﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;

namespace AshwiniTv.AshBot.Modules
{
    public sealed class BankHeist : TimedModule
    {
        private const uint StartWaitTime = 2;
        private const uint HeistLength = 1;
        private const double WinMultiplier = 0.8;

        private static readonly int[] SuccessLevelUsers = {5, 10, 15, 20};
        private static readonly int[] SuccessChances = {50, 55, 60, 65, 70};
        private static readonly string[] AllIn = {"all-in", "allin"};
        private int _bankLevel;
        private uint _cdRemaining;

        private readonly List<HeistEntry> _entries;
        private readonly RNGCryptoServiceProvider _rng;
        private HeistState _state;

        public uint Cooldown { get; set; } = 15;

        public BankHeist(ChannelContext context)
            : base(context, 3*60000, false, false)
        {
            _state = HeistState.NotStarted;
            _entries = new List<HeistEntry>();
            _bankLevel = 0;
            _rng = new RNGCryptoServiceProvider();
        }

        public void AddEntry(string user, string sPoints)
        {
            sPoints = sPoints.ToLower();

            lock (SyncLock)
            {
                if (_state == HeistState.OnCooldown)
                {
                    Context.SendMessage(ModuleResources.BankHeist_CooldownEntry, _cdRemaining);
                    return;
                }

                if (_state == HeistState.Started)
                {
                    Context.SendMessage(ModuleResources.BankHeist_LateEntry, user);
                    return;
                }

                if (_entries.Count > 0 && _entries.FirstOrDefault(t => t.User == user) != null) return;

                var currentPoints = Context.PointManager.GetUserPoints(user);
                long points;

                if (!long.TryParse(sPoints, out points))
                {
                    if (AllIn.Any(ai => ai == sPoints))
                    {
                        points = currentPoints;
                        Context.SendMessage(ModuleResources.BankHeist_AllIn, user, points);
                    }
                    else return;
                }
                else if (Context.PointManager.GetTopUsernames().Any(name => name == user))
                {
                    Context.SendAction(ModuleResources.BankHeist_AllInOnly, user);
                    return;
                }

                if (currentPoints < points)
                {
                    Context.SendMessage(ModuleResources.BankHeist_CannotAfford, user, currentPoints);
                    return;
                }

                _entries.Add(new HeistEntry {User = user, Stake = points, TotalPoints = currentPoints});
                Context.PointManager.InternalModifyUserPoints(user, -points);

                if (_state == HeistState.NotStarted)
                {
                    Context.SendMessage(ModuleResources.BankHeist_Start, user, StartWaitTime);
                    _state = HeistState.WaitingForMore;
                    ModuleTimer.Interval = StartWaitTime*60000;
                    ModuleTimer.Start();
                }

                if (_entries.Count >= SuccessLevelUsers[_bankLevel] && _bankLevel < SuccessLevelUsers.Length)
                {
                    ++_bankLevel;
                }
            }
        }

        protected override void TimerElapsed(object sender, ElapsedEventArgs args)
        {
            lock (SyncLock)
                switch (_state)
                {
                    case HeistState.WaitingForMore:
                    {
                        _state = HeistState.Started;
                        ModuleTimer.Interval = HeistLength*60000;
                        ModuleTimer.Start();
                        if (_entries.Count > 1) Context.SendMessage(ModuleResources.BankHeist_OutcomeStart);
                    }
                        break;
                    case HeistState.Started:
                    {
                        try
                        {
                            ProcessHeistOutcome();
                            _state = HeistState.OnCooldown;
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(@"Heist exception: " + e.Message);
                        }
                        finally
                        {
                            ModuleTimer.Interval = 60000;
                            ModuleTimer.AutoReset = true;
                            _cdRemaining = Cooldown;
                            ModuleTimer.Start();
                        }
                    }
                        break;
                    case HeistState.OnCooldown:
                    {
                        --_cdRemaining;
                        if (_cdRemaining == 0)
                        {
                            ModuleTimer.Stop();
                            ModuleTimer.AutoReset = false;
                            _entries.Clear();
                            _state = HeistState.NotStarted;
                            Context.SendMessage(ModuleResources.BankHeist_CooldownOver);
                        }
                    }
                        break;
                }
        }

        private void ProcessHeistOutcome()
        {
            var bankLevel = Math.Min(_bankLevel, 4);

            if (_entries.Count == 1)
            {
                var roll = _rng.GetNextTrueRandomUInt32(100U);
                if (roll < SuccessChances[0])
                {
                    //var investmentRequired = Math.Ceiling(_entries[0].TotalPoints / 100.0);

                    //if (_entries[0].Stake >= investmentRequired)
                    //{
                    //    switch (roll)
                    //    {
                    //        case 0:
                    //            if (Context.Giveaways.GiveTicket(_entries[0].User))
                    //                Context.SendMessage(2000, ModuleResources.BankHeist_TicketFound, _entries[0].User);
                    //            else
                    //            {
                    //                Context.Giveaways.GiveTicketToCops();
                    //                Context.SendMessage(2000, ModuleResources.Bankheist_TicketLeftBehind, _entries[0].User);
                    //            }
                    //            break;
                    //        case 99:
                    //            Context.Giveaways.GiveTicketToCops();
                    //            Context.SendMessage(2000, ModuleResources.Bankheist_TicketCops, _entries[0].User);
                    //            break;
                    //    }
                    //}

                    const double mult = 1.0 + WinMultiplier;
                    var reward = (long) (GivePoints(mult)*mult) - _entries[0].Stake;
                    Context.SendMessage(ModuleResources.BankHeist_OutcomeSingleSuccess, _entries[0].User, reward);
                    return;
                }
                Context.SendMessage(ModuleResources.BankHeist_OutcomeSingleFail, _entries[0].User);
                return;
            }

            var sb = new StringBuilder(ModuleResources.BankHeist_Results);
            long stolen = 0;
            var results = GetWinners(bankLevel, sb, ref stolen);
            var winRatio = results.Count/(double) _entries.Count;

            Context.PointManager.BulkModifyUserPoints(results, true);

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (winRatio == 0.0)
            {
                Context.SendMessage(ModuleResources.BankHeist_OutcomeSuccess0);
            }
            else if (winRatio < 0.6)
            {
                Context.SendMessage(ModuleResources.BankHeist_OutcomeSuccess33, stolen);
                Context.SendMessage(sb.ToString());
            }
            else if (winRatio < 1.0)
            {
                Context.SendMessage(ModuleResources.BankHeist_OutcomeSuccess67, stolen);
                Context.SendMessage(sb.ToString());
            }
            else
            {
                Context.SendMessage(ModuleResources.BankHeist_OutcomeSuccess100, stolen);
                Context.SendMessage(sb.ToString());
            }

            Logger.QueueMessage(sb.ToString());
        }

        private long GivePoints(double multiplier)
        {
            var betSum = _entries.Sum(e => e.Stake);
            Context.PointManager.BulkModifyUserPoints(
                _entries.Select(e => new Tuple<string, long>(e.User, (long) (e.Stake*multiplier))), true);
            return betSum;
        }

        private IList<Tuple<string, long>> GetWinners(int bankLevel, StringBuilder sb, ref long totalStolen)
        {
            var results = new List<Tuple<string, long>>();

            foreach (var entry in _entries)
            {
                var roll = _rng.GetNextTrueRandomUInt32(100U);
                if (roll <= SuccessChances[bankLevel])
                {
                    var user = entry.User;
                    var points = entry.Stake;
                    var award = (long) Math.Round(points*WinMultiplier);
                    totalStolen += award;
                    results.Add(new Tuple<string, long>(user, points + award));
                    sb.AppendFormat("{0} ({1}), ", user, award);

                    var investmentRequired = Math.Ceiling(entry.TotalPoints/100.0);

                    if (entry.Stake < investmentRequired) continue;

                    switch (roll)
                    {
                        case 0:
                            if (Context.Giveaways.GiveTicket(user))
                                Context.SendMessage(2000, ModuleResources.BankHeist_TicketFound, user);
                            else
                            {
                                Context.Giveaways.GiveTicketToCops();
                                Context.SendMessage(2000, ModuleResources.Bankheist_TicketLeftBehind, user);
                            }
                            break;
                        case 99:
                            Context.Giveaways.GiveTicketToCops();
                            Context.SendMessage(2000, ModuleResources.Bankheist_TicketCops, user);
                            break;
                    }
                }
                else sb.AppendFormat("{0} (rip), ", entry.User);
            }

            sb.Length -= 2;
            return results;
        }

        private enum HeistState
        {
            NotStarted,
            WaitingForMore,
            Started,
            OnCooldown
        }

        private class HeistEntry
        {
            public string User { get; set; }
            public long Stake { get; set; }
            public long TotalPoints { get; set; }
        }
    }
}