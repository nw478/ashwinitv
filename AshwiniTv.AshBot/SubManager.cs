﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;
using AshwiniTv.Twitch;

namespace AshwiniTv.AshBot
{
    public static class SubManager
    {
        private static readonly HashSet<string> Subscribers = new HashSet<string>();
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private static readonly Timer RefreshTimer;
        private static readonly object SyncLock;

        private const int SubsPerGiveaway = 50;

        static SubManager()
        {
            RefreshTimer = new Timer(12*3600*1000) {AutoReset = true};
            RefreshTimer.Elapsed += RefreshTimer_Elapsed;
            RefreshTimer.Start();

            SyncLock = new object();
        }

        public static void AddSub(string username)
        {
            lock (SyncLock)
            {
                Subscribers.Add(username);
                Console.WriteLine(@"Added sub: {0}", username);
                UpdateConsoleTitle();
            }

            using (var tits = new AshwiniTvEntities())
            {
                var user = DbUtils.EnsureUser(username, tits);
                user.subscribed = true;
                tits.SaveChanges();
            }
        }

        public static bool IsSub(string username)
        {
            lock (SyncLock)
                if (Subscribers.Count > 0) return Subscribers.Contains(username);

            using (var tits = new AshwiniTvEntities())
                return tits.Users.Any(u => u.username == username && u.subscribed);
        }

        public static bool WasSub(string username)
        {
            using (var tits = new AshwiniTvEntities())
            {
                return tits.Users.Any(u => u.username == username && !u.subscribed && u.everSubscribed);
            }
        }

        public static bool IsRegular(string username, bool alsoSubRegular)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = tits.Users.FirstOrDefault(u => u.username == username);
                if (dbUser == null) return false;
                return dbUser.regular || (alsoSubRegular && dbUser.subscribed);
            }
        }

        public static bool? AddRegular(string username)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = tits.Users.FirstOrDefault(u => u.username == username);
                if (dbUser == null) return null;
                if (dbUser.regular) return false;
                dbUser.regular = true;
                tits.SaveChanges();
                return true;
            }
        }

        public static bool RemoveRegular(string username)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbUser = tits.Users.FirstOrDefault(u => u.username == username);
                if (dbUser == null) return false;
                if (!dbUser.regular) return false;
                dbUser.regular = false;
                tits.SaveChanges();
                return true;
            }
        }

        public static IEnumerable<long> GetAllSubUserIDs()
        {
            lock (SyncLock)
            using (var tits = new AshwiniTvEntities())
            {
                foreach (var sub in Subscribers) yield return tits.Users.First(u => u.username.Trim() == sub).id;
            }
        }

        public static void GetSubs(bool fullRefresh)
        {
            lock (SyncLock)
            {
                var offset = 0;

                if (fullRefresh) Subscribers.Clear();
                else offset = Subscribers.Count - 10;

                for (; offset < 10000; offset += 100)
                {
                    var subs =
                        TwitchApi.GetChannelSubscribers("ashwinitv", TokenManager.TwitchClientId,
                            TokenManager.TwitchAshToken, 100, offset).Result;
                    if (subs != null)
                        foreach (var sub in subs) Subscribers.Add(sub);

                    if (!fullRefresh || subs == null || subs.Count < 100) break;
                }

                if (Subscribers.Count > 0) UpdateSubDb();

                Console.WriteLine(@"Subcount: {0}", Subscribers.Count);
                UpdateConsoleTitle();
            }
        }

        public static void UpdateGiveaway(int numSubs, ChannelContext context)
        {
            lock (SyncLock)
            {
                using (var tits = new AshwiniTvEntities())
                {
                    int currentSubDelta;

                    var nextGiveaway = tits.Giveaways.FirstOrDefault(g => g.subsToGo > 0);
                    if (nextGiveaway == null)
                    {
                        currentSubDelta = Math.Min(SubsPerGiveaway, numSubs);

                        var lastGiveawayId =
                            tits.Giveaways.Where(g => g.subsToGo == 0).OrderByDescending(g => g.id).Select(g => g.id).First();
                        ++lastGiveawayId;

                        nextGiveaway = new Giveaway()
                        {
                            id = lastGiveawayId,
                            subsToGo = (byte) (SubsPerGiveaway - currentSubDelta)
                        };

                        tits.Giveaways.Add(nextGiveaway);
                    }
                    else
                    {
                        currentSubDelta = Math.Min(nextGiveaway.subsToGo, numSubs);
                        nextGiveaway.subsToGo -= (byte)currentSubDelta;
                    }

                    if (nextGiveaway.subsToGo == 0) context.SendMessage(CommonResources.Giveaway_Soon);
                    else context.SendMessage(CommonResources.Giveaway_SubsLeft, nextGiveaway.subsToGo);

                    tits.SaveChanges();
                    numSubs -= currentSubDelta;
                }

                if (numSubs > 0) UpdateGiveaway(numSubs, context);
            }
        }

        private static void UpdateSubDb()
        {
            using (var tits = new AshwiniTvEntities())
            {
                // ReSharper disable once AccessToDisposedClosure
                foreach (var dbUser in Subscribers.Select(s => DbUtils.EnsureUser(s, tits)))
                {
                    dbUser.subscribed = true;
                    dbUser.everSubscribed = true;
                }

                foreach (
                    var user in tits.Users.Where(user => user.subscribed && !Subscribers.Contains(user.username.Trim()))
                    )
                {
                    user.subscribed = false;
                }

                tits.SaveChanges();
            }
        }

        private static void RefreshTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            GetSubs(true);
        }

        private static void UpdateConsoleTitle()
        {
            Console.Title = @"AshBot - Subs: " + Subscribers.Count;
        }
    }
}