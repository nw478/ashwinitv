﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using AshwiniTv.AshBot.Commands;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.AshBot.Modules;
using AshwiniTv.AshBot.Modules.SongRequest;
using AshwiniTv.AshBot.Remote;
using AshwiniTv.AshBot.Utility;
using AshwiniTv.Shared;
using Meebey.SmartIrc4net;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace AshwiniTv.AshBot
{
    public class ChannelContext
    {
        internal static readonly string[] DevNames = {"n1ghtsh0ck", "ashwinitv"};
        internal static readonly string[] BroadcasterNames = {"ashwinitv"};
        private readonly Bot _bot;
        private readonly IModule[] _chatModules;
        private readonly ConcurrentDictionary<Type, DateTime> _commandTimeouts;
        private readonly ConcurrentHashSet<string> _ignoredUsers;
        private readonly ServiceHost _serviceHost;

        private readonly string[] _shitSymbols =
        {
            "Ỏ̷",
            "ฏ๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎๎ํํํํํํํํํํํํํํํํํํํํํํํํํํ",
            " ̢҉̶̴̘̮̥͉̯̞̻͇̮̮̰", "卐"
        };

        private readonly string[] _shitSymbolsNoMessage = {"anal", "tits"};
        // ReSharper disable once CollectionNeverQueried.Local
        private readonly List<Thread> _workerThreads;
        private volatile bool _ignorePlebs;

        public ChannelContext(Bot bot, Channel channel)
        {
            var ciInitTask = Task.Run((Action) CommandInfos.Initialize);
            var subInitTask = Task.Run(() => SubManager.GetSubs(true));

            _bot = bot;
            _commandTimeouts = new ConcurrentDictionary<Type, DateTime>();
            _ignoredUsers = new ConcurrentHashSet<string>();
            _workerThreads = new List<Thread>();
            _serviceHost = new ServiceHost(new BotService(this));

            CommandPrefix = '!';
            Channel = channel;
            IgnorePlebs = false;
            MessageQueue = new BlockingCollection<IrcMessageData>();

            SongRequest = new SongRequestProvider(this);
            Announcer = new Announcer(this);
            PointManager = new PointManager(this);
            UptimeMon = new UptimeMonitor(this);
            //Quotes = new QuoteManager(this);
            Snapchat = new SnapchatNotifier(this);
            Pastas = new PastaManager(this);
            Heist = new BankHeist(this);

            _chatModules = new IModule[]
            {
                ScreamerShield = new ScreamerShield(this) {Enabled = true},
                LinkSlayer = new LinkSlayer(this) {Enabled = true},
                CustomCommands = new CustomCommandManager(this) {Enabled = true},
                EmoteCounter = new EmoteCounter(this) {Enabled = true},
                //new EmoteDisplay(this) {Enabled = true},
                new Dongers(this) {Enabled = true},
                Giveaways = new GiveawayManager(this),
                Activity = new ActivityWatcher(this)
            };

            Task.WaitAll(ciInitTask, subInitTask);

            AddWorkers(4);

            _serviceHost.Open();

            Console.WriteLine(@"Init complete");
        }

        public Channel Channel { get; }
        public BlockingCollection<IrcMessageData> MessageQueue { get; }
        public char CommandPrefix { get; private set; }

        public bool IgnorePlebs
        {
            get { return _ignorePlebs; }
            set { _ignorePlebs = value; }
        }

        public CustomCommandManager CustomCommands { get; private set; }
        public SongRequestProvider SongRequest { get; private set; }
        public Announcer Announcer { get; private set; }
        public LinkSlayer LinkSlayer { get; private set; }
        public PointManager PointManager { get; private set; }
        public UptimeMonitor UptimeMon { get; private set; }
        public ScreamerShield ScreamerShield { get; private set; }
        //public QuoteManager Quotes { get; private set; }
        public EmoteCounter EmoteCounter { get; private set; }
        public SnapchatNotifier Snapchat { get; private set; }
        public PastaManager Pastas { get; private set; }
        public BankHeist Heist { get; private set; }
        public GiveawayManager Giveaways { get; private set; }
        public ActivityWatcher Activity { get; private set; }

        public bool AddIgnoredUser(string user)
        {
            return _ignoredUsers.Add(user.Trim().ToLower());
        }

        public bool RemoveIgnoredUser(string user)
        {
            return _ignoredUsers.Remove(user.Trim().ToLower());
        }

        public bool IsUserIgnored(string user)
        {
            return _ignoredUsers.Contains(user);
        }

        public UserAccessLevel GetUserAccessLevel(string user)
        {
            if (DevNames.Any(n => n == user)) return UserAccessLevel.Developer;
            if (BroadcasterNames.Any(n => n == user)) return UserAccessLevel.Broadcaster;
            if (IsUserMod(user)) return UserAccessLevel.Moderator;
            if (SubManager.IsSub(user)) return UserAccessLevel.Subscriber;
            return SubManager.IsRegular(user, false) ? UserAccessLevel.Regular : UserAccessLevel.Pleb;
        }

        public void AddWorkers(int workers)
        {
            //while (workers-- > 0) Task.Factory.StartNew(Worker, TaskCreationOptions.LongRunning);
            while (workers-- > 0)
            {
                var t = new Thread(Worker) {Name = "ChannelContext Worker Thread " + workers};
                t.Start();
                _workerThreads.Add(t);
            }
        }

        private void Worker()
        {
            var parser = new MessageParser(this);
            var monthsRegex = new Regex(@"subscribed for (?<months>\d+) months in a row!", RegexOptions.Compiled);
            var awayResubRegex = new Regex(@"(?<count>\d+) viewers? resubscribed while you were away!");

            try
            {
                while (true)
                {
                    var message = MessageQueue.Take();

                    if ((_shitSymbols.Any(s => message.Message.Contains(s)) ||
                         message.Message.Count(c => c == '█' || c == '░' || c == '▐') > 20) &&
                        GetUserAccessLevel(message.Nick) == UserAccessLevel.Pleb)
                    {
                        TimeoutUser(message.Nick, 600);
                        SendMessage(1000, CommonResources.SymbolTimeout);
                        continue;
                    }

                    /*if (_shitSymbolsNoMessage.Any(s => message.Message.ToLower().Contains(s)) &&
                        GetUserAccessLevel(message.Nick) == UserAccessLevel.Pleb)
                    {
                        PurgeUser(message.Nick);
                        continue;
                    }*/

                    if (message.Nick == "twitchnotify")
                    {
                        if (message.Message.EndsWith(@"just subscribed!"))
                        {
                            SendMessage(
                                SubManager.WasSub(message.MessageArray[0])
                                    ? CommonResources.Sub_WelcomeBack
                                    : CommonResources.Sub_WelcomeNew, message.MessageArray[0]);
                            SendMessage(2000, CommonResources.Sub_Hype);

                            SubManager.AddSub(message.MessageArray[0]);
                            //SubManager.UpdateGiveaway(1, this);
                        }
                        else if (monthsRegex.IsMatch(message.Message))
                        {
                            SubManager.AddSub(message.MessageArray[0]);
                            SendMessage(CommonResources.Sub_WelcomeRenew, message.MessageArray[0]);
                            SendMessage(2000, CommonResources.Sub_Hype);
                            //SubManager.UpdateGiveaway(1, this);
                        }
                        //else
                        //{
                        //    var m = awayResubRegex.Match(message.Message);
                        //    if (m.Success)
                        //    {
                        //        var count = int.Parse(m.Groups["count"].Value);
                        //        SubManager.UpdateGiveaway(count, this);
                        //    }
                        //}
                    }

                    if (_chatModules.Any(m => !m.ProcessMessage(message))) continue;

                    var command = parser.ParseMessage(message);

                    if (command == null || !CommandTimeoutOk(command, message.Nick)) continue;

                    try
                    {
                        command.Execute();
                    }
                    catch (CommandAccessDeniedException)
                    {
                        //SendMessage(CommonResources.Command_AccessDenied_Donger);
                    }
                    catch (InsufficientCommandArgsException)
                    {
                        // do nothing
                    }
                    catch (InvalidCastException)
                    {
                        // do nothing
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (AggregateException ex)
            {
                Console.Write(@"Worker exception: {0}", ex.InnerExceptions[0].Message);
            }
            catch (OperationCanceledException)
            {
                // do nothing, won't happen anyway Kappa
            }
        }

        private bool CommandTimeoutOk(IBotCommand command, string user)
        {
            if (BroadcasterNames.Any(n => n == user) || DevNames.Any(n => n == user)) return true;

            lock (_commandTimeouts)
            {
                DateTime lastUse;
                if (_commandTimeouts.TryGetValue(command.GetType(), out lastUse))
                {
                    if ((DateTime.Now - lastUse) < command.Delay) return !command.RestrictMods && IsUserMod(user);
                    _commandTimeouts[command.GetType()] = DateTime.Now;
                    return true;
                }

                _commandTimeouts.AddOrUpdate(command.GetType(), DateTime.Now, (k, v) => DateTime.Now);
                return true;
            }
        }

        #region CHAT

        public void SendMessage(string message, params object[] args)
        {
            _bot.SendChatMessage(Channel.Name, message, args);
        }

        public void SendMessage(int delayMs, string message, params object[] args)
        {
            Task.Delay(delayMs).ContinueWith(t => SendMessage(message, args));
        }

        public void SendAction(string message, params object[] args)
        {
            _bot.SendAction(Channel.Name, message, args);
        }

        public bool IsUserMod(string user)
        {
            return _bot.IsUserMod(user, Channel.Name);
        }

        public bool UserExists(string user)
        {
            return _bot.UserExists(user, Channel.Name);
        }

        public void BanUser(string user)
        {
            SendMessage(1000, ".ban {0}", user);
        }

        public void TimeoutUser(string user, int timeout)
        {
            SendMessage(1000, ".timeout {0} {1}", user, timeout);
        }

        public void PurgeUser(string user)
        {
            TimeoutUser(user, 1);
        }

        public void ReportAccessDenied(string user)
        {
            _bot.ReportAccessDenied(user, Channel.Name);
        }

        public IEnumerable<string> GetAllUsersInChannel()
        {
            return Twitch.TwitchApi.GetChatters("ashwinitv", TokenManager.TwitchClientId).Result;
        }

        public bool IsUserPresent(string user)
        {
            return GetAllUsersInChannel().Contains(user);
        }

        #endregion
    }
}