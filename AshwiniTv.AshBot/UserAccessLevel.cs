﻿using System;

namespace AshwiniTv.AshBot
{
    public enum UserAccessLevel
    {
        Pleb,
        User = Pleb,
        Regular,
        Reg = Regular,
        Subscriber,
        Sub = Subscriber,
        Moderator,
        Mod = Moderator,
        Broadcaster,
        Streamer = Broadcaster,
        Developer,
        Dev = Developer
    }

    public static class UserAccessUtils
    {
        public static UserAccessLevel? Parse(string accessString)
        {
            UserAccessLevel access;
            if (Enum.TryParse(accessString, true, out access)) return access;

            return null;
        }
    }
}