﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace AshwiniTv.AshBot.Utility
{
    public sealed class ConcurrentHashSet<T> : IDisposable
    {
        private readonly ReaderWriterLockSlim _rwl;
        private readonly HashSet<T> _set;

        public ConcurrentHashSet()
        {
            _rwl = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
            _set = new HashSet<T>();
        }

        public int Count
        {
            get { using (new ScopedReadLock(_rwl)) return _set.Count; }
        }

        public void Dispose()
        {
            _rwl?.Dispose();
        }

        public bool Add(T item)
        {
            using (new ScopedWriteLock(_rwl)) return _set.Add(item);
        }

        public void AddRange(IEnumerable<T> items)
        {
            using (new ScopedWriteLock(_rwl))
            {
                foreach (var item in items) _set.Add(item);
            }
        }

        public void Clear()
        {
            using (new ScopedWriteLock(_rwl)) _set.Clear();
        }

        public bool Contains(T item)
        {
            using (new ScopedReadLock(_rwl)) return _set.Contains(item);
        }

        public bool Remove(T item)
        {
            using (new ScopedWriteLock(_rwl)) return _set.Remove(item);
        }

        private sealed class ScopedWriteLock : IDisposable
        {
            private readonly ReaderWriterLockSlim _rwl;

            public ScopedWriteLock(ReaderWriterLockSlim rwl)
            {
                _rwl = rwl;
                _rwl.EnterWriteLock();
            }

            public void Dispose()
            {
                if (_rwl.IsWriteLockHeld) _rwl.ExitWriteLock();
            }
        }

        private sealed class ScopedReadLock : IDisposable
        {
            private readonly ReaderWriterLockSlim _rwl;

            public ScopedReadLock(ReaderWriterLockSlim rwl)
            {
                _rwl = rwl;
                _rwl.EnterReadLock();
            }

            public void Dispose()
            {
                if (_rwl.IsReadLockHeld) _rwl.ExitReadLock();
            }
        }
    }
}