﻿using System;
using System.IO;
using System.Text;

namespace AshwiniTv.AshBot.Utility
{
    public sealed class ConsoleWriter : TextWriter
    {
        private readonly TextWriter _consoleOut;

        public ConsoleWriter(TextWriter oldConsoleOut)
        {
            _consoleOut = oldConsoleOut;
        }

        public override Encoding Encoding => _consoleOut.Encoding;

        public override void WriteLine(string s)
        {
            var line = $"[{DateTime.Now.ToLongTimeString()}] {s}";
            _consoleOut.WriteLine(line);
        }
    }
}
