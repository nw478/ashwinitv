﻿using AshwiniTv.AshBot.Localization;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("permit")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class PermitCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);
            Context.LinkSlayer.PermitUser(Arg());
        }
    }

    [CommandKeywords("regular")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class RegularCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            switch (Arg())
            {
                case "add":
                    AddRegular();
                    break;
                case "check":
                    CheckRegular();
                    break;
                case "remove":
                    RemoveRegular();
                    break;
            }
        }

        private void AddRegular()
        {
            var regular = Arg(1);
            var res = SubManager.AddRegular(regular);

            if (!res.HasValue) return;
            Context.SendMessage(res.Value ? CommandResources.Regular_Added : CommandResources.Regular_AlreadyAdded,
                regular);
        }

        private void RemoveRegular()
        {
            var regular = Arg(1);
            Context.SendMessage(
                SubManager.RemoveRegular(regular)
                    ? CommandResources.Regular_Removed
                    : CommandResources.Regular_IsNotRegular, regular);
        }

        private void CheckRegular()
        {
            var regular = Arg(1);
            Context.SendMessage(
                SubManager.IsRegular(regular, false)
                    ? CommandResources.Regular_IsRegular
                    : CommandResources.Regular_IsNotRegular, regular);
        }
    }

    [CommandKeywords("ignore")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class IgnoreCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            var ignore = ArgOnOff();
            if (!ignore.HasValue) return;

            if (ignore.Value)
            {
                Context.AddIgnoredUser(Arg(1));
                Context.SendMessage(CommandResources.Ignore_Ignored, Arg(1));
            }
            else
            {
                Context.RemoveIgnoredUser(Arg(1));
                Context.SendMessage(CommandResources.Ignore_Unignored, Arg(1));
            }
        }
    }
}