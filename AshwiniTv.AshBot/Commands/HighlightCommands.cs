﻿using System;
using System.Linq;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("highlight")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class HighlightCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            ArgCountCheck(1);

            switch (Arg())
            {
                case "add":
                    Add();
                    break;
                case "list":
                    List();
                    break;
            }
        }

        private void Add()
        {
            AccessCheck(UserAccessLevel.Broadcaster);
            if (!Context.UptimeMon.StreamLive) return;

            string note = null;
            if (Args.Count > 1) note = GetMessageFragment(2);

            TimeSpan timestamp;

            using (var tits = new AshwiniTvEntities())
            {
                timestamp = Context.UptimeMon.GetCurrentUptime(tits);

                var highlight = new Highlight
                {
                    date = DateTime.Today,
                    id = Guid.NewGuid(),
                    note = note,
                    timestamp = timestamp.Ticks
                };

                tits.Highlights.Add(highlight);
                tits.SaveChanges();
            }

            Context.SendMessage(CommandResources.Highlight_Saved, timestamp.Hours, timestamp.Minutes, timestamp.Seconds);
        }

        private void List()
        {
            ArgCountCheck(2);

            DateTime date;
            if (!DateTime.TryParse(Arg(1), out date)) return;

            Context.SendMessage(CommandResources.Highlight_Heading);

            using (var tits = new AshwiniTvEntities())
            {
                var hl = tits.Highlights.Where(t => t.date == date).ToArray();
                if (hl.Length == 0)
                {
                    Context.SendMessage(CommandResources.Highlight_None);
                    return;
                }

                foreach (var t in hl)
                {
                    var timestamp = new TimeSpan(t.timestamp);
                    var message = string.Format(CommandResources.Highlight_ListItem, timestamp.Hours, timestamp.Minutes,
                        timestamp.Seconds);
                    if (t.note != null) message += " - " + t.note;
                    Context.SendMessage(message);
                }
            }
        }
    }
}