﻿using System;
using AshwiniTv.AshBot.Localization;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("songrequest", "sr", "requestsong", "srq", "ohgodohgodohgod")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class SongRequestCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount == 0)
            {
                Context.SendMessage(CommandResources.SongRequest_NoUrlGiven, User);
                return;
            }

            switch (Arg())
            {
                case "list":
                    SongListAction();
                    break;
                case "on":
                    TrySetSr(true);
                    break;
                case "off":
                    TrySetSr(false);
                    break;
                case "ban":
                    TryBanAction(true);
                    break;
                case "unban":
                    TryBanAction(false);
                    break;
                case "promote":
                    Promote();
                    break;
                case "prio":
                    Prioritize();
                    break;
                case "bansong":
                    BanSong();
                    break;
                case "banplaylist":
                    BanPlaylist();
                    break;
                default:
                    RequestSong();
                    break;
            }
        }

        private void TrySetSr(bool enabled)
        {
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.SongRequest.SetEnabled(enabled);
            Context.SendMessage(enabled ? CommandResources.SongRequest_Enabled : CommandResources.SongRequest_Disabled);
        }

        private void TryBanAction(bool ban)
        {
            AccessCheck(UserAccessLevel.Moderator);
            ArgCountCheck(2);

            var user = Arg(1);
            if (Context.SongRequest.BanUser(user, ban))
                Context.SendMessage(CommandResources.SongRequest_Ban, user,
                    ban ? CommandResources.SongRequest_Ban_Banned : CommandResources.SongRequest_Ban_Unbanned);
        }

        private void RequestSong()
        {
            var prioPoints = ArgCount == 2 ? ArgInt(1) : 0;
            Context.SongRequest.RequestSong(User, Arg(), prioPoints);
        }

        private void SongListAction()
        {
            if (ArgCount == 1)
            {
                Context.SongRequest.RequestSongFromPersonalList(User, null);
                return;
            }

            ArgCountCheck(2);

            switch (Arg(1))
            {
                case "add":
                    Context.SongRequest.AddCurrentSongToPersonalList(User, ArgCount == 2 ? null : Arg(2));
                    break;
                case "addnew":
                    SongListAdd();
                    break;
                default:
                    Context.SongRequest.RequestSongFromPersonalList(User, Arg(1));
                    break;
            }
        }

        private void SongListAdd()
        {
            ArgCountCheck(3);
            Context.SongRequest.AddNewSongToPersonalList(User, Arg(2), ArgCount == 3 ? null : Arg(3));
        }

        private void Promote()
        {
            ArgCountCheck(2);
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.SongRequest.PromoteSong(ArgLong(1));
        }

        private void Prioritize()
        {
            ArgCountCheck(3);
            var force = ArgCount == 4 && Arg(3) == "force";
            Context.SongRequest.PrioritizeSong(User, ArgLong(1), ArgInt(2), force);
        }

        private void BanSong()
        {
            ArgCountCheck(2);
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.SongRequest.BanSong(Arg(1));
        }

        private void BanPlaylist()
        {
            ArgCountCheck(2);
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.SongRequest.BanPlaylist(Arg(1));
        }
    }

    [CommandKeywords("srparam")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class SongRequestParamCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            switch (Arg())
            {
                case "sub":
                    SetSrSub();
                    break;
                case "points":
                    SetPoints();
                    break;
                case "maxlength":
                    SetMaxSongLength();
                    break;
                case "capacity":
                    SetMaxCapacity();
                    break;
                case "maxsongssub":
                    SetSongsPerSub();
                    break;
                case "maxsongspleb":
                    SetSongsPerPleb();
                    break;
                case "skipvote":
                    SetSkipVote();
                    break;
                case "skipvotecount":
                    SetSkipVoteCount();
                    break;
            }
        }

        private void SetSrSub()
        {
            var subOnly = ArgOnOff(1);
            if (!subOnly.HasValue) return;

            Context.SongRequest.SetSubOnly(subOnly.Value);
            Context.SendMessage(subOnly.Value
                ? CommandResources.SongRequest_SubOnlyOn
                : CommandResources.SongRequest_SubOnlyOff);
        }

        private void SetPoints()
        {
            var pointsOn = ArgOnOff(1);
            if (!pointsOn.HasValue) return;

            Context.SongRequest.SetRequestPoints(pointsOn.Value);
            Context.SendMessage(pointsOn.Value
                ? CommandResources.SongRequest_PointsOn
                : CommandResources.SongRequest_PointsOff);
        }

        private void SetMaxSongLength()
        {
            var seconds = ArgInt(1);

            var newLen = TimeSpan.FromSeconds(seconds);
            Context.SongRequest.SetMaxSongLength(newLen);
            Context.SendMessage(CommandResources.SongRequest_MaxLengthSet, newLen.ToString());
        }

        private void SetMaxCapacity()
        {
            var capacity = ArgInt(1);

            Context.SongRequest.SetQueueCapacity(capacity);
            Context.SendMessage(CommandResources.SongRequest_CapacitySet, capacity);
        }

        private void SetSongsPerSub()
        {
            var count = ArgInt(1);

            Context.SongRequest.SetSongsPerSub(count);
            Context.SendMessage(CommandResources.SongRequest_SongsPerUserSet, count);
        }

        private void SetSongsPerPleb()
        {
            var count = ArgInt(1);

            Context.SongRequest.SetSongsPerPleb(count);
            Context.SendMessage(CommandResources.SongRequest_SongsPerPlebSet, count);
        }

        private void SetSkipVote()
        {
            var skipVote = ArgOnOff(1);
            if (!skipVote.HasValue) return;

            Context.SongRequest.SetSkipVote(skipVote.Value);
            Context.SendMessage(skipVote.Value
                ? CommandResources.SongRequest_VoteSkipOn
                : CommandResources.SongRequest_VoteSkipOff);
        }

        private void SetSkipVoteCount()
        {
            var count = ArgInt(1);

            Context.SongRequest.SetSkipVoteCount(count);
            Context.SendMessage(CommandResources.SongRequest_VoteSkipCountSet, count);
        }
    }

    [CommandKeywords("skipsong")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class SkipSongBotCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount == 0) Context.SongRequest.SongSkipVote(User);

            ArgCountCheck(1);
            AccessCheck(UserAccessLevel.Moderator);

            if (Arg() != "skip") return;

            if (ArgCount == 1) Context.SongRequest.SkipSong(-1);
            else Context.SongRequest.SkipSong(ArgInt(1));
        }
    }

    [CommandKeywords("resetsongs")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class ResetSongsCommand : BotCommand
    {
        public override void Execute()
        {
            Context.SongRequest.ResetSongs();
        }
    }

    [CommandKeywords("currentsong", "song")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(10)]
    public sealed class CurrentSongCommand : BotCommand
    {
        public override void Execute()
        {
            Context.SongRequest.CurrentSong(User);
        }
    }

    [CommandKeywords("volume")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class VolumeCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);
            var volume = ArgInt();

            if (volume < 0) volume = 0;
            else if (volume > 100) volume = 100;

            Context.SongRequest.SetVolume(volume);
        }
    }
}