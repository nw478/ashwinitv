﻿using System;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Twitch;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("snapchat")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class SnapchatCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);

            var user = Arg();

            Context.Snapchat.AddUser(user);
        }
    }

    [CommandKeywords("title")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class TitleCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            var newTitle = Message.Substring(Message.IndexOf(' ') + 1);
            Context.SendMessage(
                TwitchApi.UpdateTitle("ashwinitv", TokenManager.TwitchClientId, newTitle, TokenManager.TwitchAshToken)
                    .Result
                    ? CommandResources.Title_Set
                    : CommandResources.Title_Fail, newTitle);
        }
    }

    [CommandKeywords("game")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class GameCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            var newGame = Message.Substring(Message.IndexOf(' ') + 1);
            Context.SendMessage(
                TwitchApi.UpdateGame("ashwinitv", TokenManager.TwitchClientId, newGame, TokenManager.TwitchAshToken)
                    .Result
                    ? CommandResources.Game_Set
                    : CommandResources.Game_Fail, newGame);
        }
    }

    [CommandKeywords("random")]
    [CommandAccess(UserAccessLevel.Regular)]
    [CommandDelay(10)]
    public sealed class RandomCommand : BotCommand
    {
        public override void Execute()
        {
            Context.Pastas.SendRandomPasta();
        }
    }

    [CommandKeywords("addrandom")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class AddRandomCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            Context.Pastas.AddPasta(GetMessageFragment(1));
        }
    }

    [CommandKeywords("lastseen")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    public sealed class LastSeenCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);
            Context.Activity.LastSeen(Arg());
        }
    }

    [CommandKeywords("suggestions")]
    [CommandAccess(UserAccessLevel.Mod)]
    public sealed class SuggestionsCommand : BotCommand
    {
        public override void Execute()
        {
            Context.SendMessage(CommandResources.Suggestion_1);
            Context.SendMessage(500, CommandResources.Suggestion_2);
            Context.SendMessage(500, CommandResources.Suggestion_4);
        }
    }
}