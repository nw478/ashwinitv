﻿using System;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("uptime")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(30)]
    public sealed class UptimeCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount == 0)
            {
                Context.UptimeMon.ReportUptime();
                return;
            }

            DateTime day;
            if (!DateTime.TryParse(Arg(), out day)) return;
            if (day > DateTime.Today) Context.SendMessage(@"FailFish");
            else Context.UptimeMon.ReportPastUptime(day);
        }
    }
}