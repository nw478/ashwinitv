﻿namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("points")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class PointsCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount == 0)
            {
                Context.PointManager.ReportPointsForUser(User, true);
                return;
            }

            AccessCheck(UserAccessLevel.Moderator);

            if (ArgCount == 1)
            {
                Context.PointManager.ReportPointsForUser(Arg(), false);
                return;
            }

            ArgCountCheck(2);
            AccessCheck(UserAccessLevel.Broadcaster);

            var points = ArgInt(1);
            Context.PointManager.ModifyUserPoints(Arg(), points);
        }
    }

    [CommandKeywords("giftpoints", "givepoints")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class PointGiftCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            var amount = ArgInt(1);
            Context.PointManager.GiftPoints(User, Arg(), amount);
        }
    }

    [CommandKeywords("top")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(30)]
    public sealed class TopPointsCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);
            var top = ArgInt();
            if (top > 10) return;
            Context.PointManager.ReportTopUsers(top);
        }
    }
}