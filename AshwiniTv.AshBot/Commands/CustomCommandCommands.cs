﻿namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("addcom")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class AddComCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            var firstArg = Arg();
            UserAccessLevel? desiredAccess = null;
            string keyword;

            if (firstArg.StartsWith("-ul="))
            {
                desiredAccess = UserAccessUtils.Parse(firstArg.Substring(4));
                keyword = Arg(1).TrimStart(Context.CommandPrefix);
            }
            else keyword = firstArg.TrimStart(Context.CommandPrefix);

            var commandText = Message.Substring(Message.IndexOf(' ') + 1);
            commandText = commandText.Substring(commandText.IndexOf(' ') + 1);
            if (desiredAccess.HasValue) commandText = commandText.Substring(commandText.IndexOf(' ') + 1);

            Context.CustomCommands.AddCommand(keyword, commandText, User, desiredAccess ?? UserAccessLevel.Pleb);
        }
    }

    [CommandKeywords("delcom")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class DelComCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);

            var keyword = Arg().TrimStart(Context.CommandPrefix);

            Context.CustomCommands.RemoveCommand(keyword, User);
        }
    }

    [CommandKeywords("editcom")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class EditComCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            var firstArg = Arg();
            UserAccessLevel? desiredAccess = null;
            string keyword;

            if (firstArg.StartsWith("-ul="))
            {
                desiredAccess = UserAccessUtils.Parse(firstArg.Substring(4));
                keyword = Arg(1).TrimStart(Context.CommandPrefix);
            }
            else keyword = firstArg.TrimStart(Context.CommandPrefix);

            var commandText = Message.Substring(Message.IndexOf(' ') + 1);
            commandText = commandText.Substring(commandText.IndexOf(' ') + 1);
            if (desiredAccess.HasValue) commandText = commandText.Substring(commandText.IndexOf(' ') + 1);

            Context.CustomCommands.EditCommand(keyword, desiredAccess ?? UserAccessLevel.Pleb, commandText, User);
        }
    }

    [CommandKeywords("protectcom")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class ProtectCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);
            var keyword = Arg().TrimStart('!');
            var protect = ArgOnOff(1) ?? true;

            Context.CustomCommands.ToggleCommandProtection(keyword, protect);
        }
    }

    [CommandKeywords("command")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class CommandCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            switch (Arg())
            {
                case "who":
                    Who();
                    break;
                case "add":
                    Add();
                    break;
                case "remove":
                    Remove();
                    break;
                case "edit":
                    Edit();
                    break;
            }
        }

        private void Who()
        {
            var keyword = Arg(1).TrimStart(Context.CommandPrefix);
            Context.CustomCommands.Creator(keyword);
        }

        private void Add()
        {
            ArgCountCheck(3);
            AccessCheck(UserAccessLevel.Moderator);

            var firstArg = Arg(1);
            UserAccessLevel? desiredAccess = null;
            string keyword;

            if (firstArg.StartsWith("-ul="))
            {
                desiredAccess = UserAccessUtils.Parse(firstArg.Substring(4));
                keyword = Arg(2).TrimStart(Context.CommandPrefix);
            }
            else keyword = firstArg.TrimStart(Context.CommandPrefix);

            var commandText = desiredAccess.HasValue ? GetMessageFragment(4) : GetMessageFragment(3);
            Context.CustomCommands.AddCommand(keyword, commandText, User, desiredAccess ?? UserAccessLevel.Pleb);
        }

        private void Remove()
        {
            AccessCheck(UserAccessLevel.Moderator);
            var keyword = Arg(1).TrimStart(Context.CommandPrefix);
            Context.CustomCommands.RemoveCommand(keyword, User);
        }

        private void Edit()
        {
            ArgCountCheck(3);
            AccessCheck(UserAccessLevel.Moderator);

            var firstArg = Arg(1);
            UserAccessLevel? desiredAccess = null;
            string keyword;

            if (firstArg.StartsWith("-ul="))
            {
                desiredAccess = UserAccessUtils.Parse(firstArg.Substring(4));
                keyword = Arg(2).TrimStart(Context.CommandPrefix);
            }
            else keyword = firstArg.TrimStart(Context.CommandPrefix);

            var commandText = desiredAccess.HasValue ? GetMessageFragment(4) : GetMessageFragment(3);
            Context.CustomCommands.EditCommand(keyword, desiredAccess ?? UserAccessLevel.Pleb, commandText, User);
        }
    }
}