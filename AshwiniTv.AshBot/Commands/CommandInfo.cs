﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AshwiniTv.AshBot.Commands
{
    public class CommandInfo
    {
        public CommandInfo(Type commandType)
        {
            CommandType = commandType;

            var keywordAttribute = commandType.GetCustomAttribute<CommandKeywordsAttribute>();
            var accessAttribute = commandType.GetCustomAttribute<CommandAccessAttribute>();
            var exclusive = commandType.GetCustomAttribute<DatashExclusiveAttribute>();
            var delayAttr = commandType.GetCustomAttribute<CommandDelayAttribute>();
            var restrictAttr = commandType.GetCustomAttribute<RestrictModsAttribute>();
            var offlineAttr = commandType.GetCustomAttribute<OfflineChatOnlyAttribute>();

            Keywords = keywordAttribute.Keywords;
            RequiredAccess = accessAttribute.RequiredAccess;
            DatashExclusive = exclusive != null;
            Delay = delayAttr?.Delay ?? TimeSpan.Zero;
            RestrictMods = restrictAttr != null;
            OfflineChatOnly = offlineAttr != null;
        }

        public string[] Keywords { get; }
        public UserAccessLevel RequiredAccess { get; }
        public Type CommandType { get; }
        public bool DatashExclusive { get;}
        public TimeSpan Delay { get;}
        public bool RestrictMods { get; }
        public bool OfflineChatOnly { get; }
    }

    public static class CommandInfos
    {
        private static readonly Dictionary<string, CommandInfo> Infos = new Dictionary<string, CommandInfo>();

        public static void Initialize()
        {
            var types =
                Assembly.GetExecutingAssembly()
                    .GetTypes()
                    .Where(t => t.IsSubclassOf(typeof (BotCommand)) && !t.IsAbstract);

            foreach (var info in types.Select(t => new CommandInfo(t)))
            {
                foreach (var keyword in info.Keywords) Infos.Add(keyword, info);
            }
        }

        public static CommandInfo Get(string keyword)
        {
            CommandInfo info;
            Infos.TryGetValue(keyword, out info);
            return info;
        }

        public static CommandInfo Get(Type commandType)
        {
            return Infos.Values.First(ci => ci.CommandType == commandType);
        }
    }
}