﻿using System;

namespace AshwiniTv.AshBot.Commands
{
    public class CommandAccessDeniedException : Exception
    {
        public CommandAccessDeniedException(bool report, ChannelContext context, string user)
        {
            if (report) context.ReportAccessDenied(user);
        }
    }

    public class InsufficientCommandArgsException : Exception
    {
    }
}