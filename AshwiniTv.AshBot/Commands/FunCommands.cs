﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using AshwiniTv.Twitch;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("dicksize")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    [OfflineChatOnly]
    public sealed class DicksizeCommand : BotCommand
    {
        private static readonly Dictionary<int, int> SizeChances = new Dictionary<int, int>();

        private static readonly ConcurrentDictionary<string, Tuple<int, int>> RiggedUsers =
            new ConcurrentDictionary<string, Tuple<int, int>>();

        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        static DicksizeCommand()
        {
            SizeChances.Add(5, 20);
            SizeChances.Add(10, 10);
            SizeChances.Add(15, 8);
            SizeChances.Add(20, 6);
            SizeChances.Add(30, 5);
            SizeChances.Add(40, 4);
            SizeChances.Add(50, 3);
            SizeChances.Add(60, 2);
            SizeChances.Add(80, 1);
            SizeChances.Add(100, 0);
        }

        public override void Execute()
        {
            if (User == "ashwinitv")
            {
                Context.SendMessage(CommandResources.DickSize_Ash);
                return;
            }

            int size;
            Tuple<int, int> sizeLimits;
            if (!RiggedUsers.TryGetValue(User, out sizeLimits)) sizeLimits = new Tuple<int, int>(0, 100);

            do
            {
                size = GetDickSize();
            } while (size < sizeLimits.Item1 || size > sizeLimits.Item2);

            if (size > 1) Context.SendMessage(CommandResources.DickSize_Default, User, size);
            else if (size == 1) Context.SendMessage(CommandResources.DickSize_1, User);
            else Context.SendMessage(CommandResources.DickSize_0, User);
        }

        public static void RigUser(string username, int minLength, int maxLength)
        {
            var sizes = new Tuple<int, int>(minLength, maxLength);
            RiggedUsers.AddOrUpdate(username, sizes, (k, v) => sizes);
        }

        public static void RemoveRiggedUser(string username)
        {
            Tuple<int, int> sizes;
            RiggedUsers.TryRemove(username, out sizes);
        }

        private static int GetDickSize()
        {
            var roll = Rng.GetNextTrueRandomUInt32(100U);
            return (from chance in SizeChances where roll <= chance.Key select chance.Value).FirstOrDefault();
        }
    }

    [CommandKeywords("roulette")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(10)]
    [RestrictMods]
    public sealed class RouletteCommand : BotCommand
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        public override void Execute()
        {
            var roll = Rng.GetNextTrueRandomUInt32(60U);
            Context.SendMessage(roll > 50 ? CommandResources.Roulette_Rekt : CommandResources.Roulette_Safe, User);
        }
    }

    [CommandKeywords("smashdatash")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class SmashDatAshCommand : BotCommand
    {
        public override void Execute()
        {
            if (Context.IsUserMod(User)) Context.SendMessage(CommandResources.SmashDatAsh_Mod, User);
            else
            {
                Context.TimeoutUser(User, 10);
                Context.SendMessage(CommandResources.SmashDatAsh_Rekt);
            }
        }
    }

    [CommandKeywords("winner")]
    [CommandAccess(UserAccessLevel.Moderator)]
    public sealed class WinnerCommand : BotCommand
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        public override void Execute()
        {
            var usersInChannel = Context.GetAllUsersInChannel();
            var activeUsers = Context.Activity.GetUsernamesActiveIn(TimeSpan.FromMinutes(15));
            var users = activeUsers.Intersect(usersInChannel).ToArray();
            var index = Rng.GetNextTrueRandomUInt32((uint) users.Length);
            while (users[index] == Bot.Name) index = Rng.GetNextTrueRandomUInt32((uint) users.Length);
            Context.SendMessage(CommandResources.Winner_Result);
            Context.SendMessage(1000, "{0}!", users[index]);
        }
    }

    [CommandKeywords("bankheist")]
    [CommandAccess(UserAccessLevel.User)]
    public sealed class BankheistCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (!Context.UptimeMon.StreamLive)
            {
                Context.SendMessage(CommandResources.Bankheist_OnlyOnline);
                return;
            }

            ArgCountCheck(1);
            Context.Heist.AddEntry(User, Arg());
        }
    }

    [CommandKeywords("heistcd")]
    [CommandAccess(UserAccessLevel.Broadcaster)]
    public sealed class BankheistCooldownCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);
            var cd = (uint)ArgInt();
            Context.Heist.Cooldown = cd;
            Context.SendMessage(CommandResources.Bankheist_CDSet, cd);
        }
    }

    [CommandKeywords("flip")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class FlipCommand : BotCommandWithArgsAndMessage
    {
        public static readonly Dictionary<char, char> FlipMap;

        static FlipCommand()
        {
            FlipMap = new Dictionary<char, char>
            {
                {'a', 'ɐ'},
                {'b', 'q'},
                {'c', 'ɔ'},
                {'d', 'p'},
                {'e', 'ǝ'},
                {'f', 'ɟ'},
                {'g', 'b'},
                {'h', 'ɥ'},
                {'i', 'ı'},
                {'j', 'ظ'},
                {'k', 'ʞ'},
                {'l', 'ן'},
                {'m', 'ɯ'},
                {'n', 'u'},
                {'o', 'o'},
                {'p', 'd'},
                {'q', 'b'},
                {'r', 'ɹ'},
                {'s', 's'},
                {'t', 'ʇ'},
                {'u', 'n'},
                {'v', 'ʌ'},
                {'w', 'ʍ'},
                {'x', 'x'},
                {'y', 'ʎ'},
                {'z', 'z'},

                {'A', '∀'},
                {'B', 'q'},
                {'C', 'Ɔ'},
                {'D', 'p'},
                {'E', 'Ǝ'},
                {'F', 'Ⅎ'},
                {'G', 'פ'},
                {'H', 'H'},
                {'I', 'I'},
                {'J', 'ſ'},
                {'K', 'ʞ'},
                {'L', '˥'},
                {'M', 'W'},
                {'N', 'N'},
                {'O', 'O'},
                {'P', 'Ԁ'},
                {'Q', 'Q'},
                {'R', 'ɹ'},
                {'S', 'S'},
                {'T', '┴'},
                {'U', '∩'},
                {'V', 'Λ'},
                {'W', 'M'},
                {'X', 'X'},
                {'Y', 'ʎ'},
                {'Z', 'Z'}
            };

        }

        public override void Execute()
        {
            var message = GetMessageFragment(1);

            if (message.ToLower().Contains("ashwinitv"))
            {
                Context.SendMessage(1000, "(ง •̀_•́)ง");
                Context.PurgeUser(User);
                return;
            }

            var reversedMessage = new string(message.Reverse().ToArray());

            var flipped = new StringBuilder("(╯°Д°）╯ ︵ ", reversedMessage.Length + 10);

            foreach (var t in reversedMessage) flipped.Append(char.IsLetter(t) ? FlipMap[t] : t);

            Context.SendMessage(flipped.ToString());
        }
    }

    [CommandKeywords("flip2")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class Flip2Command : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            if (GetMessageFragment(1).ToLower().Contains("ashwinitv"))
            {
                Context.SendMessage(1000, "(ง •̀_•́)ง");
                Context.PurgeUser(User);
                return;
            }

            ArgCountCheck(2);

            var reversed1 = new string(Arg().Reverse().ToArray());
            var reversed2 = new string(Arg(1).Reverse().ToArray());
            var flipped = new StringBuilder();

            foreach (var t in reversed1) flipped.Append(char.IsLetter(t) ? FlipCommand.FlipMap[t] : t);
            flipped.Append(' ');
            flipped.Append("╰(゜Д゜)╯ ");
            foreach (var t in Arg(1)) flipped.Append(char.IsLetter(t) ? FlipCommand.FlipMap[t] : t);

            Context.SendMessage(flipped.ToString());
        }
    }

    [CommandKeywords("reverse")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class ReverseCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            var message = new string(GetMessageFragment(1).Reverse().ToArray());
            if (message[0] == '/' || message[0] == '.' || message.Contains("#")) Context.SendMessage("No Kappa");
            else if (message[0] == '!') Context.PurgeUser(User);
            else Context.SendMessage(message);
        }
    }

    [CommandKeywords("followage")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    public sealed class FollowAgeCommand : BotCommand
    {
        public override void Execute()
        {
            var result = TwitchApi.GetFollowTime("ashwinitv", TokenManager.TwitchClientId, User).Result;
            if (!result.HasValue) Context.SendMessage(CommandResources.Follow_Error);
            else
            {
                if (result.Value == DateTime.MaxValue) Context.SendMessage(CommandResources.Follow_NotFollowing, User);
                else Context.SendMessage(CommandResources.Follow_Following, User, result.Value.ToString("dd.MM.yyyy"));
            }
        }
    }

    [CommandKeywords("percent")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    public sealed class PercentCommand : BotCommandWithArgs
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        public override void Execute()
        {
            var percent = Rng.GetNextTrueRandomUInt32(100);
            Context.SendMessage("The chance is {0}%, {1}", percent, User);
        }
    }
}
