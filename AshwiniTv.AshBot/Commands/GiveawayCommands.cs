﻿using System;
using AshwiniTv.AshBot.Localization;

namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("giveaway")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class GiveawayCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            switch (Arg())
            {
                case "subs": Subcount();
                    return;
                case "start": Start();
                    return;
                case "buyticket": Ticket();
                    return;
                case "winner": Winner();
                    return;
                case "mytickets": MyTickets();
                    return;
                case "reload": Reload();
                    return;
            }
        }

        private void Subcount()
        {
            Context.Giveaways.ShowSubs();
        }

        private void Start()
        {
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.Giveaways.Start();
        }

        private void Ticket()
        {
            if (ArgCount == 2) Context.Giveaways.Ticket(User, ArgInt(1));
            else Context.Giveaways.Ticket(User);
        }

        private void Winner()
        {
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.Giveaways.Winner();
        }

        private void MyTickets()
        {
            Context.Giveaways.TicketCount(User);
        }

        private void Reload()
        {
            AccessCheck(UserAccessLevel.Broadcaster);
            Context.Giveaways.Reload();
            Context.SendMessage(@"Done");
        }
    }

    [CommandKeywords("tickets")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class GiveawayTicketsCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount == 0)
            {
                Context.Giveaways.TicketCount(User);
                return;
            }

            AccessCheck(UserAccessLevel.Broadcaster);

            var count = ArgInt(1);
            var given = 0;
            for (var i = 0; i < count; ++i)
            {
                if (Context.Giveaways.GiveTicket(Arg())) ++given;
            }

            Context.SendMessage(CommandResources.Giveaway_TicketsGiven, given, Arg());
        }
    }

    [CommandKeywords("hint")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(10)]
    public sealed class GiveawayHintCommand : BotCommandWithArgsAndMessage
    {
        public override void Execute()
        {
            if (ArgCount == 0)
            {
                Context.Giveaways.ShowHint();
                return;
            }

            AccessCheck(UserAccessLevel.Broadcaster);

            switch (Arg())
            {
                case "set":
                    Context.Giveaways.UpdateHint(GetMessageFragment(2));
                    break;
                case "clear":
                    Context.Giveaways.UpdateHint(string.Empty);
                    break;
            }
        }
    }

    [CommandKeywords("treat")]
    [CommandAccess(UserAccessLevel.Pleb)]
    public sealed class TreatCommand : BotCommand
    {
        public override void Execute()
        {
            //Context.Giveaways.Treat(User);
            Context.SendMessage("( ° ͜ʖ͡°)╭∩╮");
        }
    }
}
