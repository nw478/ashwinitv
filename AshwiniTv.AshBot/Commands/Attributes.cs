﻿using System;

namespace AshwiniTv.AshBot.Commands
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CommandKeywordsAttribute : Attribute
    {
        public CommandKeywordsAttribute(params string[] keyword)
        {
            Keywords = keyword;
        }

        public string[] Keywords { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CommandAccessAttribute : Attribute
    {
        public CommandAccessAttribute(UserAccessLevel access)
        {
            RequiredAccess = access;
        }

        public UserAccessLevel RequiredAccess { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CommandDelayAttribute : Attribute
    {
        public CommandDelayAttribute(int delaySeconds)
        {
            Delay = TimeSpan.FromSeconds(delaySeconds);
        }

        public TimeSpan Delay { get; private set; }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class RestrictModsAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class DatashExclusiveAttribute : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Class)]
    public sealed class OfflineChatOnlyAttribute : Attribute
    {
    }
}