﻿namespace AshwiniTv.AshBot.Commands
{
    [CommandKeywords("emotetotal")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    [RestrictMods]
    public sealed class EmoteTotalCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(1);

            switch (ArgCount)
            {
                case 1:
                    Context.EmoteCounter.EmoteTotalForUser(Arg(), User, User);
                    break;
                case 2:
                    if (Arg(1) == "global") Context.EmoteCounter.EmoteTotal(Arg(), User);
                    else Context.EmoteCounter.EmoteTotalForUser(Arg(), Arg(1), User);
                    break;
            }
        }
    }

    [CommandKeywords("emote")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    public sealed class EmoteCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            ArgCountCheck(2);

            switch (ArgCount)
            {
                case 2:
                    Context.EmoteCounter.EmoteUser(Arg(), User, User, Arg(1));
                    break;
                case 3:
                    Context.EmoteCounter.EmoteUser(Arg(), Arg(2), User, Arg(1));
                    break;
            }
        }
    }

    [CommandKeywords("emoteday")]
    [CommandAccess(UserAccessLevel.Pleb)]
    [CommandDelay(5)]
    public sealed class EmoteDayCommand : BotCommandWithArgs
    {
        public override void Execute()
        {
            if (ArgCount > 0) Context.EmoteCounter.EmoteTotalLastDayUser(Arg());
            else Context.EmoteCounter.EmoteTotalLastDay();
        }
    }
}