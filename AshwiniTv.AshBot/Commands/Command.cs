﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace AshwiniTv.AshBot.Commands
{
    public interface IBotCommand
    {
        string[] Keywords { get; }
        TimeSpan Delay { get; }
        bool RestrictMods { get; }
        void Execute();
    }

    public interface IBotCommandInitialize
    {
        void SetParams(TimeSpan delay, bool restrictMods, ChannelContext context, string user);
    }

    public abstract class BotCommand : IBotCommand, IBotCommandInitialize
    {
        protected BotCommand()
        {
            Keywords = GetType().GetCustomAttribute<CommandKeywordsAttribute>().Keywords;
        }

        public ChannelContext Context { get; private set; }
        public string User { get; private set; }
        public string[] Keywords { get; }
        public TimeSpan Delay { get; private set; }
        public bool RestrictMods { get; private set; }
        public abstract void Execute();

        void IBotCommandInitialize.SetParams(TimeSpan delay, bool restrictMods, ChannelContext context, string user)
        {
            Delay = delay;
            RestrictMods = restrictMods;
            Context = context;
            User = user;
        }

        internal virtual void SetMessage(string message)
        {
        }

        protected void AccessCheck(UserAccessLevel neededAccess, bool report = true)
        {
            var access = Context.GetUserAccessLevel(User);
            if ((int) access < (int) neededAccess) throw new CommandAccessDeniedException(report, Context, User);
        }
    }

    public abstract class BotCommandWithArgs : BotCommand
    {
        protected IReadOnlyList<string> Args { get; private set; }

        protected int ArgCount => Args.Count;

        protected string Arg(int index = 0)
        {
            if (index == -1 && Args.Count != 0) return Args.Last();
            if (index >= Args.Count) throw new IndexOutOfRangeException();
            return Args[index];
        }

        protected int ArgInt(int index = 0)
        {
            if (index >= Args.Count) throw new IndexOutOfRangeException();
            return int.Parse(Args[index]);
        }

        protected long ArgLong(int index = 0)
        {
            if (index >= Args.Count) throw new IndexOutOfRangeException();
            return long.Parse(Args[index]);
        }

        protected bool? ArgOnOff(int index = 0)
        {
            switch (Arg(index).ToLower())
            {
                case "on":
                    return true;
                case "off":
                    return false;
                default:
                    return null;
            }
        }

        protected void ArgCountCheck(int requiredArgs)
        {
            if (Args.Count < requiredArgs) throw new InsufficientCommandArgsException();
        }

        internal override void SetMessage(string message)
        {
            var start = message.IndexOf(' ');
            Args = start == -1
                ? new string[0]
                : message.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries).Skip(1).ToArray();
        }
    }

    public abstract class BotCommandWithArgsAndMessage : BotCommandWithArgs
    {
        protected string Message { get; private set; }

        protected string GetMessageFragment(int firstWord)
        {
            if (string.IsNullOrEmpty(Message)) return string.Empty;
            var result = Message;

            for (var i = 0; i < firstWord; ++i) result = result.Substring(result.IndexOf(' ') + 1);

            return result;
        }

        internal override void SetMessage(string message)
        {
            base.SetMessage(message);
            Message = message;
        }
    }
}