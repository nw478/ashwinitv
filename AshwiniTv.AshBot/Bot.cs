﻿using System.Text;
using AshwiniTv.AshBot.Localization;
using AshwiniTv.Shared;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot
{
    public class Bot
    {
        private const string Server = "irc.chat.twitch.tv";
        private const int Port = 80;
        public const string Name = "winibutt";
        private const string Channel = "#ashwinitv";
        private readonly IrcClient _client;
        private ChannelContext _context;

        public Bot()
        {
            _client = new IrcClient
            {
                Encoding = Encoding.UTF8,
                ActiveChannelSyncing = true,
                SendDelay = 200,
                AutoRejoin = true,
                AutoRejoinOnKick = true,
                AutoReconnect = true,
                AutoRelogin = true,
                AutoRetry = true,
                AutoRetryLimit = 0
            };

            _client.OnChannelMessage += EvChannelMessage;
            _client.OnChannelAction += EvChannelMessage;

            _client.OnJoin += EvJoin;
            _client.OnPart += EvPart;
        }

        public void Run()
        {
            _client.Connect(Server, Port);
            _client.Login(Name, Name, 0, Name, TokenManager.TwitchChatOAuth);

            _client.WriteLine("CAP REQ :twitch.tv/membership");

            _client.RfcJoin(Channel);
            _client.Listen();
        }

        public void SendChatMessage(string channel, string message, params object[] args)
        {
            _client.SendMessage(SendType.Message, channel, string.Format(message, args));
        }

        public void SendAction(string channel, string message, params object[] args)
        {
            _client.SendMessage(SendType.Action, channel, string.Format(message, args));
        }

        public bool IsUserMod(string user, string channel)
        {
            var cUser = _client.GetChannelUser(channel, user);
            return cUser != null && cUser.IsOp;
        }

        public bool UserExists(string user, string channel)
        {
            return _client.GetChannelUser(channel, user) != null;
        }

        public void ReportAccessDenied(string user, string channel)
        {
            SendChatMessage(channel, CommonResources.Command_AccessDenied_Donger, user);
        }

        private void EvChannelMessage(object sender, IrcEventArgs args)
        {
            _context.MessageQueue.Add(args.Data);
        }

        private void EvJoin(object sender, JoinEventArgs args)
        {
            if (_context == null && args.Who == Name)
                _context = new ChannelContext(this, _client.GetChannel(args.Channel));
        }

        private void EvPart(object sender, PartEventArgs args)
        {
            _context.Activity.RemoveUserActivity(args.Who);
        }
    }
}