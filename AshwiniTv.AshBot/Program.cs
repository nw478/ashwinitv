﻿using System;
using AshwiniTv.AshBot.Utility;

namespace AshwiniTv.AshBot
{
    internal class Program
    {
        // ReSharper disable once UnusedParameter.Local
        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Console.SetOut(new ConsoleWriter(Console.Out));

            var bot = new Bot();
            bot.Run();
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = e.ExceptionObject as Exception;
            if (ex != null) Console.WriteLine(@"Unhandled exception at {0}: {1}", ex.TargetSite, ex.Message);
        }
    }
}