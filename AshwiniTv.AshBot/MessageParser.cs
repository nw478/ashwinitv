﻿using System;
using System.Diagnostics.CodeAnalysis;
using AshwiniTv.AshBot.Commands;
using Meebey.SmartIrc4net;

namespace AshwiniTv.AshBot
{
    public class MessageParser
    {
        private readonly ChannelContext _context;

        public MessageParser(ChannelContext context)
        {
            _context = context;
        }

        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        public IBotCommand ParseMessage(IrcMessageData message)
        {
            if (message.Message[0] != _context.CommandPrefix) return null;

            var keyword = message.MessageArray[0].Substring(1).ToLower();
            var info = CommandInfos.Get(keyword);

            if (info == null) return null;
            if (!VerifyAccess(message.Nick, info)) return null;
            if (_context.UptimeMon.StreamLive && info.OfflineChatOnly) return null;

            var command = Activator.CreateInstance(info.CommandType) as BotCommand;
            ((IBotCommandInitialize) command).SetParams(info.Delay, info.RestrictMods, _context, message.Nick);
            command.SetMessage(message.Message);
            return command;
        }

        private bool VerifyAccess(string user, CommandInfo info)
        {
            var access = _context.GetUserAccessLevel(user);

            if (access == UserAccessLevel.Pleb && _context.IgnorePlebs) return false;
            if (_context.IsUserIgnored(user)) return false;

            if ((int) access >= (int) info.RequiredAccess) return true;

            _context.ReportAccessDenied(user);
            return false;
        }
    }
}