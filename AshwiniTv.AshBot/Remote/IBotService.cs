﻿using System.ServiceModel;

namespace AshwiniTv.AshBot.Remote
{
    [ServiceContract]
    public interface IBotService
    {
        [OperationContract(IsOneWay = true)]
        void SongStateChange(long requestId, bool isPlaying);

        [OperationContract]
        bool SongRequestEnabled();

        [OperationContract(IsOneWay = true)]
        void Chat(string message);

        [OperationContract(IsOneWay = true)]
        void RigDicksize(string username, int minSize, int maxSize);

        [OperationContract(IsOneWay = true)]
        void RemoveRiggedDicksize(string username);

        [OperationContract(IsOneWay = true)]
        void AddBannedSong(string link);

        [OperationContract]
        bool SetGiveawayWord(string word);

        [OperationContract]
        bool IgnoreUser(string user);
    }
}