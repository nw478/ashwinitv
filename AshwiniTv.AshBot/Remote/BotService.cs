﻿using System;
using System.ServiceModel;
using AshwiniTv.AshBot.Commands;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.AshBot.Remote
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class BotService : IBotService
    {
        private readonly ChannelContext _context;

        public BotService(ChannelContext context)
        {
            _context = context;
        }

        public void SongStateChange(long requestId, bool isPlaying)
        {
            Console.WriteLine(@"> SongStateChange {0} {1}", requestId, isPlaying);
            _context.SongRequest.SongStateChange(requestId, isPlaying);
        }

        public bool SongRequestEnabled()
        {
            Console.WriteLine(@"> SongRequestEnabled");
            Console.WriteLine(@"< {0}", _context.SongRequest.Enabled);
            return _context.SongRequest.Enabled;
        }

        public void Chat(string message)
        {
            _context.SendMessage(message);
        }

        public void RigDicksize(string username, int minSize, int maxSize)
        {
            DicksizeCommand.RigUser(username, minSize, maxSize);
        }

        public void RemoveRiggedDicksize(string username)
        {
            DicksizeCommand.RemoveRiggedUser(username);
        }

        public void AddBannedSong(string link)
        {
            _context.SongRequest.BanSong(link);
        }

        public bool SetGiveawayWord(string word)
        {
            using (var tits = new AshwiniTvEntities())
            {
                try
                {
                    tits.SecretWords.Add(new SecretWord {id = 1, word = word});
                    tits.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public bool IgnoreUser(string user)
        {
            return _context.AddIgnoredUser(user);
        }
    }
}