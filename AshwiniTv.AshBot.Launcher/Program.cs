﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace AshwiniTv.AshBot.Launcher
{
    internal class Program
    {
        [LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        private static void Main()
        {
            using (var p = Process.GetCurrentProcess()) p.PriorityClass = ProcessPriorityClass.High;

            var startupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            var cachePath = Path.Combine(startupPath, "__cache");
            var configFile = Path.Combine(startupPath, "AshwiniTv.AshBot.exe.config");
            var assembly = Path.Combine(startupPath, "AshwiniTv.AshBot.exe");

            if (Directory.Exists(cachePath)) Directory.Delete(cachePath, true);

            var setup = new AppDomainSetup
            {
                ApplicationName = "AshwiniTv.AshBot",
                ShadowCopyFiles = "true",
                CachePath = cachePath,
                ConfigurationFile = configFile
            };

            var domain = AppDomain.CreateDomain("AshwiniTv.AshBot", AppDomain.CurrentDomain.Evidence, setup);
            domain.ExecuteAssembly(assembly);

            AppDomain.Unload(domain);
        }
    }
}