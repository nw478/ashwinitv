﻿using System;
using AshwiniTv.AshBot.Remote.Service;

namespace AshwiniTv.AshBot.Remote
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            while (true)
            {
                Console.Write(@"Command: ");
                PerformCommand(Console.ReadLine());
            }
        }

        private static void PerformCommand(string command)
        {
            try
            {
                var commandTokens = command.Split(' ');
                if (commandTokens.Length == 0) return;

                switch (commandTokens[0])
                {
                    case "chat":
                        var message = command.Substring(command.IndexOf(' ') + 1);
                        using (var client = new BotServiceClient()) client.Chat(message);
                        break;
                    //case "rig-dick":
                    //    if (commandTokens.Length < 4)
                    //    {
                    //        Console.WriteLine(@"Not enough arguments");
                    //        return;
                    //    }

                    //    var min = int.Parse(commandTokens[2]);
                    //    var max = int.Parse(commandTokens[3]);
                    //    using (var client = new BotServiceClient()) client.RigDicksize(commandTokens[1], min, max);

                    //    break;
                    //case "unrig-dick":
                    //    var username = command.Substring(command.IndexOf(' ') + 1);
                    //    using (var client = new BotServiceClient()) client.RemoveRiggedDicksize(username);
                    //    break;
                    case "ban-song":
                        using (var client = new BotServiceClient()) client.AddBannedSong(commandTokens[1]);
                        break;
                    case "secretword":
                        var word = command.Split(' ')?[1];
                        using (var client = new BotServiceClient())
                            Console.WriteLine(client.SetGiveawayWord(word) ? "OK" : "Fail");
                        break;
                    case "ignore":
                        var user = command.Split(' ')?[1];
                        using (var client = new BotServiceClient())
                            Console.WriteLine(client.IgnoreUser(user) ? "OK" : "Fail");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(@"Error executing command: " + ex.Message);
            }
        }
    }
}