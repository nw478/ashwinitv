﻿using System.Diagnostics.CodeAnalysis;

namespace AshwiniTv.Shared
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum TokenType
    {
        TwitchSecret = 0,
        YouTube = 1,
        YouTubeDataApi = 2,
        TwitchAshToken = 3,
        SoundCloudClientId = 4,
        SoundCloudClientSecret = 5,
        TwitchChatOAuthToken = 6,
        VimeoApiPersonalToken = 7,
        TwitchClientId = 8,
        TwitchSecret_DEBUG = 9,
        TwitchClientId_DEBUG = 10,
        SnapchatName = 11
    }
}