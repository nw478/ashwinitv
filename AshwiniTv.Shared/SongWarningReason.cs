﻿using System;

namespace AshwiniTv.Shared
{
    [Flags]
    public enum SongWarningReason : byte
    {
        None = 0,
        TooShort = 1,
        TooRecent = 2
    }
}