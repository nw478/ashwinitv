﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Threading;

namespace AshwiniTv.Shared
{
    public static class Logger
    {
        private static readonly StreamWriter Sw;
        private static readonly BlockingCollection<MessageData> Messages;
        // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        private static readonly Thread Worker;

        static Logger()
        {
#if LOGGING
            Directory.CreateDirectory("Logs");
            var now = DateTime.Now;
            Sw =
                new StreamWriter(
                    $"Logs\\{now.Year}-{now.Month}-{now.Day}__{now.Hour}-{now.Minute}-{now.Second}.log", false, Encoding.UTF8);
            Messages = new BlockingCollection<MessageData>();
            Worker = new Thread(WriteWorker) {Name = "Logger Writer"};
            Worker.Start();
#endif
        }

        public static void QueueMessage(string format, params object[] args)
        {
#if LOGGING
            Messages.Add(new MessageData
            {
                Timestamp = DateTime.Now,
                ThreadName = Thread.CurrentThread.Name,
                FormatLine = format,
                Args = args
            });
#endif
        }

        private static void WriteWorker()
        {
#if LOGGING
            while (true)
            {
                var data = Messages.Take();
                Sw.WriteLine(FormatMessage(data));
                Sw.Flush();
            }
#endif
            // ReSharper disable once FunctionNeverReturns
        }

#if LOGGING
        private static string FormatMessage(MessageData data)
        {
            var message = string.Format(data.FormatLine, data.Args);
            return $"[{data.Timestamp.ToString("HH:mm:ss.fff")}][{data.ThreadName}]:{message}";
        }
#endif

        private class MessageData
        {
            public DateTime Timestamp { get; set; }
            public string ThreadName { get; set; }
            public string FormatLine { get; set; }
            public object[] Args { get; set; }
        }
    }
}