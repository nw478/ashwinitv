﻿namespace AshwiniTv.Shared
{
    public enum SongRequestState
    {
        Paused,
        Playing
    }
}