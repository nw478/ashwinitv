﻿using System.Data.Entity.Core.Objects;
using System.Linq;

namespace AshwiniTv.Shared.DB
{
    public static class DbUtils
    {
        public static User EnsureUser(string username, AshwiniTvEntities context = null)
        {
            var tits = context ?? new AshwiniTvEntities();

            try
            {
                var dbUser = tits.Users.FirstOrDefault(u => u.username == username);

                if (dbUser != null) return dbUser;
                var param = new ObjectParameter("userID", 0L);
                tits.GetUser(username, param);

                dbUser = tits.Users.Find((long) param.Value);

                return dbUser;
            }
            finally
            {
                if (context == null) tits.Dispose();
            }
        }
    }
}