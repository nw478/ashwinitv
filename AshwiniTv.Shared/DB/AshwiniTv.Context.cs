﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AshwiniTv.Shared.DB
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class AshwiniTvEntities : DbContext
    {
        public AshwiniTvEntities()
            : base("name=AshwiniTvEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Announce> Announces { get; set; }
        public virtual DbSet<EmoteCount> EmoteCounts { get; set; }
        public virtual DbSet<EmoteCountsLifetime> EmoteCountsLifetimes { get; set; }
        public virtual DbSet<Emote> Emotes { get; set; }
        public virtual DbSet<Points> Points { get; set; }
        public virtual DbSet<Quote> Quotes { get; set; }
        public virtual DbSet<SongList> SongLists { get; set; }
        public virtual DbSet<SongRequest> SongRequests { get; set; }
        public virtual DbSet<Song> Songs { get; set; }
        public virtual DbSet<SRData> SRDatas { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<UptimeProgress> UptimeProgresses { get; set; }
        public virtual DbSet<Uptime> Uptimes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<PointConfig> PointConfigs { get; set; }
        public virtual DbSet<CustomCommand> CustomCommands { get; set; }
        public virtual DbSet<Pasta> Pastas { get; set; }
        public virtual DbSet<Highlight> Highlights { get; set; }
        public virtual DbSet<GiveawayEntry> GiveawayEntries { get; set; }
        public virtual DbSet<Giveaway> Giveaways { get; set; }
        public virtual DbSet<SecretWord> SecretWords { get; set; }
        public virtual DbSet<SecretWordHint> SecretWordHints { get; set; }
        public virtual DbSet<Treat> Treats { get; set; }
        public virtual DbSet<CopTicketCount> CopTicketCounts { get; set; }
    
        public virtual int GetUser(string username, ObjectParameter userID)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetUser", usernameParameter, userID);
        }
    
        public virtual int ClearUptimeProgress()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ClearUptimeProgress");
        }
    
        public virtual int ResetSongs()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ResetSongs");
        }
    
        public virtual int SetAnnounceTime(Nullable<int> hours)
        {
            var hoursParameter = hours.HasValue ?
                new ObjectParameter("hours", hours) :
                new ObjectParameter("hours", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SetAnnounceTime", hoursParameter);
        }
    }
}
