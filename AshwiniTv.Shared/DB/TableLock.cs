﻿using System;
using System.Data;
using System.Data.Entity;
using System.Threading;

namespace AshwiniTv.Shared.DB
{
    public sealed class TableLock : IDisposable
    {
        private static long _idCounter;
        private readonly string _id;
        private readonly DbContextTransaction _scope;

        public TableLock(DbContext context, string table)
        {
            var myIdCounter = Interlocked.Increment(ref _idCounter);
            _id = "TL" + myIdCounter;

            Logger.QueueMessage("About to acquire table lock on table {0}, lock id {1}", table, _id);

            const string sql = "SELECT TOP 1 1 FROM {0} WITH (TABLOCKX, HOLDLOCK)";
            var cmd = string.Format(sql, table);
            _scope = context.Database.BeginTransaction(IsolationLevel.Serializable);
            context.Database.ExecuteSqlCommand(cmd);
        }

        public void Dispose()
        {
            _scope.Commit();
            _scope.Dispose();
            Logger.QueueMessage("Released table lock id {0}", _id);
        }
    }
}