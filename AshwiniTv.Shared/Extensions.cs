﻿using System;
using System.Security.Cryptography;

namespace AshwiniTv.Shared
{
    public static class Extensions
    {
        public static uint GetNextTrueRandomUInt32(this RandomNumberGenerator rng, uint max)
        {
            var discard = (ulong.MaxValue - max) % max;
            var pool = new byte[8];
            
            rng.GetBytes(pool);

            var poolValue = BitConverter.ToUInt64(pool, 0);

            while (poolValue > (ulong.MaxValue - discard))
            {
                rng.GetBytes(pool);
                poolValue = BitConverter.ToUInt64(pool, 0);
            }

            var result = (uint) (poolValue%max);
            Logger.QueueMessage("Rolling a number, max {0}, result {1}", max, result);
            return result;
        }
    }
}