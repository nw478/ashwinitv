﻿using System.Linq;
using AshwiniTv.Shared.DB;

namespace AshwiniTv.Shared
{
    public static class SongRequest
    {
        public static bool SkipSong(long id, long currentSongId)
        {
            Logger.QueueMessage("Shared::SongRequest::SkipSong - skipping song id {0}, current song id {1}", id,
                currentSongId);

            using (var tits = new AshwiniTvEntities())
            {
                using (new TableLock(tits, "SongRequests"))
                {
                    var dbRequest = tits.SongRequests.Find(id == -1 ? currentSongId : id);

                    if (dbRequest == null) return false;

                    dbRequest.skipped = true;
                    var pos = dbRequest.position;
                    dbRequest.position = null;
                    tits.SaveChanges();

                    foreach (var song in tits.SongRequests.Where(sr => sr.position > pos)) song.position--;

                    tits.SaveChanges();
                    Logger.QueueMessage("Shared::SongRequest::SkipSong - skipped song id {0}, current song id {1}", id,
                        currentSongId);
                }
            }

            return true;
        }
    }
}