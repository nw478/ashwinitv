﻿using AshwiniTv.Shared.DB;

namespace AshwiniTv.Shared
{
    public static class TokenManager
    {
        private static string _ytData;
        private static string _twitchSecret;
        private static string _twitchClientId;
        private static string _scId;
        private static string _twitchChatOAuth;
        private static string _twitchAshToken;
        private static string _vimeoApi;
        private static string _snapchat;

        public static string YouTubeDataApi => GetValue(TokenType.YouTubeDataApi, ref _ytData);

        public static string TwitchSecret
        {
            get
            {
#if DEBUG_LOCAL
                const TokenType type = TokenType.TwitchSecret_DEBUG;
#else
                const TokenType type = TokenType.TwitchSecret;
#endif
                return GetValue(type, ref _twitchSecret);
            }
        }

        public static string TwitchClientId
        {
            get
            {
#if DEBUG_LOCAL
                const TokenType type = TokenType.TwitchClientId_DEBUG;
#else
                const TokenType type = TokenType.TwitchClientId;
#endif
                return GetValue(type, ref _twitchClientId);
            }
        }

        public static string SoundCloudClientId => GetValue(TokenType.SoundCloudClientId, ref _scId);

        public static string TwitchChatOAuth => GetValue(TokenType.TwitchChatOAuthToken, ref _twitchChatOAuth);

        public static string TwitchAshToken => GetValue(TokenType.TwitchAshToken, ref _twitchAshToken);

        public static string VimeoApi => GetValue(TokenType.VimeoApiPersonalToken, ref _vimeoApi);

        public static string SnapchatName => GetValue(TokenType.SnapchatName, ref _snapchat);

        private static string GetValue(TokenType tokenType, ref string value)
        {
            if (!string.IsNullOrEmpty(value)) return value;

            using (var tits = new AshwiniTvEntities())
            {
                value = tits.Tokens.Find((int) tokenType).value;
                return value;
            }
        }
    }
}