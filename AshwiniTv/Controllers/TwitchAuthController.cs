﻿using System;
using System.Web;
using System.Web.Mvc;
using AshwiniTv.Shared;
using AshwiniTv.Twitch;

namespace AshwiniTv.Controllers
{
    public class TwitchAuthController : Controller
    {
        public ActionResult Index(string code, string state)
        {
            var expectedState = Session["TwitchState"] as string;

            if (state != expectedState || state == null) return RedirectToAction("Home", "Index");

            var token =
                TwitchApi.AuthUser(code, AshController.RedirectUrl, TokenManager.TwitchClientId,
                    TokenManager.TwitchSecret).Result;

            var tokenCookie = new HttpCookie("AshwiniTv2_TwitchToken", token)
            {
                Secure = true,
                Expires = DateTime.Now.AddMonths(1)
            };

            Response.Cookies.Add(tokenCookie);

            return RedirectToAction("Index", state.StartsWith("Ash") ? "Ash" : "SongList");
        }
    }
}