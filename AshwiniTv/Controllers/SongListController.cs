﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;
using AshwiniTv.WiniBot;

namespace AshwiniTv.Controllers
{
    public class SongListController : Controller
    {
        private static readonly Regex RxWarnId = new Regex(@"\d+$");

        private static readonly string[] WarningStrings =
        {
            "Video is too short (shorter than 2 minutes).",
            "Video was uploaded less than a day ago."
        };

        public ActionResult Index()
        {
            try
            {
                using (var client = new BotServiceClient()) ViewBag.SREnabled = client.SongRequestEnabled();
            }
            catch
            {
                ViewBag.SREnabled = false;
            }

            return View();
        }

        [HttpPost]
        public JsonResult GetWarningTooltip(string id)
        {
            var iid = long.Parse(RxWarnId.Match(id).Value);
            SongWarningReason warningType;

            using (var tits = new AshwiniTvEntities())
            {
                warningType = (SongWarningReason) tits.Songs.Find(iid).warningReason;
            }

            var tooltipText = new StringBuilder("Suspicious song!<br><ul>");

            if ((warningType & SongWarningReason.TooShort) == SongWarningReason.TooShort)
            {
                tooltipText.AppendFormat("<li>{0}</li>", WarningStrings[0]);
            }

            if ((warningType & SongWarningReason.TooRecent) == SongWarningReason.TooRecent)
            {
                tooltipText.AppendFormat("<li>{0}</li>", WarningStrings[1]);
            }

            tooltipText.Append("</ul>");

            return Json(tooltipText.ToString());
        }

        [HttpPost]
        public JsonResult GetSongs(int jtStartIndex, int jtPageSize)
        {
            using (var tits = new AshwiniTvEntities())
            {
                try
                {
                    var songs =
                        (from s in tits.SongRequests
                            where !s.played && !s.hidden && !s.skipped
                            let pos = s.position.HasValue ? 0 : 1
                            orderby s.promoted descending, pos, s.position
                            select new
                            {
                                ID = s.id,
                                Name = s.Song.name,
                                Link = s.Song.link,
                                Tid = s.Song.trackID,
                                Source = s.Song.source,
                                Length = s.Song.length.ToString(),
                                RequestedBy = s.User.username,
                                Promoted = s.promoted
                            }).ToArray();

                    if (songs.Length > 1 && songs[0].Promoted)
                    {
                        var temp = songs[0];
                        songs[0] = songs[1];
                        songs[1] = temp;
                    }

                    return
                        Json(
                            new
                            {
                                Result = "OK",
                                Records = songs.Skip(jtStartIndex).Take(jtPageSize),
                                TotalRecordCount = songs.Length
                            });
                }
                catch (Exception ex)
                {
                    return Json(new {Result = "ERROR", ex.InnerException.Message});
                }
            }
        }

        [HttpPost]
        public JsonResult GetPrevSongs(int jtStartIndex, int jtPageSize)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var songs =
                    (from s in tits.SongRequests
                        where s.played && !s.hidden && !s.skipped
                        orderby s.position descending
                        select new
                        {
                            ID = s.id,
                            Name = s.Song.name,
                            Link = s.Song.link,
                            Tid = s.Song.trackID,
                            Source = s.Song.source,
                            Length = s.Song.length.ToString(),
                            RequestedBy = s.User.username
                        }).ToArray();

                return
                    Json(
                        new
                        {
                            Result = "OK",
                            Records = songs.Skip(jtStartIndex).Take(jtPageSize),
                            TotalRecordCount = songs.Length
                        });
            }
        }
    }
}