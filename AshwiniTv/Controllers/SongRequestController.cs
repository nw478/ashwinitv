﻿using System.Web.Mvc;
using AshwiniTv.Hubs;
using Microsoft.AspNet.SignalR;

namespace AshwiniTv.Controllers
{
#if !DEBUG
    [BotAccessOnly]
#endif
    public class SongRequestController : Controller
    {
        [HttpGet]
        public ActionResult SongListChange()
        {
            Response.StatusCode = 200;

            var hub = GlobalHost.ConnectionManager.GetHubContext<SongRequestHub>();
            hub.Clients.All.SongListChange();

            return null;
        }

        [HttpGet]
        public ActionResult SkipCurrentSong()
        {
            Response.StatusCode = 200;

            var hub = GlobalHost.ConnectionManager.GetHubContext<SongRequestHub>();
            hub.Clients.All.SkipCurrentSong();

            return null;
        }

        [HttpGet]
        public ActionResult SetVolume(int volume)
        {
            Response.StatusCode = 200;

            var hub = GlobalHost.ConnectionManager.GetHubContext<SongRequestHub>();
            hub.Clients.All.SetVolume(volume);

            return null;
        }
    }
}