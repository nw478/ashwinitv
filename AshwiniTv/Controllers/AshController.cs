﻿using System;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using AshwiniTv.Hubs;
using AshwiniTv.Models;
using AshwiniTv.Shared;
using AshwiniTv.Shared.DB;
using AshwiniTv.Twitch;
using AshwiniTv.WiniBot;
using Microsoft.AspNet.SignalR;

namespace AshwiniTv.Controllers
{
    public class AshController : Controller
    {
#if DEBUG_LOCAL
        public const string RedirectUrl = "https://localhost:44300/TwitchAuth";
#else
        public const string RedirectUrl = "https://www.ashwini.tv/TwitchAuth";
#endif

        private static readonly string TwitchAuthUrl =
            $"https://api.twitch.tv/kraken/oauth2/authorize?response_type=code&client_id={TokenManager.TwitchClientId}&redirect_uri={RedirectUrl}&scope=user_read+channel_subscriptions+channel_check_subscription+channel_editor+channel_commercial";

        public ActionResult Index()
        {
            var twitchCookie = Request.Cookies["AshwiniTv2_TwitchToken"];
            if (twitchCookie == null)
            {
                var state = "Ash_" + Utils.GenRandomString(20);
                Session["TwitchState"] = state;
                return Redirect(TwitchAuthUrl + "&state=" + state);
            }

            var twitchToken = twitchCookie.Value;

            TwitchUser twitchUser;

            if (string.IsNullOrEmpty(twitchToken) ||
                (twitchUser = TwitchApi.GetAuthedUser(TokenManager.TwitchClientId, twitchToken).Result) == null)
            {
                twitchCookie.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(twitchCookie);
                var state = "Ash_" + Utils.GenRandomString(20);
                Session["TwitchState"] = state;
                return Redirect(TwitchAuthUrl + "&state=" + state);
            }

            if (twitchUser.Name != "ashwinitv" && twitchUser.Name != "n1ghtsh0ck" && twitchUser.Name != "daehauq")
                return RedirectToAction("Index", "Home");

            Session["UsernameVerified"] = true;

            if (twitchUser.Name != "ashwinitv") return RedirectToAction("SongRequest");

            SaveAshToken(twitchToken);

            return RedirectToAction("SongRequest");
        }

        public ActionResult SongRequest()
        {
            if (Session["UsernameVerified"] == null) return RedirectToAction("Index");
            return View();
        }

        public ActionResult Emotes()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetNextSong(SongStub lastSong)
        {
            using (var tits = new AshwiniTvEntities())
            {
                if (lastSong.Id != -1)
                {
                    var dbRequest = tits.SongRequests.Find(lastSong.Id);
                    dbRequest.played = true;
                    tits.SaveChanges();

                    NotifySongListChange();
                }

                var nextSong =
                    tits.SongRequests.Where(sr => !sr.played && !sr.hidden && !sr.skipped)
                        .OrderByDescending(sr => sr.promoted)
                        .ThenBy(sr => sr.position)
                        .FirstOrDefault();

                if (nextSong != null)
                    return
                        Json(new SongStub
                        {
                            Id = nextSong.id,
                            Tid = nextSong.Song.trackID,
                            Name = nextSong.Song.name,
                            Source = nextSong.Song.source,
                            RequestedBy = nextSong.User.username
                        });

                return Json(new {});
            }
        }

        [HttpPost]
        public ActionResult SkipSong(SongStub song)
        {
            Shared.SongRequest.SkipSong(song.Id, 0);
            NotifySongListChange();
            return null;
        }

        [HttpPost]
        public ActionResult ResetSongs()
        {
            using (var tits = new AshwiniTvEntities()) tits.ResetSongs();
            NotifySongListChange();
            return null;
        }

        [HttpPost]
        public ActionResult SetSongRequestState(SongState state)
        {
            using (var client = new BotServiceClient()) client.SongStateChange(state.Id, state.State == 1);
            return null;
        }

        [HttpPost]
        public ActionResult PromoteSong(long id)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var promoted = tits.SongRequests.FirstOrDefault(sr => sr.promoted && !sr.played);
                if (promoted != null) return null;

                var song = tits.SongRequests.Find(id);
                if (song == null) return null;
                song.promoted = true;
                tits.SaveChanges();
            }

            NotifySongListChange();

            return null;
        }

        private static void NotifySongListChange()
        {
            var hub = GlobalHost.ConnectionManager.GetHubContext<SongRequestHub>();
            hub.Clients.All.SongListChange();
        }

        private static void SaveAshToken(string token)
        {
            using (var tits = new AshwiniTvEntities())
            {
                var dbToken = tits.Tokens.Find((int) TokenType.TwitchAshToken);
                if (dbToken == null)
                    tits.Tokens.Add(new Token {type = (byte) TokenType.TwitchAshToken, value = token});
                else if (dbToken.value != token) dbToken.value = token;
                tits.SaveChanges();
            }
        }

        public class SongState
        {
            public long Id { get; set; }
            public int State { get; set; }
        }
    }
}