﻿using System.Web.Optimization;

namespace AshwiniTv
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsHome").Include(
                "~/Scripts/jssor.js",
                "~/Scripts/jssor.slider.js",
                "~/Scripts/twitter.js",
                "~/Scripts/twitch.js",
                "~/Scripts/slideshow.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsSongList").Include(
                "~/Scripts/jquery-ui-{version}.js",
                "~/Scripts/jtable/jquery.jtable.js",
                "~/Scripts/jquery.signalR-{version}.js",
                "~/Scripts/songdata.js",
                "~/Scripts/songlist.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsSongRequest").Include(
                "~/Scripts/jtable/jquery.jtable.js",
                "~/Scripts/jquery.signalR-{version}.js",
                "~/Scripts/jquery.jplayer.js",
                "~/Scripts/songdata.js",
                "~/Scripts/songrequest.js",
                "~/Scripts/songrequest-youtube.js",
                "~/Scripts/songrequest-soundcloud.js",
                "~/Scripts/froogaloop.js",
                "~/Scripts/songrequest-vimeo.js"));

            bundles.Add(new StyleBundle("~/Content/cssMain").Include(
                "~/Content/site.css",
                "~/Content/Themes/base/jquery-ui.css"));

            bundles.Add(new StyleBundle("~/Content/cssSongList").Include(
                "~/Scripts/jtable/themes/metro/brown/jtable.css",
                "~/Scripts/theme-vintage-wine/style.css",
                "~/Content/SongList.css"));
        }
    }
}