﻿using AshwiniTv;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof (SignalRConfig))]

namespace AshwiniTv
{
    public class SignalRConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}