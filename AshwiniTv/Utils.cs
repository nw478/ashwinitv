﻿using System.Linq;
using System.Security.Cryptography;
using AshwiniTv.Shared;

namespace AshwiniTv
{
    public static class Utils
    {
        private static readonly RNGCryptoServiceProvider Rng = new RNGCryptoServiceProvider();

        public static string GenRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[(int)Rng.GetNextTrueRandomUInt32((uint)s.Length)]).ToArray());
        }
    }
}