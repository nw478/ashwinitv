﻿using System.Web;
using System.Web.Mvc;

namespace AshwiniTv
{
    public class BotAccessOnlyAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return !string.IsNullOrEmpty(httpContext.Request.UserAgent) &&
                   httpContext.Request.UserAgent.StartsWith("WiniBot");
        }
    }
}