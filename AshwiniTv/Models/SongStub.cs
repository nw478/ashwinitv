﻿namespace AshwiniTv.Models
{
    public class SongStub
    {
        public long Id { get; set; }
        public string Tid { get; set; }
        public short Source { get; set; }
        public string RequestedBy { get; set; }
        public string Name { get; set; }
    }
}