﻿using System.IO;
using System.Threading;
using Microsoft.AspNet.SignalR;

namespace AshwiniTv.Hubs
{
    public class EmoteHub : Hub
    {
        private static readonly ReaderWriterLockSlim Srwl = new ReaderWriterLockSlim();

        public void EmoteAppeared(string emote)
        {
            Clients.All.EmoteAppeared(emote);
        }
    }
}