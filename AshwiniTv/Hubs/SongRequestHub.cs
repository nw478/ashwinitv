﻿using Microsoft.AspNet.SignalR;

namespace AshwiniTv.Hubs
{
    public class SongRequestHub : Hub
    {
        public void SongListChange()
        {
            Clients.All.SongListChange();
        }

        public void SkipCurrentSong()
        {
            Clients.All.SkipCurrentSong();
        }

        public void SetVolume(int volume)
        {
            Clients.All.SetVolume(volume);
        }
    }
}