﻿var ytReady = false;
var firstInit = true;

var SONG_STATE_PAUSED = 0;
var SONG_STATE_PLAYING = 1;

var players = [null, null, null];
var activePlayer = -1;
var playerState = SONG_STATE_PAUSED;

var lastSong = { Id: -1, Tid: "" };

function SongRequestPlayer(type) {
    this.type = type;
    this.playerObj = null;
    this.displayID = null;
    this.loadSong = null;
    this.play = null;
    this.pause = null;
    this.setVolume = null;
}

$(document).ready(function() {
    var sr = $.connection.songRequestHub;

    sr.client.SongListChange = function() {
        $("#SongList").jtable("load");
    };

    sr.client.SkipCurrentSong = function() {
        skipSong(-1);
    };

    sr.client.SetVolume = setVolume;

    $.connection.hub.start();

    loadScript("https://www.youtube.com/iframe_api");

    scCreatePlayer();
    vmCreatePlayer();

    $("#youtube_player").css("z-index", 1);
    $("#soundcloud_player").css("z-index", 0);

    initSongList();
});

function loadScript(url) {
    var tag = document.createElement("script");
    tag.src = url;

    var scriptTag = document.getElementsByTagName("script")[0];
    scriptTag.parentNode.insertBefore(tag, scriptTag);
}

function activatePlayer(playerId) {
    for (var id = ID_YOUTUBE; id < ID_MAX; id++) {
        if (id === playerId) $(players[id].displayID).css("z-index", 1);
        else $(players[id].displayID).css("z-index", 0);
    }
}

function setVolume(volume) {
    players[activePlayer].setVolume(volume);
}

function playPause() {
    if (playerState === SONG_STATE_PAUSED) playSong();
    else players[activePlayer].pause();
}

function playSong() {
    if (firstInit) nextSong();
    else players[activePlayer].play();
}

function nextSong() {
    songStateChange(SONG_STATE_PAUSED);
    $.ajax({
        url: "/Ash/GetNextSong",
        type: "POST",
        dataType: "json",
        data: lastSong,
        cache: false,
        success: loadNewSong
    });
}

function loadNewSong(song) {
    if (jQuery.isEmptyObject(song)) return;
    if (firstInit) firstInit = false;
    lastSong = song;
    activePlayer = song.Source;
    activatePlayer(song.Source);
    $("#reqLabel").text(song.RequestedBy);
    players[song.Source].loadSong(song.Tid, song.Name);
    players[song.Source].play();
}

function skipSong(songId) {
    var songData = { Id: songId };

    $.ajax({
        url: "/Ash/SkipSong",
        type: "POST",
        dataType: "json",
        data: songData,
        cache: false
    });

    if (songId === lastSong.Id) {
        if (!firstInit) players[activePlayer].pause();
        nextSong();
    }
}

function clearSongs() {
    $.post("/Ash/ResetSongs");
}

function promoteSong(id) {
    $.post("/Ash/PromoteSong", {
        id: id
    });
}

function songStateChange(state) {
    $("#playPauseButton").text(state === SONG_STATE_PAUSED ? "Play" : "Pause");
    playerState = state;
    var stateObj = { Id: lastSong.Id, State: state };
    $.post("/Ash/SetSongRequestState", {
        state: stateObj,
        dataType: "json"
    });
}

function initSongList() {
    $("#SongList").jtable({
        title: "Song Queue",
        paging: true,
        actions: {
            listAction: "/SongList/GetSongs"
        },
        recordsLoaded: function() {
            addWarningTooltips();
            setPromoteArrowVisibility();
        },
        fields: {
            ID: {
                title: "ID",
                key: true,
                width: "7%",
                listClass: "songlist"
            },
            Warning: {
                title: "Name", // such hax wow
                width: "5%",
                listClass: "warningcell",
                display: drawIcons
            },
            Name: {
                title: "",
                width: "40%",
                listClass: "songlist namecell"
            },
            Link: {
                title: "Link",
                width: "5%",
                listClass: "songlist",
                display: getLinkDisplay
            },
            Length: {
                title: "Length",
                width: "10%",
                listClass: "songlist"
            },
            RequestedBy: {
                title: "Requested By",
                width: "15%",
                listClass: "songlist"
            },
            Actions: {
                title: "Actions",
                width: "10%",
                listClass: "songlist",
                display: drawActions
            }
        }
    });

    $("#SongList").jtable("load");
}

function drawIcons(data) {
    if (data.record.Warning) return "<img id=\"warning_" + data.record.ID + "\" class=\"tooltip\" src=\"/Content/Images/SR/warning.gif\"/>";
    else if (data.record.Promoted) return "<img id=\"promoteStar\" src=\"/Content/Images/SR/star.png\"/>";
    else return "";
}

function drawActions(data) {
    return "<a href=\"javascript: skipSong(" + data.record.ID + "); \"><img src=\"/Content/Images/SR/trash.png\" width=\"32\" height=\"32\" title=\"Skip song\"></img></a>" +
        "<img class=\"promoteArrow\" onclick=\"javascript: promoteSong(" + data.record.ID + ");\" src=\"/Content/Images/SR/promote.png\" width=\"32\" height=\"32\" title=\"Promote song\"></img>";
}

function addWarningTooltips() {
    $(document).tooltip({
        items: ".tooltip",
        tooltipClass: "warning_tooltip",
        content: function(callback) {
            $.post("/SongList/GetWarningTooltip", {
                id: this.id
            }, function(data) {
                callback(data);
            });
        }
    });
}

function setPromoteArrowVisibility() {
    if ($("#promoteStar").length > 0) {
        $(".promoteArrow").attr("src", "/Content/Images/SR/promote_disabled.png");
        $(".promoteArrow").removeAttr("click");
    }
}