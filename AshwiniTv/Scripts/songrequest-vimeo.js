﻿function vmCreatePlayer(tid) {
    var vPlayerTag = document.getElementById("vimeo_player_PH");
    vPlayerTag.frameBorder = "0";
    vPlayerTag.src = "https://player.vimeo.com/video/109516154?autopause=0&autoplay=0&badge=0&byline=0&portrait=0&api=1&wmode=opaque&player_id=vimeo_player_PH";

    var vPlayer = $f("vimeo_player_PH");
    vPlayer.addEvent("ready", function() {
        vmRegisterEvents(vPlayer);
    });

    var srPlayer = new SongRequestPlayer(ID_VIMEO);
    srPlayer.playerObj = vPlayer;
    srPlayer.displayID = "#vimeo_player";
    srPlayer.loadSong = function(trackId) { vPlayerTag.src = vmCreateSrc(trackId); };
    srPlayer.play = function() { vPlayer.api("play") };
    srPlayer.pause = function() { vPlayer.api("pause") };
    srPlayer.setVolume = function(volume) { vPlayer.api("setVolume", volume / 100.0); };
    players[ID_VIMEO] = srPlayer;
}

function vmCreateSrc(tid) {
    return "https://player.vimeo.com/video/" +
        tid +
        "?autopause=0&autoplay=1&badge=0&byline=0&portrait=0&api=1&wmode=opaque&player_id=vimeo_player_PH";
}

function vmRegisterEvents(player) {
    player.addEvent("play", function() { songStateChange(SONG_STATE_PLAYING); });
    player.addEvent("pause", function() { songStateChange(SONG_STATE_PAUSED); });
    player.addEvent("finish", function() {
        vmUnregisterEvents(player);
        nextSong();
    });
}

function vmUnregisterEvents(player) {
    player.removeEvent("play");
    player.removeEvent("pause");
    player.removeEvent("finish");
}