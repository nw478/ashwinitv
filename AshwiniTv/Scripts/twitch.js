﻿$(document).ready(function() {
    $.getJSON("https://api.twitch.tv/kraken/streams/ashwinitv?callback=?", function(streamData) {
        if (streamData && streamData.stream) {
            document.getElementById("streamWidget").style.backgroundImage = "url('/Content/Images/online.jpg')";
        } else {
            document.getElementById("streamWidget").style.backgroundImage = "url('/Content/Images/offline.jpg')";
        }
    });
})