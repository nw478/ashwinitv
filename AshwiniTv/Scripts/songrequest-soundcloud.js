﻿var sc_clID = "c5c96f105109b6e10b65b18de9e6a9ac";

function scCreatePlayer() {
    var scPlayer = $("#soundcloud_player_PH").jPlayer({
        cssSelectorAncestor: "#soundcloud_player",
        swfPath: "/Scripts",
        useStateClassSkin: true,
        smoothPlayBar: true,
        volume: 0.5,
        size: {
            width: "640px",
            height: "328px",
            cssClass: "jp-video-360p"
        },
        ready: function() {
            $(this).jPlayer("setMedia", {
                mp3: scGetUrl(193369430),
                poster: "/Content/Images/SR/sc_player_bg.jpg"
            });
            $(this).bind($.jPlayer.event.click, function() {
                var isPaused = $(this).data().jPlayer.status.paused;
                if (isPaused) {
                    $(this).jPlayer("play");
                } else {
                    $(this).jPlayer("pause");
                }
            });
        },
        play: function() { songStateChange(SONG_STATE_PLAYING); },
        pause: function() { songStateChange(SONG_STATE_PAUSED); },
        ended: nextSong
    });

    var srPlayer = new SongRequestPlayer(ID_SOUNDCLOUD);
    srPlayer.playerObj = scPlayer;
    srPlayer.displayID = "#soundcloud_player";
    srPlayer.loadSong = scLoadSong;
    srPlayer.play = function() { scPlayer.jPlayer("play") };
    srPlayer.pause = function() { scPlayer.jPlayer("pause"); };
    srPlayer.setVolume = function(volume) { scPlayer.jPlayer("volume", volume / 100.0); };
    players[ID_SOUNDCLOUD] = srPlayer;
}

function scGetUrl(tid) {
    return "http://api.soundcloud.com/tracks/" +
        tid +
        "/stream?client_id=" +
        sc_clID;
}

function scLoadSong(tid, name) {
    this.playerObj.jPlayer("setMedia", {
        mp3: scGetUrl(tid),
        poster: "/Content/Images/SR/sc_player_bg.jpg"
    });

    $("#scSongTitle").text(name);
}