﻿var ID_YOUTUBE = 0;
var ID_SOUNDCLOUD = 1;
var ID_VIMEO = 2;
var ID_MAX = 3;

function getLinkDisplay(data) {
    switch (data.record.Source) {
    case ID_YOUTUBE:
        return "<a href=\"https://www.youtube.com/watch?v=" + data.record.Tid + "\" target=\"_blank\"><img src=\"/Content/Images/SR/youtube.png\" width=\"32\" height=\"32\" /></a>";
    case ID_SOUNDCLOUD:
        return "<a href=\"https://soundcloud.com/" + data.record.Link + "\" target=\"_blank\"><img src=\"/Content/Images/SR/soundcloud.png\" width=\"32\" height=\"32\" /></a>";
    case ID_VIMEO:
        return "<a href=\"https://vimeo.com/" + data.record.Tid + "\" target=\"_blank\"><img src=\"/Content/Images/SR/vimeo.png\" width=\"32\" height=\"32\" /></a>";
    }
}