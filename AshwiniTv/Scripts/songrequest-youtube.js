﻿function onYouTubeIframeAPIReady() {
    var ytPlayer = new YT.Player("youtube_player_PH", {
        width: "640",
        height: "390",
        videoId: "gCYcHz2k5x0",
        playerVars: {
            "autohide": 1,
            "iv_load_policy": 3,
            "modestbranding": 1,
            "rel": 0,
            "wmode": "opaque"
        },
        events: {
            "onStateChange": onYTPlayerStateChange
        }
    });

    var srPlayer = new SongRequestPlayer(ID_YOUTUBE);
    srPlayer.playerObj = ytPlayer;
    srPlayer.displayID = "#youtube_player";
    srPlayer.loadSong = function(tid) { ytPlayer.loadVideoById(tid); };
    srPlayer.play = function() { ytPlayer.playVideo(); };
    srPlayer.pause = function() { ytPlayer.pauseVideo(); };
    srPlayer.setVolume = function(volume) { ytPlayer.setVolume(volume); };
    players[ID_YOUTUBE] = srPlayer;
}

function onYTPlayerStateChange(event) {
    switch (event.data) {
    case YT.PlayerState.PLAYING:
        songStateChange(SONG_STATE_PLAYING);
        break;
    case YT.PlayerState.PAUSED:
        songStateChange(SONG_STATE_PAUSED);
        break;
    case YT.PlayerState.ENDED:
        nextSong();
        break;
    }
}