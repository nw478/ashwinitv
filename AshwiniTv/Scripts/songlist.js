﻿$(document).ready(function() {
    var sr = $.connection.songRequestHub;
    sr.client.SongListChange = function() {
        $("#SongList").jtable("load");
        $("#PreviousSongs").jtable("load");
    };
    $.connection.hub.start();

    $("#SongList").jtable({
        title: "Ashwini&apos;s song list",
        paging: true,
        actions: {
            listAction: "/SongList/GetSongs"
        },
        fields: {
            ID: {
                title: "ID",
                key: true,
                width: "7%",
                listClass: "songlist"
            },
            Warning: {
                title: "Name", // such hax wow
                width: "5%",
                listClass: "warningcell",
                display: drawIcons
            },
            Name: {
                title: "",
                width: "40%",
                listClass: "songlist namecell"
            },
            Link: {
                title: "Link",
                width: "5%",
                listClass: "songlist",
                display: getLinkDisplay
            },
            Length: {
                title: "Length",
                width: "10%",
                listClass: "songlist"
            },
            RequestedBy: {
                title: "Requested By",
                width: "15%",
                listClass: "songlist"
            }
        }
    });

    $("#PreviousSongs").jtable({
        title: "Previously played songs",
        paging: true,
        actions: {
            listAction: "/SongList/GetPrevSongs"
        },
        fields: {
            ID: {
                title: "ID",
                key: true,
                listClass: "songlist"
            },
            Name: {
                title: "Name",
                width: "30%",
                listClass: "songlist"
            },
            Link: {
                title: "Link",
                width: "5%",
                listClass: "songlist",
                display: getLinkDisplay
            },
            Length: {
                title: "Length",
                width: "10%",
                listClass: "songlist"
            },
            RequestedBy: {
                title: "Requested By",
                width: "15%",
                listClass: "songlist"
            }
        }
    });

    $("#SongList").jtable("load");
    $("#PreviousSongs").jtable("load");
});

function drawIcons(data) {
    if (data.record.Promoted) return "<img src=\"/Content/Images/SR/star.png\"/>";
}